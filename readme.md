#### Laravel Project Deployment Steps ####

##Server Requirements
	The Laravel framework has a few system requirements. All of these requirements are satisfied by the Laravel Homestead virtual machine, so it's highly recommended that you use Homestead as your local Laravel development environment.

	you will need to make sure your server meets the following requirements:

	PHP >= 7.1.3
	OpenSSL PHP Extension
	PDO PHP Extension
	Mbstring PHP Extension
	Tokenizer PHP Extension
	XML PHP Extension
	Ctype PHP Extension
	JSON PHP Extension
	BCMath PHP Extension
	
	for all this you can install xampp/wamp server.
	
	Database required - MySQL


## Install Laravel Project

	# cd in your project directory and open command prompt.
	e.g. - C:\laravel\zone>

	# Install the dependencies with Composer.
	composer install

	after this you will get 'vendor' folder in your project.


##Run the following command to generate your app key:
php artisan key:generate


## .env file variables - 
	
	# Environment
	APP_ENV = DEV / BETA / PROD

	# Debug mode
	APP_DEBUG = true / false

	# Base URL 
	APP_URL = 'your_website_url' 
	e.g. - http://example.com
	
	Or you can create virtual host to run this project.

	# Database detail
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1 or remote adderess of db host
	DB_PORT=3306 or other port
	DB_DATABASE=your_database_name
	DB_USERNAME=your_username
	DB_PASSWORD=your_password
	
	# Google Map Key
	MAP_KEY=AIzaSyD1OqTZ3JXEGl8P39rS44DlFAwGeTmveGc

	# Google Login
	GOOGLE_CLIENT_ID=xxxxxxxx.apps.googleusercontent.com
	GOOGLE_CLIENT_SECRET=
	GOOGLE_REDIRECT=http://example.com/callback/google

	WHATSAPP_FLAG=1
	GOOGLE_LOGIN_DEFAULT=0	
	
	# Google Cloud Storage Bucket
	
	GCS_BUCKET=arms4-zonificacion-dev
	
	Please make sure this bucket is Readable and Writable by service account user.
	
## Google Clound Storage Private Key
	Please paste the service account key json into this file.
	/public/gcs_key.json	
	
	
##Migrating DB schema. 
php artisan migrate

Please make sure you’ve set up correct database credentials in .env file.


##start your server:

php artisan serve

Your Laravel project is now up and running!




#### ARMS Zone Details & Queries ####
1.	Replaced SQLite database and MySQL database is configured now.
2.	Please Provide Google Login client secret with client id 
3.	Please provide valid service account private key(should be in a json file) and we will placed it to /public folder as name 'gcs_key.json';
4.	Please provide valid storage bucket name with Read/Write permissions for the user.
5.	For new APIs(for DEV), add user 'alcyonedeveloper@gmail.com' to login.
6.	Please grant access to 'http://local.zone.com' (for DEV) with google map key
7.	Please grant access to 'http://local.zone.com/callback/google' (for DEV) with google login service account.


