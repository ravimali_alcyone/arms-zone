<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['guest']], function () {
	Route::get('/', function () {
        return redirect('/login');
    });
});

Route::get('/auth/redirect/{provider}', 'LoginController@redirect');
Route::get('/callback/{provider}', 'LoginController@callback');
Route::get('lang/{param}/{page}', 'LoginController@changeLanguage'); 


Route::get('logout', 'LoginController@logout')->name('logout');

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
	Route::get('login2', 'ZoneController@index')->name('login2');
	Route::get('zone', 'ZoneController@zone')->name('zone');
	Route::get('predict', 'ZoneController@predict')->name('predict');

	Route::post('get-from-db', 'ZoneController@getDataFromAPI')->name('get-from-db');
	Route::post('get-delivery-files', 'ZoneController@getDeliveryFiles')->name('get-delivery-files');
	Route::post('get-from-delivery-file', 'ZoneController@getDataFromDeliveryFile')->name('get-from-delivery-file');
	//Route::post('get-data-from-local-db', 'ZoneController@getDataFromLocalDB')->name('get-data-from-local-db');
	Route::post('save-delivery-file', 'ZoneController@saveDeliveryFile')->name('save-delivery-file');
	
	Route::post('get-model-files', 'ZoneController@getModelFiles')->name('get-model-files');
	Route::post('get-model-file-data', 'ZoneController@getDataFromModelFile')->name('get-model-file-data');		
	Route::post('save-model-file', 'ZoneController@saveModelFile')->name('save-model-file');

	Route::post('get-data-for-predict', 'ZoneController@getDataForPredict')->name('get-data-for-predict');
	
	Route::post('save-map-data', 'ZoneController@saveMapData')->name('save-map-data');
	
	Route::post('save-export-files', 'ZoneController@ExportCSVZoneWise')->name('save-export-files');
	
	Route::post('set-default-options', 'ZoneController@setDefaultOptions')->name('set-default-options');
	
	Route::post('get-deliveries-by-warhouse', 'ZoneController@getDeliveriesByWarehouse')->name('get-deliveries-by-warhouse');
	Route::post('get-warhouse-markers', 'ZoneController@getWarehouseMarkers')->name('get-warhouse-markers');
	Route::post('get-deliveries-by-day', 'ZoneController@getDeliveriesByDay')->name('get-deliveries-by-day');
	Route::post('get-heatmap-deliveries-by-units', 'ZoneController@getHeatmapDeliveriesByUnits')->name('get-heatmap-deliveries-by-units');
	
	Route::get('export', 'ZoneController@export')->name('export');
	Route::get('export/{id}', 'ZoneController@export');
	
}); 