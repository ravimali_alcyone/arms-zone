@extends('layouts.header')
@section('content')
<?php
$cw_json = 'NULL';
$cw_data = array();
if(Session::get('user')){
	$cw_json = $data['user']['user_warehouse_accesss_list'];
	if($cw_json != ''){
		$cw_data = json_decode($cw_json,true);
	}
}
?>

    <!-- Login Section -->
    <div class="login-section-wrapper">
        <section>
            <img src="{{ asset('images/logo.png') }}" alt="logo" width="76%" class="d-block m-auto">

            <div class="google-btn-wrapper text-center mt-5">
				<?php
					$user  = Session::get('getInfo');
					if($user) {
					?>
					<p class="loggedin-user-name font-14"><?php echo $user['name'];?></p>
				<?php } else {?>
                <a href="{{ url('/auth/redirect/google') }}" class="google-btn">
                    <img src="{{ asset('images/google-logo.png') }}" alt="google-logo" class="google-logo mr-3"> {{ __('messages.login.google_login') }}
                </a>
                <p class="mt-4 font-14">{{ __('messages.login.dontHaveAnAccount') }} <a href="mailto:contactoarms@arete.ws" class="text-orange">{{ __('messages.login.contactArms') }}</a></p>
                <?php } ?>
                @foreach ($errors->all() as $error)
				  <div>{{ $error }}</div>
				@endforeach
            </div>

            <form action="#" class="login-form" id="setDefaultOptions">
                <div>
                    <div class="form-group custom-select-box-wrapper position-relative d-inline-block">
                        <select class="form-control" name="company" required="required" <?php if(!$user){?>disabled="disabled"<?php } else{?> onchange="onSelectCompany(this)" <?php } ?>
						oninvalid="this.setCustomValidity(returnRquiredMessage())" oninput="setCustomValidity('')">
                            <option value="">{{ __('messages.login.company') }}</option>
							<?php if($cw_data){
									foreach($cw_data as $key => $cw){?>
									<option value="<?php echo $key;?>"><?php echo $key;?></option>
							<?php } }?>
                        </select>
                    </div>
                    <div class="form-group custom-select-box-wrapper position-relative d-inline-block">
                        <select class="form-control" name="warehouse" required="required" <?php if(!$user){?>disabled="disabled"<?php } ?> onchange="onSelectWarehouseModel(this)"
						oninvalid="this.setCustomValidity(returnRquiredMessage())" oninput="setCustomValidity('')">
                            <option value="">{{ __('messages.login.warehouse') }}</option>
                        </select>
                    </div>
                    <div class="form-group custom-select-box-wrapper position-relative d-inline-block">
                        <select class="form-control" <?php if(!$user){?>disabled="disabled"<?php } ?> name="fileName">
                            <option value="">{{ __('messages.login.model') }}</option>
                        </select>
                    </div>
                </div>
				<input name="model_check_type" type="hidden" value="replace">
                <button type="submit" class="custom-btn mt-3" <?php if(!$user){?>disabled="disabled"<?php } ?>>{{ __('messages.login.continue') }}</button>
            </form>
        </section>
    </div>
<script>
$(".loader").css('display', 'none');

	function onSelectCompany(e){
		var comp_ware_data = <?php echo $cw_json;?>;
		var wareData = "<option value=''><?php echo __('messages.login.warehouse');?></option>";
		var company = e.value;
		var wt = $(e).parent().next().find('select');
		if(company !== ''){
			var ware_data = comp_ware_data[company];
			$.each(ware_data, function(key, val) {
				wareData += "<option value='"+val+"'>"+val+"</option>";
			});
		}
		wt.html(wareData);
    }

	function onSelectWarehouseModel(e){
		$(".loader").css('display', 'flex');
		var JData = "<option value=''><?php echo __('messages.zone.model');?></option>";
		var warehouse_val = e.value;
		var company_val = $(e).parent().prev().find('select').val();
		var formName = $(e).closest("form");
		var jt = $(e).parent().next().find('select');
		if(warehouse_val !== '' && company_val !== ''){
			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/get-model-files',
				data:{'company':company_val, 'warehouse': warehouse_val},
				success:function(response){
					response = JSON.parse(response);
					if(response['status'] === true){
						let fileNames = response['data'];
						$.each(fileNames, function(key, val) {
							JData += "<option value='"+val+"'>"+val+"</option>";
						});
						jt.html(JData);
					}
					$(".loader").css('display', 'none');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		}else{
			$(".loader").css('display', 'none');
			jt.html(JData);
		}
	}

	function returnRquiredMessage() {
		return "{{__('messages.zone.file_required_warning')}}";
	}

    // Change Select-box Arrows
    $(document).ready(function() {
        $(".custom-select-box-wrapper>select").click(function() {
            $(".custom-select-box-wrapper").removeClass('active');
            $(this).closest('.custom-select-box-wrapper').addClass('active');
        });
        $(".custom-select-box-wrapper>select").focusout(function() {
            $(".custom-select-box-wrapper").removeClass('active');
        });

        // For Color Picker
        $(".color-picker-btn>img").click(function() {
            if ($(this).closest('.picker-wrapper').find('.color-picker').is(":visible")) {
                $(this).closest('.picker-wrapper').find('.color-picker').fadeOut();
            } else {
                $(this).closest('.picker-wrapper').find('.color-picker').fadeIn();
            }
        });

		//Set default options for further screens in session
		$("#setDefaultOptions").submit(function(e){
			e.preventDefault();
			$(".loader").css('display', 'flex');
			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/set-default-options',
				data:$("#setDefaultOptions").serialize(),
				success:function(response){
					$(".loader").css('display', 'none');
					response = JSON.parse(response);
					if(response['status'] === true){
						successAlert("{{ __('messages.login.login_successful') }}");
						setTimeout(function(){ window.location = '<?php echo route('zone');?>'; }, 500);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		});
    });
</script>
@endsection
