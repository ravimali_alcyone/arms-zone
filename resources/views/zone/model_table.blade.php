<?php if($modelData){
	foreach($modelData as $row){
		$days = $row['day_name'];
		$DayNames = '';
		if($days){
			$DayArr = array();
			foreach($days as $key => $day){
				$d = __("messages.days.$day");
				array_push($DayArr,$d);
			}
			if($DayArr){
				$DayNames = implode(', ',$DayArr);
			}
		}
?>
<tr>
	<td><?php echo $row['zone_name'].' '.$row['comment1'];?></td>
	<td><?php echo number_format($row['delivery_count']);?></td>
	<td><?php echo number_format(((float)$row['order_weight']),2);?></td>
	<td><?php echo number_format(((float)$row['order_volume']),2);?></td>
	<td><?php echo number_format($row['order_pieces']);?></td>
	<td><?php echo number_format(((float)$row['order_cost_goods']),2);?></td>
	<td><?php if($days){ echo $DayNames; }?></td>
	<td><?php echo number_format(((float)$row['comment2']),2);?></td>
</tr>
<?php }  } ?>