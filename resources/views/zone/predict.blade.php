@extends('layouts.header')
@section('content')

@php
    $cw_json = '';
    $cw_data = array();
    $user  = Session::get('getInfo');
@endphp

@if(Session::get('user'))
    @php
        $cw_json = $data['user']['user_warehouse_accesss_list'];
    @endphp
    @if($cw_json != '')
        @php
            $cw_data = json_decode($cw_json,true);
        @endphp
	@endif
@endif

<!-- Predict Main Section -->
<div class="predict-main-wrapper">
    <div class="container-fluid pr-0">
        <div class="row mr-0">
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 pr-0">
                <div class="predict-left-section">
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="accordianOne">
                                <button class="btn btn-link active" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <p class="sub-heading text-white mb-0">{{ __('messages.zone.model') }}</p>
                                </button>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="accordianOne" data-parent="#accordion">
                                <div class="card-body">
                                    <form id="getModelFileData" class="mt-3" >
                                        <div class="form-group custom-select-box-wrapper position-relative">
                                            <select class="form-control" onchange="onSelectCompany(this)" name="company" required="required"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
                                                <option>{{ __('messages.zone.company') }}</option>
												<?php if($cw_data){
														foreach($cw_data as $key => $cw){?>
														<option value="<?php echo $key;?>" <?php if($data['default_options'] && $data['default_options']['my_company'] == $key){ echo 'selected';}?> ><?php echo $key;?></option>
												<?php } }?>
                                            </select>
                                        </div>
                                        <div class="form-group custom-select-box-wrapper position-relative">
                                            <select class="form-control" onchange="onSelectWarehouseModel(this)" name="warehouse" required="required"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
                                                <option value="">{{ __('messages.zone.warehouse') }}</option>
												<?php if($data['default_options'] && $data['default_options']['my_warehouse'] != ''){

												$wArr = $cw_data[$data['default_options']['my_company']];
												if($wArr){
													foreach($wArr as $w => $val){
												?>
													<option value="<?php echo $val;?>" <?php if($data['default_options']['my_warehouse'] == $val){ echo 'selected';}?>><?php echo $val;?>
													</option>
												<?php } } } ?>
                                            </select>
                                        </div>
                                        <div class="form-group custom-select-box-wrapper position-relative">
                                            <select class="form-control model-files-dropdown" name="fileName" required="required"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
                                               <option value="">{{ __('messages.zone.model') }}</option>
                                            </select>
                                        </div>
                                        <div class="text-center mt-4">
                                            <button type="submit" class="custom-btn">{{ __('messages.zone.open') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="predict-delivery-section">
                        <p class="sub-heading text-center">{{ __('messages.zone.delivery_date') }}</p>
                        <form action="javascript: searchOnEnter();" class="mt-3">
                            <div class="comments-field">
                                <input type="text" id="address" placeholder="{{ __('messages.zone.zip_or_address') }}">
                            </div>
                            <div class="text-center mt-4">
                                <button type="button" class="custom-btn" id="findBtn">{{ __('messages.zone.search') }}</button>
                            </div>
                        </form>

                        <div class="text-center mt-5" id="detectResult">

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8 pl-0 pr-0">
                <div class="google-map" id="predictMap"></div>
            </div>
        </div>
    </div>
</div>

@include('partials.predict.javascript')

<script>
    // Set Warehouse Options
    function onSelectCompany(e){
		var comp_ware_data = <?php echo $cw_json;?>;
		var wareData = "<option value=''><?php echo __('messages.login.warehouse');?></option>";
		var company = e.value;
		var wt = $(e).parent().next().find('select');
		if(company !== ''){
			var ware_data = comp_ware_data[company];
			$.each(ware_data, function(key, val) {
				wareData += "<option value='"+val+"'>"+val+"</option>";
			});
		}
		wt.html(wareData);
    }

	function onSelectWarehouseModel(e){
		$(".loader").css('display', 'flex');
		var JData = "<option value=''><?php echo __('messages.zone.model');?></option>";
		var warehouse_val = e.value;
		var company_val = $(e).parent().prev().find('select').val();
		var formName = $(e).closest("form");
		var jt = $(e).parent().next().find('select');
		if(warehouse_val !== '' && company_val !== ''){
			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/get-model-files',
				data:{'company':company_val, 'warehouse': warehouse_val},
				success:function(response){
					response = JSON.parse(response);
					if(response['status'] === true){
						let fileNames = response['data'];
						$.each(fileNames, function(key, val) {
							JData += "<option value='"+val+"'>"+val+"</option>";
						});
						jt.html(JData);
					}
					$(".loader").css('display', 'none');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		}else{
			$(".loader").css('display', 'none');
			jt.html(JData);
		}
	}

    //Open Selected Json file of model from bucket and load data for predict
    $("#getModelFileData").submit(function(e){
        e.preventDefault();
		$(".loader").css('display', 'flex');
        $.ajax({
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/get-data-for-predict',
            data:$("#getModelFileData").serialize(),
            success:function(response){
                $(".loader").css('display', 'none');
                response = JSON.parse(response);
                if(response['status'] === true){
					// successAlert("{{ __('messages.zone.success_heading') }}", "{{ __('messages.zone.predict_success_msg') }}");
                    var data = response['data'];
                    //console.log(data);
                    readJson(data);
                }
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
        });
    });

	function displayMarkerDetectInfo(shapeInfo){
		//console.log('shapeInfo',shapeInfo);
		let myhtml = "";
		if(markerDetectStatus === true){
			if(shapeInfo){
				let comment1 = shapeInfo.comment1;
				let comment2 = shapeInfo.comment2;
				let prefix = "{{ __('messages.zone.other_day_cost') }}";
				if(comment1 == null && comment2 == '0'){
					myhtml = "<p class='comment text-danger'><?php echo __('messages.zone.no_info');?></p>";
				}else{
					myhtml = "<p class='comment text-green'>"+comment1+"</p>";
					myhtml += "<p class='comment text-yellow'>"+prefix+' $'+comment2+"</p>";
				}
			}else{
				myhtml = "";
			}
		}else{
			myhtml = "<p class='comment text-danger'><?php echo __('messages.zone.out_of_zone');?></p>";
		}
		$("#detectResult").html(myhtml);
	}

	// Get Titles for Map Controls
	function getMapTitles() {
		let address_not_found = "{{ __('messages.zone.address_not_found') }}";
		let Titles = {
			"address_not_found": address_not_found
		};
		return Titles;
	}
	function returnRequiredMessage() {
		return "{{__('messages.zone.file_required_warning')}}";
	}

	// Search action fire on press Enter key
	function searchOnEnter() {
		$("#findBtn").trigger('click');
	}
</script>

<?php if($data['default_options'] && $data['default_options']['my_company'] != '' && $data['default_options']['my_warehouse'] != ''){ ?>
<script>
	function defaultSelectedWarehouseModel(company,warehouse,model){
		$(".loader").css('display', 'flex');
		var JData = "<option value=''><?php echo __('messages.zone.model');?></option>";
		var warehouse_val = warehouse;
		var company_val = company;
		var jt = $('select.model-files-dropdown');
		if(warehouse_val !== '' && company_val !== ''){

			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/get-model-files',
				data:{'company':company_val, 'warehouse': warehouse_val},
				success:function(response){
					response = JSON.parse(response);
					if(response['status'] === true){
						let fileNames = response['data'];
						$.each(fileNames, function(key, val) {
							if(val === model){
								JData += "<option value='"+val+"' selected>"+val+"</option>";
							}else{
								JData += "<option value='"+val+"'>"+val+"</option>";
							}

						});
						jt.html(JData);
						$(".loader").css('display', 'none');
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		}else{
			jt.html(JData);
			$(".loader").css('display', 'none');
		}
	}

	defaultSelectedWarehouseModel("<?php echo $data['default_options']['my_company'];?>","<?php echo $data['default_options']['my_warehouse'];?>","<?php echo $data['default_options']['my_model'];?>");
</script>
<?php }else{ ?>
<script>
$(".loader").css('display', 'none');
</script>
<?php } ?>

@endsection