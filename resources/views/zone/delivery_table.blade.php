<tr>
	<td>{{ __('messages.zone.orders') }}</td>
	<td><?php echo number_format($data['total_deliveries']);?></td>
</tr>
<tr>
	<td>{{ __('messages.zone.unique_destinations') }}</td>
	<td><?php echo number_format($data['total_unique_destinations']);?></td>
</tr>
<tr>
	<td>$ {{ __('messages.zone.delivery_cost') }}</td>
	<td><?php echo number_format($data['total_deliveriey_cost'],2);?></td>
</tr>
<tr>
	<td>{{ __('messages.zone.delivery_vehicles') }}</td>
	<td><?php echo number_format($data['total_trips']);?></td>
</tr>
<tr>
	<td>{{ __('messages.zone.km') }}</td>
	<td><?php echo number_format($data['total_kms'],2);?></td>
</tr>
<tr>
	<td>{{ __('messages.zone.pieces') }}</td>
	<td><?php echo number_format($data['total_pieces']);?></td>
</tr>
<tr>
	<td>$ {{ __('messages.zone.goods') }}</td>
	<td><?php echo number_format($data['total_goods'],2);?></td>
</tr>
<tr>
	<td>{{ __('messages.zone.kg') }}</td>
	<td><?php echo number_format($data['total_weight'],2);?></td>
</tr>
<tr>
	<td>{{ __('messages.zone.m3') }}</td>
	<td><?php echo number_format($data['total_volume'],2);?></td>
</tr>
<tr>
	<td>% {{ __('messages.zone.volume_occupation') }}</td>
	<td><?php echo number_format($data['total_per_volume_occupation'],2);?></td>
</tr>
<tr>
	<td>% {{ __('messages.zone.weight_occupation') }}</td>
	<td><?php echo number_format($data['total_per_weight_occupation'],2);?></td>
</tr>