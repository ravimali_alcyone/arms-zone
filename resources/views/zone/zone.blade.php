@extends('layouts.header')
@section('content')

@php
    $cw_json = '';
    $cw_data = array();
    $user  = Session::get('getInfo');
@endphp

@if(Session::get('user'))
    @php
        $cw_json = $data['user']['user_warehouse_accesss_list'];
    @endphp
    @if($cw_json != '')
        @php
            $cw_data = json_decode($cw_json,true);
        @endphp
	@endif
@endif

<!-- Main Section -->
<div class="zones-main-wrapper">
	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade show active" id="nav-open" role="tabpanel" aria-labelledby="nav-open-tab">
			<section class="open-tab-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
							<div class="section visits-section section-1 mt-0">
								<p class="sub-heading text-center mb-0">{{ __('messages.zone.orders') }}</p>
								<div class="visits-form-wrapper">
									<div class="sub-form">
										<p class="text-center font-14 mb-0">{{ __('messages.zone.file') }}</p>
										<form action="#" id="getDataFromFile">
											<div class="form-group custom-select-box-wrapper position-relative">
												<select class="form-control" name="company" required="required" onchange="onSelectCompany(this)"
													oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
													<option value="">{{ __('messages.zone.company') }}</option>
													<?php if($cw_data){
														foreach($cw_data as $key => $cw){?>
															<option value="<?php echo $key;?>" <?php if($data['default_options'] && $data['default_options']['my_company'] == $key){ echo 'selected';}?> ><?php echo $key;?></option>
													<?php } }?>
												</select>
											</div>
											<div class="form-group custom-select-box-wrapper position-relative">
												<select class="form-control warehouseSelected" name="warehouse" required="required" onchange="onSelectWarehouseDelivery(this)"
													oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
													<option value="">{{ __('messages.zone.warehouse') }}</option>

													<?php if($data['default_options'] && $data['default_options']['my_warehouse'] != ''){

													$wArr = $cw_data[$data['default_options']['my_company']];
													if($wArr){
														foreach($wArr as $w => $val){
													?>
														<option value="<?php echo $val;?>" <?php if($data['default_options']['my_warehouse'] == $val){ echo 'selected';}?>><?php echo $val;?>
														</option>
													<?php } } } ?>
												</select>
											</div>
											<div class="form-group custom-select-box-wrapper position-relative">
												<select class="form-control delivery-files-dropdown" name="fileName" required="required"
													oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
												<option value="">{{ __('messages.zone.file') }}</option>
											</select>
											</div>
											<div class="radio-wrapper">
												<div class="form-group">
													<input id="add-1" class="radio-custom" name="delivery_check_type" type="radio" value="add" checked>
													<label for="add-1" class="radio-custom-label font-12">{{ __('messages.zone.add') }}</label>
												</div>
												<div class="form-group">
													<input id="replace-1" class="radio-custom" name="delivery_check_type" value="replace" type="radio">
													<label for="replace-1" class="radio-custom-label font-12">{{ __('messages.zone.replace') }}</label>
												</div>
											</div>
											<div class="text-center">
												<button type="submit" class="custom-btn mt-3">{{ __('messages.zone.open') }}</button>
											</div>
										</form>
									</div>

									<div class="sub-form">
										<p class="text-center font-14 mb-0">{{ __('messages.zone.database') }}</p>
										<form action="#" id="getDataFromDB">
											<div class="form-group custom-select-box-wrapper position-relative">
												<select class="form-control" name="company" required="required" onchange="onSelectCompany(this)"
													oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
												<option value="">{{ __('messages.zone.company') }}</option>
												<?php if($cw_data){
														foreach($cw_data as $key => $cw){?>
														<option value="<?php echo $key;?>" <?php if($data['default_options'] && $data['default_options']['my_company'] == $key){ echo 'selected';}?>><?php echo $key;?></option>
												<?php } }?>
												</select>
											</div>
											<div class="form-group custom-select-box-wrapper position-relative">
												<select class="form-control warehouseSelected" name="warehouse" required="required"
													oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
													<option value="">{{ __('messages.zone.warehouse') }}</option>

													<?php if($data['default_options'] && $data['default_options']['my_warehouse'] != ''){

													$wArr = $cw_data[$data['default_options']['my_company']];
													if($wArr){
														foreach($wArr as $w => $val){
													?>
														<option value="<?php echo $val;?>" <?php if($data['default_options']['my_warehouse'] == $val){ echo 'selected';}?>><?php echo $val;?>
														</option>
													<?php } } } ?>

												</select>
											</div>
											<div class="input-date-wrapper form-group position-relative">
												<div class="input-group date start_date">
													<input title="" id="start_date" type="text" class="form-control date position-relative" placeholder="{{ __('messages.zone.dd_mm_yyyy') }}" name="from_date" required="required"
												oninvalid="this.setCustomValidity(returnInputRequiredMessage())" oninput="setCustomValidity('')">
													<span class="input-group-addon">
														<i class="glyphicon glyphicon-th"></i>
													</span>
												</div>	

												<div class="input-group date end_date">
													<input title="" id="end_date" type="text" class="form-control date position-relative" placeholder="{{ __('messages.zone.dd_mm_yyyy') }}" name="to_date" required="required"
													oninvalid="this.setCustomValidity(returnInputRequiredMessage())" oninput="setCustomValidity('')">
													<span class="input-group-addon">
														<i class="glyphicon glyphicon-th"></i>
													</span>
												</div>											
											</div>
											<div class="radio-wrapper">
												<div class="form-group">
													<input id="add-2" class="radio-custom" name="db_check_type" type="radio" value="add" checked>
													<label for="add-2" class="radio-custom-label font-12">{{ __('messages.zone.add') }}</label>
												</div>
												<div class="form-group">
													<input id="replace-2" class="radio-custom" name="db_check_type" type="radio" value="replace">
													<label for="replace-2" class="radio-custom-label font-12">{{ __('messages.zone.replace') }}</label>
												</div>
											</div>
											<div class="text-center">
												<button type="submit" class="custom-btn mt-3">{{ __('messages.zone.load') }}</button>
											</div>
										</form>
									</div>
								</div>
							</div>

							<div class="comments-field">
								<input type="text" placeholder="{{ __('messages.zone.comments') }}" name="comments" id="deliveryComment" value="<?php if($data['delivery_data_comments']){echo $data['delivery_data_comments'];}?>" >
							</div>

							<table class="deliveryTableData table custom-table visits-table">
								<thead>
									<tr>
										<th></th>
										<th>{{ __('messages.zone.total') }}</th>
									</tr>
								</thead>
								<tbody>
								<?php if($data['deliveryTableData']){ echo $data['deliveryTableData']; }?>
								</tbody>
							</table>
						</div>

						<div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
							<div class="section visits-section">
								<p class="sub-heading text-center">{{ __('messages.zone.zones2') }}</p>
								<div class="zones-form-wrapper">
									<form action="#" id="getModelFileData">
										<div class="form-group custom-select-box-wrapper position-relative">
											<select class="form-control" name="company" required="required" onchange="onSelectCompany(this)"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
												<option value="">{{ __('messages.zone.company') }}</option>
										<?php if($cw_data){
												foreach($cw_data as $key => $cw){?>
												<option value="<?php echo $key;?>" <?php if($data['default_options'] && $data['default_options']['my_company'] == $key){ echo 'selected';}?>><?php echo $key;?></option>
										<?php } }?>
											</select>
										</div>
										<div class="form-group custom-select-box-wrapper position-relative">
											<select class="form-control" name="warehouse" required="required" onchange="onSelectWarehouseModel(this)"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
												<option value="">{{ __('messages.zone.warehouse') }}</option>
												<?php if($data['default_options'] && $data['default_options']['my_warehouse'] != ''){

												$wArr = $cw_data[$data['default_options']['my_company']];
												if($wArr){
													foreach($wArr as $w => $val){
												?>
													<option value="<?php echo $val;?>" <?php if($data['default_options']['my_warehouse'] == $val){ echo 'selected';}?>><?php echo $val;?>
													</option>
												<?php } } } ?>
											</select>
										</div>
										<div class="form-group custom-select-box-wrapper position-relative">
											<select class="form-control model-files-dropdown" name="fileName" required="required"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
												<option value="">{{ __('messages.zone.model') }}</option>
											</select>
										</div>
										<div class="radio-wrapper">
											<div class="form-group">
												<input id="add-3" class="radio-custom" name="model_check_type" type="radio" value="add" checked>
												<label for="add-3" class="radio-custom-label font-12">{{ __('messages.zone.add') }}</label>
											</div>
											<div class="form-group ml-4">
												<input id="replace-3" class="radio-custom" name="model_check_type" type="radio" value="replace">
												<label for="replace-3" class="radio-custom-label font-12">{{ __('messages.zone.replace') }}</label>
											</div>
										</div>
										<div class="text-center mt-3">
											<button type="submit" class="custom-btn">{{ __('messages.zone.open') }}</button>
										</div>
									</form>
								</div>
							</div>

							<div class="comments-field">
								<input type="text" placeholder="{{ __('messages.zone.comments') }}" name="comments" id="modelComment" value="<?php if($data['model_data_comments']){echo $data['model_data_comments'];}?>" >
							</div>

							<table class="modelTableData table custom-table open-model-table">
								<thead>
									<tr>
										<th>{{ __('messages.zone.zone_free') }}</th>
										<th>{{ __('messages.zone.orders') }}</th>
										<th>Kg</th>
										<th>m3</th>
										<th>{{ __('messages.zone.pieces') }}</th>
										<th>$ {{ __('messages.zone.goods') }}</th>
										<th>{{ __('messages.zone.day_of_delivery') }}</th>
										<th>{{ __('messages.zone.charge') }}</th>
									</tr>
								</thead>
								<tbody>
								<?php if($data['modelTableData']){ echo $data['modelTableData']; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="tab-pane fade" id="nav-zones" role="tabpanel" aria-labelledby="nav-zones-tab">
			<section class="zones-tab-content">
				<div class="container-fluid pr-0">
					<div class="row mr-0">
						<div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 pr-0">
							<div class="left-section-wrapper">
								<div id="accordion">
									<div class="card">
										<div class="card-header" id="accordianOne">
											<button class="btn btn-link active" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												<p class="sub-heading text-white text-left mb-0">{{ __('messages.zone.orders') }}</p>
											</button>
										</div>
										<div id="collapseOne" class="collapse show" aria-labelledby="accordianOne">
											<div class="card-body">
												<div class="visits-table">
													<table class="deliveryTableData table custom-table visits-table mt-0 mb-0">
														<thead>
															<tr>
																<th></th>
																<th>{{ __('messages.zone.total') }}</th>
															</tr>
														</thead>

														<tbody>
															<?php if($data['deliveryTableData']){ echo $data['deliveryTableData']; }?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>

									<div class="card mt-2">
										<div class="card-header" id="accordianTwo">
											<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
												<p class="sub-heading text-white text-left mb-0">{{ __('messages.zone.number_of_zones') }}(<span class="zone_count"><?php if($data['modelTableData']){ echo $data['zone_count']; }else{ echo '0';}?></span>)</p>
											</button>
										</div>
										<div id="collapseTwo" class="collapse" aria-labelledby="accordianTwo">
											<div class="card-body">
												<div class="zones-table">
													<table class="modelTableData table custom-table visits-table table-responsive mt-0">
														<thead>
															<tr>
																<th>{{ __('messages.zone.zone_free') }}/</th>
																<th>{{ __('messages.zone.orders') }}</th>
																<th>Kg</th>
																<th>m3</th>
																<th>{{ __('messages.zone.pieces') }}</th>
																<th>$ {{ __('messages.zone.goods') }}</th>
																<th>{{ __('messages.zone.day_of_delivery') }}</th>
																<th>{{ __('messages.zone.charge') }}</th>
															</tr>
														</thead>
														<tbody>
														<?php if($data['modelTableData']){ echo $data['modelTableData']; }?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>

									<div class="card mt-2">
										<div class="card-header" id="accordianThree">
											<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
												<p class="sub-heading text-white text-left mb-0">{{ __('messages.zone.selected_shape') }}</p>
											</button>
										</div>
										<div id="collapseThree" class="collapse" aria-labelledby="accordianThree">
											<div class="card-body">
												<div class="selected-shape-wrapper position-relative">
													<section>
														<div>
															<div class="selected-shape-top-section">
																<div class="selected-shape-name-wrapper form-group position-relative">
																	<!-- <select class="" id="selected-shape" disabled>
																		<option value="rectangle">{{ __('messages.zone.rectangle') }}</option>
																		<option value="circle">{{ __('messages.zone.circle') }}</option>
																		<option value="polygon">{{ __('messages.zone.polygon') }}</option>
																	</select> -->
																	<p class="rectangle-title">{{ __('messages.zone.rectangle') }}</p>
																	<p class="circle-title">{{ __('messages.zone.circle') }}</p>
																	<p class="polygon-title">{{ __('messages.zone.polygon') }}</p>
																</div>

																<span class="fill-drip-icon color-picker-btn picker-wrapper">
																	<img src="{{ asset('images/icon-color-fill.png') }}" alt="icon-color-fill">
																	<div class="color-picker bg-picker">
																		<i class="yellowgreen" title="Yellowgreen" style="background: yellowgreen;"></i>
																		<i class="greenyellow" title="Greenyellow" style="background: greenyellow;"></i>
																		<i class="green" title="Green" style="background: green;"></i>
																		<i class="lightgreen" title="Lightgreen" style="background: lightgreen;"></i>
																		<i class="teal" title="Teal" style="background: teal;"></i>
																		<i class="gray" title="Gray" style="background: gray;"></i>
																		<i class="lightgray" title="Lightgray" style="background: lightgray;"></i>
																		<i class="pink" title="Pink" style="background: pink;"></i>
																		<i class="tomato" title="Tomato" style="background: tomato;"></i>
																		<i class="chocolate" title="Chocolate" style="background: chocolate;"></i>
																		<i class="coral" title="Coral" style="background: coral;"></i>
																		<i class="orange" title="Orange" style="background: orange;"></i>
																		<i class="dodgerblue" title="Dodgerblue" style="background: dodgerblue;"></i>
																		<i class="lightblue" title="Lightblue" style="background: lightblue;"></i>
																		<i class="skyblue" title="Skyblue" style="background: skyblue;"></i>
																		<i class="blue" title="Blue" style="background: blue;"></i>
																		<i class="aqua" title="Aqua" style="background: aqua;"></i>
																		<i class="black" title="Black" style="background: black;"></i>
																		<i class="darkred" title="Darkred" style="background: darkred;"></i>
																		<i class="purple" title="Purple" style="background: purple;"></i>
																		<i class="navy" title="Navy" style="background: navy;"></i>
																		<i class="white" title="White" style="background: white; border: 1px solid gray"></i>
																		<i class="lime" title="Lime" style="background: lime;"></i>
																		<i class="yellow" title="Yellow" style="background: yellow;"></i>
																		<i class="red" title="Red" style="background: red;"></i>
																	</div>
																</span>

																<span class="color-picker-btn picker-wrapper">
																	<img src="{{ asset('images/icon-a.png') }}" alt="icon-a">
																	<div class="color-picker text-picker">
																	<i class="yellowgreen" title="Yellowgreen" style="background: yellowgreen;"></i>
																	<i class="greenyellow" title="Greenyellow" style="background: greenyellow;"></i>
																	<i class="green" title="Green" style="background: green;"></i>
																	<i class="lightgreen" title="Lightgreen" style="background: lightgreen;"></i>
																	<i class="teal" title="Teal" style="background: teal;"></i>
																	<i class="gray" title="Gray" style="background: gray;"></i>
																	<i class="lightgray" title="Lightgray" style="background: lightgray;"></i>
																	<i class="pink" title="Pink" style="background: pink;"></i>
																	<i class="tomato" title="Tomato" style="background: tomato;"></i>
																	<i class="chocolate" title="Chocolate" style="background: chocolate;"></i>
																	<i class="coral" title="Coral" style="background: coral;"></i>
																	<i class="orange" title="Orange" style="background: orange;"></i>
																	<i class="dodgerblue" title="Dodgerblue" style="background: dodgerblue;"></i>
																	<i class="lightblue" title="Lightblue" style="background: lightblue;"></i>
																	<i class="skyblue" title="skyblue" style="background: skyblue;"></i>
																	<i class="blue" title="Blue" style="background: blue;"></i>
																	<i class="aqua" title="Aqua" style="background: aqua;"></i>
																	<i class="black" title="Black" style="background: black;"></i>
																	<i class="darkred" title="Darkred" style="background: darkred;"></i>
																	<i class="purple" title="Purple" style="background: purple;"></i>
																	<i class="navy" title="Navy" style="background: navy;"></i>
																	<i class="white" title="White" style="background: white; border: 1px solid gray"></i>
																	<i class="lime" title="Lime" style="background: lime;"></i>
																	<i class="yellow" title="Yellow" style="background: yellow;"></i>
																	<i class="red" title="Red" style="background: red;"></i>
																	</div>
																</span>
															</div>

															<form action="#" id="updateZoneForm-1" class="left-side-form float-form mt-3">
																<div class="form-group position-relative">
																	<input type="text" class="form-control" id="zoneName">
																	<label for="zone" class="text-orange font-14">{{ __('messages.zone.zone') }}</label>
																</div>
																<div class="form-group position-relative">
																	<input type="text" class="form-control" id="comment1">
																	<label for="comment1" class="text-orange font-14">{{ __('messages.zone.delivery_day') }}</label>
																</div>
																<div class="form-group position-relative">
																	<input type="number" class="form-control" id="comment2">
																	<label for="comment2" class="text-orange font-14">{{ __('messages.zone.delivery_charge_other_days') }}</label>
																</div>
															</form>
														</div>

														<div class="mt-3">
															<form action="#" id="updateZoneForm-2" class="right-side-form form-inline">
																<div class="form-group position-relative">
																	<label for="xLatitude" class="text-orange font-12">X {{ __('messages.zone.latitude') }}</label>
																	<input type="text" class="form-control" id="xLatitude" readonly="readonly">
																</div>
																<div class="form-group position-relative mt-3">
																	<label for="length" class="text-orange font-12">{{ __('messages.zone.length') }}</label>
																	<input type="text" class="form-control" id="length" readonly="readonly">
																</div>
																<div class="form-group position-relative mt-3">
																	<label for="height" class="text-orange font-12">{{ __('messages.zone.height') }}</label>
																	<input type="text" class="form-control" id="height" >
																</div>
																<div class="form-group position-relative mt-3">
																	<label for="width" class="text-orange font-12">{{ __('messages.zone.width') }}</label>
																	<input type="text" class="form-control" id="width">
																</div>
																<div class="form-group position-relative mt-3">
																	<label for="radius" class="text-orange font-12">{{ __('messages.zone.radius') }}</label>
																	<input type="text" class="form-control" id="radius">
																</div>
																<div class="form-group position-relative mt-3">
																	<label for="polygone" class="text-orange font-12">{{ __('messages.zone.polygon') }}</label>
																	<input type="text" class="form-control" id="polygone" readonly="readonly">
																</div>

															</form>
														</div>
														<span class="total-area"></span>
													</section>
												</div>
												<div class="text-center mt-3">
													<button class="custom-btn" id="updateZoneBtn">{{ __('messages.zone.update') }}</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="map-wrapper col-sm-12 col-md-12 col-lg-8 col-xl-8 p-0">
							<div class="google-map" id="map"></div>
							<div class="map-popup-option font-14"><span>{{ __('messages.zone.show') }}</span></div>

							<div class="map-main-popup">
								<form action="#">
									<p class="title mt-0">{{ __('messages.zone.warehouse') }}</p>
									<div class="position-relative">
										<p class="mb-0 radio-wrapper-round">
											<input type="radio" id="test1" name="warehouseType" checked="checked" value="all" class="deliveryWarehouseChk" onchange="getWarehouseMarkers();">
											<label for="test1">{{ __('messages.zone.all') }}</label>
										</p>
										<p class="mb-0 radio-wrapper-round">
											<input type="radio" id="test2" name="warehouseType" value="selected" class="deliveryWarehouseChk" onchange="getWarehouseMarkers();">
											<label for="test2">{{ __('messages.zone.selected') }}</label>
										</p>
										<p class="mb-0 radio-wrapper-round border-bottom-0">
											<input type="radio" id="test3" name="warehouseType" value="none" class="deliveryWarehouseChk" onchange="getWarehouseMarkers();">
											<label for="test3">{{ __('messages.zone.none') }}</label>
										</p>
									</div>

									<p class="title">{{ __('messages.zone.day_of_delivery') }}</p>
									<div class="position-relative">
										<div class="checkbox">
											<input class="styled-checkbox deliveryDayChk" id="monday" type="checkbox" value="monday" checked="checked" onchange="getFilteredDeliveryByDay();">
											<label for="monday" style="color: #8683b2;">{{ __('messages.days.monday') }}</label>
										</div>
										<div class="checkbox">
											<input class="styled-checkbox deliveryDayChk" id="tuesday" type="checkbox" value="tuesday" checked="checked" onchange="getFilteredDeliveryByDay();">
											<label for="tuesday" style="color: #5b85b0;">{{ __('messages.days.tuesday') }}</label>
										</div>
										<div class="checkbox">
											<input class="styled-checkbox deliveryDayChk" id="wednesday" type="checkbox" value="wednesday" checked="checked" onchange="getFilteredDeliveryByDay();">
											<label for="wednesday" style="color: #4ca743;">{{ __('messages.days.wednesday') }}</label>
										</div>
										<div class="checkbox">
											<input class="styled-checkbox deliveryDayChk" id="thursday" type="checkbox" value="thursday" checked="checked" onchange="getFilteredDeliveryByDay();">
											<label for="thursday" style="color: #aeb416;">{{ __('messages.days.thursday') }}</label>
										</div>
										<div class="checkbox">
											<input class="styled-checkbox deliveryDayChk" id="friday" type="checkbox" value="friday" checked="checked" onchange="getFilteredDeliveryByDay();">
											<label for="friday" style="color: #d2924c;">{{ __('messages.days.friday') }}</label>
										</div>
										<div class="checkbox">
											<input class="styled-checkbox deliveryDayChk" id="saturday" type="checkbox" value="saturday" checked="checked" onchange="getFilteredDeliveryByDay();">
											<label for="saturday" style="color: #cd6867;">{{ __('messages.days.saturday') }}</label>
										</div>
										<div class="checkbox">
											<input class="styled-checkbox deliveryDayChk" id="sunday" type="checkbox" value="sunday" checked="checked" onchange="getFilteredDeliveryByDay();">
											<label for="sunday" style="color: #b579ae;">{{ __('messages.days.sunday') }}</label>
										</div>
									</div>
								</form>
								<form action="#" id="unitsForm">
									<p class="title">{{ __('messages.zone.units') }}</p>
									<div class="position-relative units-wrapper">
										<p class="mb-0 radio-wrapper-round">
											<input type="radio" id="no-fill" name="radio-group" value="no-fill" checked class="deliveryUnitChk" onchange="getHeatmapDeliveryByUnit();">
											<label for="no-fill">{{ __('messages.zone.no_fill') }}</label>
										</p>
										<p class="mb-0 radio-wrapper-round">
											<input type="radio" id="frequency" name="radio-group" value="frequency" class="deliveryUnitChk" onchange="getHeatmapDeliveryByUnit();">
											<label for="frequency">{{ __('messages.zone.frequency') }}</label>
										</p>
										<p class="mb-0 radio-wrapper-round">
											<input type="radio" id="kg" name="radio-group" value="kg" class="deliveryUnitChk" onchange="getHeatmapDeliveryByUnit();">
											<label for="kg">Kg</label>
										</p>
										<p class="mb-0 radio-wrapper-round">
											<input type="radio" id="m3" name="radio-group" value="m3" class="deliveryUnitChk" onchange="getHeatmapDeliveryByUnit();">
											<label for="m3">m3</label>
										</p>
										<p class="mb-0 radio-wrapper-round">
											<input type="radio" id="pieces" name="radio-group" value="pieces" class="deliveryUnitChk" onchange="getHeatmapDeliveryByUnit();">
											<label for="pieces">{{ __('messages.zone.pieces') }}</label>
										</p>
										<p class="mb-0 radio-wrapper-round border-bottom-0">
											<input type="radio" id="merchandise" name="radio-group" value="merchandise" class="deliveryUnitChk" onchange="getHeatmapDeliveryByUnit();">
											<label for="merchandise">$ {{ __('messages.zone.merchandise') }}</label>
										</p>
									</div>
								</form>
							</div>
							<div class="checkbox statistics-on-map">
								<input class="styled-checkbox" id="statistics-on-map" type="checkbox" value="value1">
								<label for="statistics-on-map">{{ __('messages.zone.statistics_on_map') }}</label>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="tab-pane fade" id="nav-save" role="tabpanel" aria-labelledby="nav-save-tab">
			<section class="save-tab-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="section visits-section first-visits-section">
                                    <p class="sub-heading text-center">{{ __('messages.zone.orders') }}</p>
                                    <form action="#" id="SaveDeliveryFile">
                                        <div class="form-group custom-select-box-wrapper position-relative">
                                            <select class="form-control" name="company" required="required" onchange="onSelectCompany(this)"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
                                                <option value="">{{ __('messages.zone.company') }}</option>
												<?php if($cw_data){
													foreach($cw_data as $key => $cw){?>
													<option value="<?php echo $key;?>" <?php if($data['default_options'] && $data['default_options']['my_company'] == $key){ echo 'selected';}?>><?php echo $key;?></option>
												<?php } }?>
                                            </select>
                                        </div>
                                        <div class="form-group custom-select-box-wrapper position-relative">
                                            <select class="form-control" name="warehouse" required="required"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
                                                <option value="">{{ __('messages.zone.warehouse') }}</option>

												<?php if($data['default_options'] && $data['default_options']['my_warehouse'] != ''){

												$wArr = $cw_data[$data['default_options']['my_company']];
												if($wArr){
													foreach($wArr as $w => $val){
												?>
													<option value="<?php echo $val;?>" <?php if($data['default_options']['my_warehouse'] == $val){ echo 'selected';}?>><?php echo $val;?>
													</option>
												<?php } } } ?>

                                            </select>
                                        </div>
                                        <div class="form-group comments-field">
                                            <input type="text" placeholder="{{ __('messages.zone.comments') }}" name="comments">
                                        </div>
										<div class="form-group save-as-wrapper">
											<input type="text" class="save-delivery-file" name="fileName" placeholder="{{ __('messages.zone.saveAsDeliveryFile') }}" required="required"
												oninvalid="this.setCustomValidity(returnInputRequiredMessage())" oninput="setCustomValidity('')">
										</div>
                                        <div class="form-group text-center mt-4 mb-0">
                                            <button type="submit" class="custom-btn">{{ __('messages.zone.save') }}</button>
                                        </div>
                                    </form>
                                </div>

                                <table class="deliveryTableData table custom-table visits-table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>{{ __('messages.zone.total') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php if($data['deliveryTableData']){ echo $data['deliveryTableData']; }?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="section visits-section">
                                    <p class="sub-heading text-center">{{ __('messages.zone.zones2') }}</p>
                                    <form action="#" id="SaveModelFile">
										<input type="hidden" name="folder" value="ZONES"/>
                                        <div class="form-group custom-select-box-wrapper position-relative">
                                            <select class="form-control" onchange="onSelectCompany(this)" name="company" required="required"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
                                                <option value="">{{ __('messages.zone.company') }}</option>
										<?php if($cw_data){
												foreach($cw_data as $key => $cw){?>
												<option value="<?php echo $key;?>" <?php if($data['default_options'] && $data['default_options']['my_company'] == $key){ echo 'selected';}?>><?php echo $key;?></option>
										<?php } }?>
                                            </select>
                                        </div>
                                        <div class="form-group custom-select-box-wrapper position-relative">
                                            <select class="form-control" name="warehouse" required="required"
												oninvalid="this.setCustomValidity(returnRequiredMessage())" oninput="setCustomValidity('')">
                                                <option value="">{{ __('messages.zone.warehouse') }}</option>

												<?php if($data['default_options'] && $data['default_options']['my_warehouse'] != ''){

												$wArr = $cw_data[$data['default_options']['my_company']];
												if($wArr){
													foreach($wArr as $w => $val){
												?>
													<option value="<?php echo $val;?>" <?php if($data['default_options']['my_warehouse'] == $val){ echo 'selected';}?>><?php echo $val;?>
													</option>
												<?php } } } ?>

                                            </select>
                                        </div>
                                        <div class="form-group comments-field">
                                            <input type="text" placeholder="{{ __('messages.zone.comments') }}" name="comments">
                                        </div>
										<div class="form-group save-as-wrapper">
											<input type="text" class="save-model-file" name="fileName" placeholder="{{ __('messages.zone.saveAsModelFile') }}">
										</div>
                                        <div class="form-group text-center mt-4 mb-0">
                                            <button type="submit" class="custom-btn">{{ __('messages.zone.save') }}</button>
                                        </div>
                                    </form>
                                </div>

                                <table class="modelTableData table custom-table table-responsive">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.zone.zone_free') }}/</th>
                                            <th>{{ __('messages.zone.count') }}</th>
                                            <th>Kg</th>
                                            <th>m3</th>
                                            <th>{{ __('messages.zone.pieces') }}</th>
                                            <th>$ {{ __('messages.zone.goods') }}</th>
                                            <th>{{ __('messages.zone.day_of_delivery') }}</th>
                                            <th>{{ __('messages.zone.charge') }}</th>
                                        </tr>
                                    </thead>
									<tbody>
									<?php if($data['modelTableData']){ echo $data['modelTableData']; }?>
									</tbody>
                                </table>
                            </div>

                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <div class="section visits-section export-csv-form">
                                    <p class="sub-heading text-center">{{ __('messages.zone.export_csv_per_zone') }}</p>
                                    <form action="#" id="SaveExportFiles">
										<div class="form-group save-as-wrapper">
											<input type="text" name="fileName" placeholder="{{ __('messages.zone.export_as') }}" id="exportFileName">
										</div>
                                        <div class="export-btn form-group text-center mb-0">
                                            <button type="submit" class="custom-btn mt-4">{{ __('messages.zone.export') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
		</div>
	</div>
</div>

<!-- Footer Section -->
<footer id="footer" class="shadow-md pb-0">
	<ul class="nav footer-nav" id="nav-tab" role="tablist">
		<li class="nav-item active">
			<a class="nav-item nav-link font-12" id="nav-home-tab" data-toggle="tab" href="#nav-open" role="tab" aria-controls="nav-open" aria-selected="true">{{ __('messages.zone.open') }}</a>
		</li>
		<li class="nav-item">
			<a class="nav-item nav-link font-12" id="nav-profile-tab" data-toggle="tab" href="#nav-zones" role="tab" aria-controls="nav-zones" aria-selected="false">{{ __('messages.zone.zones') }}</a>
		</li>
		<li class="nav-item">
			<a class="nav-item nav-link font-12" id="nav-contact-tab" data-toggle="tab" href="#nav-save" role="tab" aria-controls="nav-save" aria-selected="false">{{ __('messages.zone.save') }}</a>
		</li>
	</ul>
</footer>
<div id="successMsg" style="display:none">{{ __('messages.zone.success_heading') }}</div>
<div id="shapeSelectRemoveErr" style="display:none">{{ __('messages.zone.select_zone_error') }}</div>
<div id="infoWindowOldContent" style="display:none">
</div>
<script>
	// Get Titles for Map Controls
	function getMapControlTitles() {
		let select = "{{ __('messages.tooltip.select') }}";
		let rectangle = "{{ __('messages.tooltip.rectangle') }}";
		let circle = "{{ __('messages.tooltip.circle') }}";
		let polygon = "{{ __('messages.tooltip.polygon') }}";
		let screenshot = "{{ __('messages.tooltip.screenshot') }}";
		let deleteTitle = "{{ __('messages.tooltip.delete') }}";
		let deliveryDay = "{{ __('messages.zone.delivery_day') }}";
		let count = "{{ __('messages.zone.count_for_figure') }}";
		let zone = "{{ __('messages.zone.zone_for_figure') }}";
		let netErrorForDelivery = "{{ __('messages.zone.net_error_for_deliveries') }}";
		let netErrorForModel = "{{ __('messages.zone.net_error_for_model') }}";
		let show_text = "{{ __('messages.zone.show') }}";
		let hide_text = "{{ __('messages.zone.hide') }}";
		let order_pieces_shortform = "{{ __('messages.zone.order_pieces_shortform') }}";
		let tooltipTitles = {
			"select": select,
			"rectangle": rectangle,
			"circle": circle,
			"polygon": polygon,
			"screenshot": screenshot,
			"delete": deleteTitle,
			"deliveryDay": deliveryDay,
			"count": count,
			"zone": zone,
			"net_error_for_deliveries": netErrorForDelivery,
			"net_error_for_model": netErrorForModel,
			"show_text": show_text,
			"hide_text": hide_text,
			"order_pieces_shortform": order_pieces_shortform
		};
		//console.log(tooltipTitles);
		return tooltipTitles;
	}
</script>
@include('partials.zone.javascripts')
	<script>
		let lang;
	<?php if(App::isLocale('sp')){ ?>
		lang = "es";
	<?php } else {?>
		lang = "en";
	<?php } ?>

	console.log('lang',lang);
	
	$('.input-group.start_date').datepicker({
		language: lang,
		autoclose: true,
		format: "dd-mm-yyyy"
	});	
	$('.input-group.end_date').datepicker({
		language: lang,
		autoclose: true,
		format: "dd-mm-yyyy"
	});		
	</script>
   <script>
        var selectedShapeNameForColor = 'rectangle';
        var selectedRectengleBgColor, selectedCircleBgColor, selectedPolygonBgColor;
        var selectedRectangleInfoTextColor, selectedCircleInfoTextColor, selectedPolygonInfoTextColor;

        $(document).ready(function() {

            // Link Activation For Footer
            var footerLinks = [];
            $(document).on('click', '.footer-nav>li', function() {
                footerLinks.push($(this).find('a').attr('aria-controls'));
                $(".footer-nav>li").removeClass('active');
                $(this).addClass('active');
            });

            // Change Shapes Background
            $(".color-picker.bg-picker>i").click(function() {
                if (selectedShapeNameForColor == 'rectangle') {
                    selectedRectengleBgColor = $(this).attr('class');
                } else if (selectedShapeNameForColor == 'circle') {
                    selectedCircleBgColor = $(this).attr('class');
                } else if (selectedShapeNameForColor == 'polygon') {
                    selectedCircleBgColor = $(this).attr('class');
                }
                changeBgColorOfShapes($(this).attr('class'), selectedShapeNameForColor);
                $(".color-picker.bg-picker i").removeClass('active');
                $(this).addClass('active');
            });

            // Change Text Color Of InfoWindow
            $(".color-picker.text-picker>i").click(function() {
                if (selectedShapeNameForColor == 'rectangle') {
                    selectedRectangleInfoTextColor = $(this).attr('class');
                } else if (selectedShapeNameForColor == 'circle') {
                    selectedCircleInfoTextColor = $(this).attr('class');
                } else if (selectedShapeNameForColor == 'polygon') {
                    selectedPolygonInfoTextColor = $(this).attr('class');
                }
				changeInfowindowTextColor($(this).attr('class'), selectedShapeNameForColor);
                $(".custom-info-window." + selectedShapeNameForColor + "-infowindow>p").css('color', $(this).attr('class'));
				selectedInfowindowTextColor = $(this).attr('class');
                $(".color-picker.text-picker i").removeClass('active');
                $(this).addClass('active');
            });

            // Snapshot Process
            var imgCount = 0;
            $(document).on('click', '#screenCaptureBtn', function() {
                imgCount += 1;
                $(".loader").css('display', 'flex');
                html2canvas(document.getElementById("map"), {
                    "proxy": "html2canvasproxy.php",
                    "useOverflow": false,
                    "logging": true, // Enable log (use Web Console for get Errors and Warnings)
                    "onrendered": function(canvas) {
                        canvas.toBlob(function(blob) {
                            //let filename = "image1.png";
                            var uri = URL.createObjectURL(blob);
                            var img = new Image();
                            img.src = uri;

                            // Create Tabs For Snapshots
                            $("#footer>ul").append(
                                '<li class="nav-item">' +
                                '<a class="nav-item nav-link font-12 nav-img-' + imgCount + '" id="nav-contact-tab" data-toggle="tab" href="#nav-img-' + imgCount + '" role="tab" aria-controls="nav-img-' + imgCount + '" aria-selected="false">IMG' + imgCount + '</a>' +
                                '</li>'
                            );

                            $("#nav-tabContent").append(
                                '<div class="tab-pane fade snapshot-tab position-relative" id="nav-img-' + imgCount + '" role="tabpanel" aria-labelledby="nav-img-tab">' +
                                '<div class="btn-wrapper">' +
                                //'<img src="./images/icon-copy-content.png" alt="icon-copy-content" title="Copy" class="copy-clipboard mr-3" data="' + imgCount + '">' +
                                '<img src="./images/icon-download.png" alt="icon-download" title="Download" class="mr-3 download-snapshot">' +
                                '<img src="./images/icon-trash.png" alt="icon-trash" data="' + imgCount + '" title="Remove" class="remove-snapshot">' +
                                '</div>' +
                                '<img src="' + uri + '" class="snapshot-preview d-block m-auto" id="map-img' + imgCount + '" >' +
                                '</div>'
                            );
							$(".loader").css('display', 'none');
							successAlert("{{ __('messages.zone.take_screenshot') }}");
                        });
                    }
                });
            });

            // Remove Snapshot
            $(document).on('click', '.remove-snapshot', function() {
				let url = $('.snapshot-tab.active.show img.snapshot-preview').attr('src');
                let snapshotNumber = $(this).attr('data');
                let lastActiveFooterLink = footerLinks.length - 2;
                $(".nav-img-" + snapshotNumber).closest('.nav-item').remove();
                $("#nav-img-" + snapshotNumber).remove();

                if ($("#footer ul li").has('a[aria-controls="' + footerLinks[lastActiveFooterLink] + '"]').length) {
                    $('a[href="#' + footerLinks[lastActiveFooterLink] + '"]').trigger('click');
                } else {
                    $("a[href='#nav-zones']").trigger('click');
                }
				window.URL.revokeObjectURL(url);
				successAlert("{{ __('messages.zone.delete_screenshot') }}");
            });

            // Download Img
            $(document).on('click', '.download-snapshot', function() {
				let url = $('.snapshot-tab.active.show img.snapshot-preview').attr('src');
				let d = new Date();
				let n = d.getTime();
				let fileName = 'IMG'+n+'.png';
				var a = document.createElement("a");
				document.body.appendChild(a);
				a.style = "display: none";
				a.href = url;
				a.download = fileName;
				a.id = fileName;
				a.click();

				setTimeout(function(){
					let element = document.getElementById(fileName);
					element.parentNode.removeChild(element);
				}, 1000);

            });

            // Copy Clipboard Image
            $(document).on('click', '.copy-clipboard', function() {
                let div = $(this).attr('data');
                div = $('map-img' + div);
                // var div = document.getElementById('chart1');
                div.contentEditable = true;
                var controlRange;
                if (document.body.createControlRange) {
                    controlRange = document.body.createControlRange();
                    controlRange.addElement(div);
                    controlRange.execCommand('Copy');
                }
                div.contentEditable = false;
            });

	            // Show Infowindow On Active Statistics
            $(document).on('click', '.statistics-on-map>label', function() {
                if ($('#statistics-on-map').hasClass('active')) {
					$('#statistics-on-map').removeClass('active');
					//$(".custom-info-window-destination.units").hide();
					activeOrInactiveStatistics(false);
                } else {
                    $('#statistics-on-map').addClass('active');
					//$(".custom-info-window-destination.units").show();
					activeOrInactiveStatistics(true);
                }
				
				deliveryDayFilterDestinationsCheck();
            });


			if (window.innerWidth <= 1200) {
				$(".open-model-table").addClass('table-responsive');
			}

			// Update Selected Zone
			$("#updateZoneBtn").click(function(event) {
				event.preventDefault();
				var zoneName = $("#zoneName").val();
				var comment1 = $("#comment1").val();
				var comment2 = $("#comment2").val();
				var xLatitude = $("#xLatitude").val();
				var length = $("#length").val();
				var radius = $("#radius").val();
				var height = $("#height").val();
				var width = $("#width").val();
				var zoneInfo = {
					"zoneName": zoneName,
					"comment1": comment1,
					"comment2": comment2,
					"xLatitude": xLatitude,
					"length": length,
					"radius": radius,
					"height": height,
					"width": width
				};
				updateSelectedZone(zoneInfo);
				successAlert("{{ __('messages.zone.update_zone') }}");
			});

        });
    </script>
	<script>
		function onSelectCompany(e){
			var comp_ware_data = <?php echo $cw_json;?>;
			//console.log('comp_ware_data',comp_ware_data);
			var wareData = "<option value=''><?php echo __('messages.login.warehouse');?></option>";
			var company = e.value;
			var wt = $(e).parent().next().find('select');
			if(company !== ''){
				var ware_data = comp_ware_data[company];
				$.each(ware_data, function(key, val) {
					wareData += "<option value='"+val+"'>"+val+"</option>";
				});
			}
			wt.html(wareData);
		}

    // Load Data from DB using API
    $("#getDataFromDB").submit(function(e){
        e.preventDefault();
		$(".loader").css('display', 'flex');
		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/get-from-db',
			data:$("#getDataFromDB").serialize(),
			success:function(response){
				$(".loader").css('display', 'none');
				response = JSON.parse(response);
				if(response['status'] === true){
					successAlert("{{ __('messages.zone.get_data_from_db_success') }}");
					// warningAlert();
					removeHeatmapDelivery();
					var tdata = response['data']['table'];
					setDeliveryTableData(tdata);
					//var del_data = response['data']['all']['data'];
					var del_data = response['data']['unique'];
					setDeliveryDataOnMap(del_data);
					setModelTableAfterDelivery();
					saveMapData();
					$("#deliveryComment").val(response['data']['all']['comments']);
					getWarehouseMarkers();
				}else{
					if(response['error_type'] == 1){
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}else{
						warningAlert("{{ __('messages.zone.net_error_for_daterange') }}");
					}

				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
    });

	function onSelectWarehouseDelivery(e){
		$(".loader").css('display', 'flex');
		var JData = "<option value=''><?php echo __('messages.zone.file');?></option>";
		var warehouse_val = e.value;
		var company_val = $(e).parent().prev().find('select').val();
		var formName = $(e).closest("form");
		var jt = $(e).parent().next().find('select');
		if(warehouse_val !== '' && company_val !== ''){
			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/get-delivery-files',
				data:{'company':company_val, 'warehouse': warehouse_val},
				success:function(response){
					response = JSON.parse(response);
					if(response['status'] === true){
						let fileNames = response['data'];
						$.each(fileNames, function(key, val) {
							JData += "<option value='"+val+"'>"+val+"</option>";
						});
						jt.html(JData);
					}else{
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
					$(".loader").css('display', 'none');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		}else{
			$(".loader").css('display', 'none');
			jt.html(JData);
		}
	}

    // Open selected JSON file of delivery from bucket and load data
    $("#getDataFromFile").submit(function(e){
        e.preventDefault();
		$(".loader").css('display', 'flex');
		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/get-from-delivery-file',
			data:$("#getDataFromFile").serialize(),
			success:function(response, textStatus, xhr){
				$(".loader").css('display', 'none');
				response = JSON.parse(response);
				//console.log(response);
				if(response['status'] === true){
					successAlert("{{ __('messages.zone.get_data_from_file_success') }}");
					removeHeatmapDelivery();
					var tdata = response['data']['table'];
					setDeliveryTableData(tdata);
					// var del_data = response['data']['all']['data'];
					var del_data = response['data']['unique'];
					//console.log(del_data);
					setDeliveryDataOnMap(del_data);
					setModelTableAfterDelivery();
					saveMapData();
					$("#deliveryComment").val(response['data']['all']['comments']);
					getWarehouseMarkers();
				}else{
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
    });

    //Save Delivery File to Google Storage Bucket Folder VISIT
    $("#SaveDeliveryFile").submit(function(e){
        e.preventDefault();
		$(".loader").css('display', 'flex');
		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/save-delivery-file',
			data:$("#SaveDeliveryFile").serialize(),
			success:function(response){
				$(".loader").css('display', 'none');
				response = JSON.parse(response);
				if(response['status'] === true){
					let newFile = $("input.save-delivery-file").val();
					newFile = newFile+'.json';
					let newFileOption = "<option value='"+newFile+"'>"+newFile+"</option>";
					$("select.delivery-files-dropdown").append(newFileOption);
					// alert('Success');
					successAlert("{{ __('messages.zone.delivery_file_save_success') }}");
					// warningAlert();
				} else {
					errorAlert("{{ __('messages.zone.error_on_delivery_file_save') }}");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
    });

	function onSelectWarehouseModel(e){
		$(".loader").css('display', 'flex');
		var JData = "<option value=''><?php echo __('messages.zone.model');?></option>";
		var warehouse_val = e.value;
		var company_val = $(e).parent().prev().find('select').val();
		var formName = $(e).closest("form");
		var jt = $(e).parent().next().find('select');
		if(warehouse_val !== '' && company_val !== ''){
			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/get-model-files',
				data:{'company':company_val, 'warehouse': warehouse_val},
				success:function(response){
					response = JSON.parse(response);
					if(response['status'] === true){
						let fileNames = response['data'];
						$.each(fileNames, function(key, val) {
							JData += "<option value='"+val+"'>"+val+"</option>";
						});
						jt.html(JData);
					}else{
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
					$(".loader").css('display', 'none');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		}else{
			$(".loader").css('display', 'none');
			jt.html(JData);
		}
	}

    //Open Selected Json file of model from bucket and load data
    $("#getModelFileData").submit(function(e){
        e.preventDefault();
		$(".loader").css('display', 'flex');
		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/get-model-file-data',
			data:$("#getModelFileData").serialize(),
			success:function(response){
				$(".loader").css('display', 'none');
				response = JSON.parse(response);
				if(response['status'] === true){
					successAlert("{{ __('messages.zone.get_model_data_success') }}");
					var tdata = response['data']['table'];
					var zone_count = response['data']['zone_count'];
					$("#accordianTwo .zone_count").text(zone_count);
					$("#modelComment").val(response['data']['all']['comments']);
					removeZonesForModelFile();
					setModelTableData(tdata);
					readJson(response['data']['all']['data']);
				}else{
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
    });


    //Save Model File to Google Storage Bucket Folder ZONES
    $("#SaveModelFile").submit(function(e){
        e.preventDefault();
		if(isZoneOverlapped == true){
			errorAlert("{{ __('messages.zone.overlapping_zone') }}");
			return false;
		}

		$(".loader").css('display', 'flex');
		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/save-model-file',
			data:$("#SaveModelFile").serialize(),
			success:function(response){
				$(".loader").css('display', 'none');
				response = JSON.parse(response);
				if(response['status'] === true){
					let newFile = $("input.save-model-file").val();
					newFile = newFile+'.json';
					let newFileOption = "<option value='"+newFile+"'>"+newFile+"</option>";
					$("select.model-files-dropdown").append(newFileOption);
					// alert('Success');
					successAlert("{{ __('messages.zone.model_file_save_success') }}");
					// warningAlert();
				} else {
					errorAlert("{{ __('messages.zone.error_on_model_file_save') }}");
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
    });

	function setDeliveryTableData(data){
		$(".deliveryTableData tbody").html(data);
	}

	function setModelTableData(data){
		$(".modelTableData tbody").html(data);
	}


	//It is called when any shape created/dragged/resized/color changes or any other map related operations
	function saveMapData(){
		//checkForOverLappingZone(ZoneWiseDeliveryRows);
		checkForOverLappingZones();
		$(".loader").css('display', 'flex');
		let model_comments = $("#modelComment").val();
 		var allMapData = {
			"shape_data" : allShapesData,
			"ZoneWiseDeliveryRows":ZoneWiseDeliveryRows,
			"NoZoneDeliveryRows":NoZoneDeliveryRows,
			"model_comments":model_comments
		}
		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/save-map-data',
			data:allMapData,
			success:function(response){
				response = JSON.parse(response);
				if(response['status'] === true){
					var tdata = response['data']['table'];
					var zone_count = response['data']['zone_count'];
					$("#accordianTwo .zone_count").text(zone_count);
					setModelTableData(tdata);
					$(".loader").css('display', 'none');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
	}

	//SaveExportFiles -> File-Zone1.csv, File2-Zone2.csv and so on
    $("#SaveExportFiles").submit(function(e){
		e.preventDefault();
		let prefix  = $('#exportFileName').val();
		prefix  = prefix.trim();
		let prefixEncode  = encodeURI(prefix);

		if (allShapesData.zones != null && allShapesData.zones != '') {

			$.each(allShapesData.zones, function(key, val) {
				let id = val.id
				var a = document.createElement("a");
				let url = "<?php echo env('APP_URL').'/export/'; ?>"+id+"?name="+prefixEncode;
				//console.log(url);
				a.href = url;
				//console.log('prefix'+key,prefix);
				a.download = prefix+' - '+val.name+'.csv';
				document.body.appendChild(a);
				a.click();
				a.remove();
			});
			successAlert("{{ __('messages.zone.export_file_save_success') }}");
		} else  {
			errorAlert("{{ __('messages.zone.csv_name_required_msg') }}");
		}
    });



	// For File Required
	// function fileRequired(event) {
	// 	return event.setCustomValidity("{{__('messages.zone.file_required_warning')}}");
	// }
	function returnRequiredMessage() {
		return "{{__('messages.zone.file_required_warning')}}";
	}
	function returnInputRequiredMessage() {
		return "{{__('messages.zone.input_required_warning')}}";
	}

	var deliveryData = <?php echo json_encode($data['delivery_data']);?>;
	// Filters For Delivery Data
 	// $("#unitsForm input").click(function() {
		//console.log($(this).val());
		// $(".custom-info-window.units").show();
		// $('.custom-info-window.units p').hide();
		// $('.map-infowindow-' + $(this).val()).show();

		// deliveryFilters(deliveryData, $(this).val());
	// });

	//It is called when right side menu "day options" are checked/unchecked
	function getFilteredDeliveryByDay(){
		$(".loader").css('display', 'flex');

		var dayArr = [];
		var deliveryDayChk = $("input.deliveryDayChk:checked");

		for(var i=0; i<deliveryDayChk.length; i++)  {
			let val = $(deliveryDayChk[i]).val();
			dayArr.push(val);
		}

/* 		if(deliveryDayChk) {
			FilteredDeliveryByDay = true;
		} else {
			FilteredDeliveryByDay = false;
			return false;
		} */

		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/get-deliveries-by-day',
			data: {'days': dayArr },
			success:function(response){
				response = JSON.parse(response);
				if(response['status'] === true){
					let deliveryIds = response['data'];
					deliveryDayFilterDestinations = deliveryIds;
					if(destinationMarkers){
						$.each(destinationMarkers, function(key, value) {
							let visit_destination_name = value.visit_destination_name;
							let marker = value.marker;
							
							if(deliveryDayFilterDestinations.includes(visit_destination_name)){
								marker.setVisible(true);
							}else{
								marker.setVisible(false);
							}
						});
					}
					//console.log("calling setModelTableAfterDelivery...");
					setModelTableAfterDelivery();
					saveMapData();
					updateAllZoneInfoWindowData();
					getHeatmapDeliveryByUnit();
										
					$(".loader").css('display', 'none');
				}else{					
					if(destinationMarkers){
						$.each(destinationMarkers, function(key, value) {
							let marker = value.marker;
							marker.setVisible(false);
						});
					}
					deliveryDayFilterDestinations = [];
					setModelTableAfterDelivery();
					saveMapData();
					updateAllZoneInfoWindowData();					
					getHeatmapDeliveryByUnit();
					$(".loader").css('display', 'none');					
				}
				deliveryDayFilterDestinationsCheck();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					//errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
	}

	//It is called when right side menu "warehouse options" are checked/unchecked
	function getWarehouseMarkers(){
		$(".loader").css('display', 'flex');

		let warehouse = $("input.deliveryWarehouseChk:checked").val();

		$.ajax({
			type:'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:'/get-warhouse-markers',
			data:{'warehouse':warehouse},
			success:function(response){
				response = JSON.parse(response);
				if(response['status'] === true){

					let warehouseData = response['data'];
					
					displayWarehouseMarkers(warehouseData);

					$(".loader").css('display', 'none');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					//errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
	}

	// It is called when right side menu "units options" are checked/unchecked
	function getHeatmapDeliveryByUnit(){
		var deliveryDayChk = $("input.deliveryDayChk:checked");
		var selectedDays = [];
		let selectedAllDays;
		let unit = $("input.deliveryUnitChk:checked").val();
		
		//for statistics infowindow
/* 		if(statistics == true) {
		   $('.custom-info-window-destination.units').show();
		}else{
		  $('.custom-info-window-destination.units').hide();
		} */
		
		if(unit == 'no-fill'){
			removeHeatmapDelivery();
			return false;
		}

		if (deliveryDayChk.length == 7) {
			selectedAllDays = 'true';
		} else {
			selectedAllDays = 'false';
		}

		for(var i = 0; i < deliveryDayChk.length; i++)  {
			let val = $(deliveryDayChk[i]).val();
			selectedDays.push(val);
		}
					
		$(".loader").css('display', 'flex');
		$.ajax({
			type: 'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: '/get-heatmap-deliveries-by-units',
			data: {
				'unit': unit,
				'selectedDays': selectedDays,
				'selectedAllDays': selectedAllDays
			},
			success:function(response){
				 response = JSON.parse(response);
				 $(".loader").css('display', 'none');
				 if(response['status'] === true){
				 	removeHeatmapDelivery();
 				 	let heatmap_data = response['data'];
				deliveryFiltersUnits(heatmap_data, unit);
				 }
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				if(navigator.onLine == false) {
					$(".loader").css('display', 'none');
					//errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
				}
			}
		});
	}
</script>



<?php
// The below script tag is conditional based. to add any code please use above script tag.

if($data['default_options'] && $data['default_options']['my_company'] != '' && $data['default_options']['my_warehouse'] != ''){ ?>
<script>

	function defaultSelectedWarehouseModel(company,warehouse,model){
		$(".loader").css('display', 'flex');
		var JData = "<option value=''><?php echo __('messages.zone.model');?></option>";
		var warehouse_val = warehouse;
		var company_val = company;
		var jt = $('select.model-files-dropdown');
		if(warehouse_val !== '' && company_val !== ''){

			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/get-model-files',
				data:{'company':company_val, 'warehouse': warehouse_val},
				success:function(response){
					response = JSON.parse(response);
					if(response['status'] === true){
						let fileNames = response['data'];
						$.each(fileNames, function(key, val) {
							if(val === model){
								JData += "<option value='"+val+"' selected>"+val+"</option>";
							}else{
								JData += "<option value='"+val+"'>"+val+"</option>";
							}

						});
						jt.html(JData);

					}else{
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
					$(".loader").css('display', 'none');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		}else{
			jt.html(JData);
			$(".loader").css('display', 'none');
		}
	}

	defaultSelectedWarehouseModel("<?php echo $data['default_options']['my_company'];?>","<?php echo $data['default_options']['my_warehouse'];?>","<?php echo $data['default_options']['my_model'];?>");

	function defaultSelectedWarehouseDelivery(company,warehouse){
		$(".loader").css('display', 'flex');
		var JData = "<option value=''><?php echo __('messages.zone.file');?></option>";
		var warehouse_val = warehouse;
		var company_val = company;
		var jt = $('select.delivery-files-dropdown');
		if(warehouse_val !== '' && company_val !== ''){
			$.ajax({
				type:'POST',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url:'/get-delivery-files',
				data:{'company':company_val, 'warehouse': warehouse_val},
				success:function(response){
					response = JSON.parse(response);
					if(response['status'] === true){
						let fileNames = response['data'];
						$.each(fileNames, function(key, val) {
								JData += "<option value='"+val+"'>"+val+"</option>";
						});
						jt.html(JData);

					}else{
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
					$(".loader").css('display', 'none');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					if(navigator.onLine == false) {
						$(".loader").css('display', 'none');
						errorAlert("{{ __('messages.zone.net_error_for_deliveries') }}");
					}
				}
			});
		}else{
			jt.html(JData);
			$(".loader").css('display', 'none');
		}
	}

	defaultSelectedWarehouseDelivery("<?php echo $data['default_options']['my_company'];?>","<?php echo $data['default_options']['my_warehouse'];?>");

</script>
<?php }else{ ?>
<script>
$(".loader").css('display', 'none');
</script>
<?php } ?>

<?php if($data['delivery_data']){?>
<script>

	function afterRefreshDeliveryData(){
		$(".loader").css('display', 'flex');
		var del_data = <?php echo json_encode($data['delivery_data']);?>;

		setTimeout(function() {
			setDeliveryDataOnMap(del_data);
			<?php if(!$data['model_data']){?>
			setModelTableAfterDelivery();
			saveMapData();
			<?php } ?>
			getWarehouseMarkers();
			$(".loader").css('display', 'none');
		}, 1000);
	}

	afterRefreshDeliveryData();
</script>
<?php } ?>

<?php if($data['model_data']){?>
<script>
	function afterRefreshModelData(){
		$(".loader").css('display', 'flex');
		var model_data = <?php echo json_encode($data['model_data']);?>;
		 setTimeout(function() {
			//console.log(model_data);
			removeZonesForModelFile();
			readJson(model_data);
			$(".loader").css('display', 'none');
		 }, 1250);
	}
	// $(document).ready(function() {
	// 	afterRefreshModelData();
	// });
	window.onload = function() {
		afterRefreshModelData();
	}
</script>
<?php } ?>

@endsection
