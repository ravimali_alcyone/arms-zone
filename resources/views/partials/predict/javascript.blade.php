<!-- Custom Scripts -->
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/predict-map.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_KEY') }}&libraries=drawing,geometry&callback=initMap" async defer></script>