    <!-- Custom Scripts -->
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ asset('js/map.js') }}"></script>
    <script src="{{ asset('js/readJson.js') }}"></script>
    <script src="{{ asset('js/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ asset('js/datepicker/locales/bootstrap-datepicker.es.min.js') }}" charset="UTF-8"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_KEY') }}&libraries=drawing,geometry,visualization&callback=initMap" async defer></script>