<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="auther" content="Miguel Marengo">
    <meta name="description" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ARMS Zone</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/fevicon.png') }}" type="image/x-icon">
	@include('partials.header.stylesheets')
	@include('partials.header.javascripts')
</head>

<body>
<?php if(Request::route()->getName() == 'zone' || Request::route()->getName() == 'predict' || Request::route()->getName() == 'login2'){?>
    <!-- Loader -->
    <div class="loader" style="display:flex;">
        <img src="./images/loading.gif" alt="loading">
    </div>
<?php } ?>
	<!-- Header -->
    <header>
        <nav class="navbar navbar-expand-md pb-0">
            <!-- Brand -->
			<a class="navbar-brand" href="#"><img src="./images/logo.png" alt="logo" width="100" class="d-block m-auto"></a>
            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="fa fa-bars text-white"></span>
            </button>
<?php $user  = Session::get('getInfo'); ?>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav header-nav">
                    <li class="nav-item {{ Request::route()->getName() == 'login' || Request::route()->getName() == 'login2' ? 'active' : '' }}">
                        <a class="nav-link font-12" href="/login">{{ __('messages.menu.login_logout') }}</a>
                    </li>
                    <li class="nav-item {{ Request::route()->getName() == 'zone' ? 'active' : '' }}">
                        <a class="nav-link font-12" <?php if($user){ echo "href='/zone'";}else{ echo " id='zones-disabled' ";} ?> >{{ __('messages.menu.zones') }}</a>
                    </li>
                    <li class="nav-item {{ Request::route()->getName() == 'predict' ? 'active' : '' }}">
                        <a class="nav-link font-12" <?php if($user){ echo "href='/predict'";}else{ echo " id='predict-disabled' ";} ?> >{{ __('messages.menu.predict') }}</a>
                    </li>					
                </ul>
<?php 
	if(Request::route()->getName() == 'login'){?>
                <ul class="navbar-nav header-nav">
					<li class="nav-item">
						<a class="nav-link font-12 <?php if(App::isLocale('en')){ echo 'active'; }?>" href="{{ env('APP_URL')}}/lang/en/{{ Request::segment(1) }}">{{ __('messages.menu.eng-lang') }}</a>
					</li>
					<li class="nav-item">
						<a class="nav-link font-12 <?php if(App::isLocale('sp')){ echo 'active'; }?>" href="{{ env('APP_URL')}}/lang/sp/{{ Request::segment(1) }}">{{ __('messages.menu.spa-lang') }}</a>
					</li>
                </ul>
<?php }elseif($user){?>

                <ul class="navbar-nav header-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle font-12" href="#" id="navbardrop" data-toggle="dropdown"><?php echo $user['name'];?></a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item <?php if(App::isLocale('en')){ echo 'active'; }?>" href="{{ env('APP_URL')}}/lang/en/{{ Request::segment(1) }}">{{ __('messages.menu.eng-lang') }}</a>
                            <a class="dropdown-item <?php if(App::isLocale('sp')){ echo 'active'; }?>" href="{{ env('APP_URL')}}/lang/sp/{{ Request::segment(1) }}">{{ __('messages.menu.spa-lang') }}</a>
                            <hr class="mt-1 mb-1">
                            <a class="dropdown-item" href="/logout"><span class="fa fa-power-off mr-2"></span> Logout</a>
                        </div>
                    </li>
                </ul>
<?php } ?>

            </div>
        </nav>
    </header>
    @yield('content')
</body>

</html>