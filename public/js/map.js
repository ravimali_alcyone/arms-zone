/*  Website Name: ARMS Zone
 *  Author: Megual Morengo
 *  Description: All Map JS For Zones Screen
 */
var map;
var drawingManager, selectedShape, recSW, recNE, selectedShapeName, selectedShapeCenter;
var circleArea, rectangleArea, polygonArea, rectangleWidth, rectangleHeight;
var rectangleInfowindowTextColor, circleInfowindowTextColor, polygonInfowindowTextColor;
var selectedInfowindowTextColor = "#fff";
var statistics = false;
var allInfowindows = [];
var infowindowId = 0;
var allShapesData = {
    "zones": []
};
var ZoneId = 0;
var warehouseMarkers = [];
var destinationMarkers = [];
var destinationMarkerIds = [];
var ZoneWiseDeliveryRows = [];
var NoZoneDeliveryRows = [];
var zoneWiseDeliveryCount = [];
var shapes = [];
var HeatMapData = [];
var isZoneOverlapped = false;
var deliveryDayFilterDestinations = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {
            lat: 19.432608,
            lng: -99.133209
        },
        mapTypeId: 'roadmap',
        mapTypeControl: false,
        zoomControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_RIGHT
        },
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
        gestureHandling: 'cooperative',
        clickableIcons: false,
        styles: [
            { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{ color: '#263c3f' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#6b9a76' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{ color: '#38414e' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#212a37' }]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#9ca5b3' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{ color: '#746855' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#1f2835' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#f3d19c' }]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{ color: '#2f3948' }]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{ color: '#17263c' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#515c6d' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{ color: '#17263c' }]
            }
        ]
    });

    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: false,
        drawingControl: false,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT,
            style: google.maps.MapTypeControlStyle.DEFAULT,
            drawingModes: ['rectangle', 'circle', 'polygon']
        },
        polygonOptions: {
            strokeColor: '#fd6d21',
            strokeOpacity: 1,
            strokeWeight: 2,
            fillColor: '#fd6d21',
            fillOpacity: 0.40,
            editable: true,
            draggable: true,
            suppressUndo: true,
            geodesic: true,
            zIndex: 1
        },
        circleOptions: {
            strokeColor: '#fd6d21',
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: '#fd6d21',
            fillOpacity: 0.40,
            editable: true,
            draggable: true,
            suppressUndo: true,
            geodesic: true,
            zIndex: 1
        },
        rectangleOptions: {
            strokeColor: '#fd6d21',
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: '#fd6d21',
            fillOpacity: 0.40,
            editable: true,
            draggable: true,
            suppressUndo: true,
            geodesic: true,
            zIndex: 1
        }
    });
    drawingManager.setMap(map);
    $(".loader").css('display', 'none');

    // Process After Overlay Complete
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
        ZoneId += 1;
        var newShape = e.overlay; //all data
        newShape.type = e.type; //type

        if (newShape.type == 'rectangle') {
            shapes.push({ 'shapename': 'rectangle', 'shape': newShape, 'ZoneId': ZoneId });
            getDetectedMarkers(newShape, ZoneId);
            getUndetectedMarkers();
            rectangleInfo(e);

            allShapesData.zones.push({
                "id": ZoneId,
                "name": getMapControlTitles().zone + ZoneId,
                "type": e.type,
                "comment1": "",
                "comment2": 0,
                "map_details": {
                    "southWestCoordinate": recSW,
                    "northEastCoordinate": recNE,
                    "center_lat_lng": { "lat": newShape.getBounds().getCenter().lat(), "lng": newShape.getBounds().getCenter().lng() },
                    "height": rectangleHeight,
                    "width": rectangleWidth,
                    "area": rectangleArea,
                    "bg_color": newShape.fillColor,
                    "text_color": "#fff"
                }
            });
            saveMapData();
        }

        if (newShape.type == 'circle') {

            shapes.push({ 'shapename': 'circle', 'shape': newShape, 'ZoneId': ZoneId });
            getDetectedMarkers(newShape, ZoneId);
            getUndetectedMarkers();
            circleInfo(e);

            allShapesData.zones.push({
                "id": ZoneId,
                "name": getMapControlTitles().zone + ZoneId,
                "type": newShape.type,
                "comment1": "",
                "comment2": 0,
                "map_details": {
                    "radius": newShape.getRadius(),
                    "center_lat_lng": { "lat": newShape.getCenter().lat(), "lng": newShape.getCenter().lng() },
                    "area": circleArea,
                    "bg_color": newShape.fillColor,
                    "text_color": "#fff"
                }
            });
            saveMapData();
        }

        if (newShape.type == 'polygon') {

            shapes.push({ 'shapename': 'polygon', 'shape': newShape, 'ZoneId': ZoneId });
            getDetectedMarkers(newShape, ZoneId);
            getUndetectedMarkers();
            polygonInfo(e);

            let polygonCoordinates = [];
            let paths = newShape.getPath().getArray();
            $.each(paths, function(key, value) {
                polygonCoordinates.push({ "lat": value.lat(), "lng": value.lng() });
            });

            // Create the bounds object
            var bounds = new google.maps.LatLngBounds();
            var sides = [];
            // Get paths from polygon and set event listeners for each path separately
            newShape.getPath().forEach(function(path, index) {
                let length = google.maps.geometry.spherical.computeLength(path);
                bounds.extend(path);
            });

            for (var i = 0; i < newShape.getPath().getLength(); i++) {
                // for each side in path, compute center and length
                let start = newShape.getPath().getAt(i);
                let end = newShape.getPath().getAt(i < newShape.getPath().getLength() - 1 ? i + 1 : 0);
                let sideLength = google.maps.geometry.spherical.computeDistanceBetween(start, end);
                sideLength = Math.round(sideLength / 1000); //in Km
                sides.push(sideLength);
            }

            allShapesData.zones.push({
                'id': ZoneId,
                "name": getMapControlTitles().zone + ZoneId,
                "type": newShape.type,
                "comment1": "",
                "comment2": 0,
                "map_details": {
                    'coordinates': polygonCoordinates,
                    "area": polygonArea,
                    "bg_color": newShape.fillColor,
                    "text_color": "#fff",
                    "center_lat_lng": { "lat": bounds.getCenter().lat(), "lng": bounds.getCenter().lng() },
                    "sides": sides,
                }
            });
            saveMapData();
        }
		
        if (newShape.type !== google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setDrawingMode(null);
        }
        // Add an event listener that selects the newly-drawn shape when the user mouses down on it.
        google.maps.event.addListener(newShape, 'click', function(e) {
            setSelection(newShape);
        });
        setSelection(newShape);
    });

    // Clear The Current Selection When The Drawing Mode Is Changed
    google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
    google.maps.event.addListener(map, 'click', clearSelection);
    document.getElementById("map").addEventListener('keyup', deleteSelectedShapeUsingBtn);

    // ***** Add Custom Cntrols On Map ***** //
    // Screen Capture
    const cameraImg = document.createElement("IMG");
    cameraImg.setAttribute('src', './images/icon-screenshot.png');
    cameraImg.setAttribute('id', 'screenCaptureBtn');
    cameraImg.setAttribute('data-html2canvas-ignore', 'true');
    cameraImg.setAttribute('data-toggle', 'tooltip');
    cameraImg.setAttribute('title', getMapControlTitles().screenshot);
    cameraImg.className = 'camera-control map-custom-control';
    cameraImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(cameraImg);

    // Polygon
    const polyImg = document.createElement("IMG");
    polyImg.setAttribute('src', './images/icon-hexagon.png');
    polyImg.setAttribute('data-html2canvas-ignore', 'true');
    polyImg.setAttribute('data-toggle', 'tooltip');
    polyImg.setAttribute('title', getMapControlTitles().polygon);
    polyImg.className = "polygone-control map-custom-control";
    polyImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(polyImg);

    // Circle
    const circleImg = document.createElement("IMG");
    circleImg.setAttribute('src', './images/icon-circle.png');
    circleImg.setAttribute('data-html2canvas-ignore', 'true');
    circleImg.setAttribute('data-toggle', 'tooltip');
    circleImg.setAttribute('title', getMapControlTitles().circle);
    circleImg.className = "circle-control map-custom-control";
    circleImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.CIRCLE);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(circleImg);

    // Rectangle
    const rectangleImg = document.createElement("IMG");
    rectangleImg.setAttribute('src', './images/icon-rectangle.png');
    rectangleImg.setAttribute('data-html2canvas-ignore', 'true');
    rectangleImg.setAttribute('data-toggle', 'tooltip');
    rectangleImg.setAttribute('title', getMapControlTitles().rectangle);
    rectangleImg.className = "rectangle-control map-custom-control";
    rectangleImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.RECTANGLE);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(rectangleImg);

    // Remove Shape
    const crossImg = document.createElement("IMG");
    crossImg.setAttribute('src', './images/icon-cross.png');
    crossImg.setAttribute('data-html2canvas-ignore', 'true');
    crossImg.setAttribute('data-toggle', 'tooltip');
    crossImg.setAttribute('title', getMapControlTitles().delete);
    crossImg.setAttribute('id', 'delete-button');
    crossImg.className = "cross-control map-custom-control";
    crossImg.addEventListener("click", (() => {
        return e => {
            deleteSelectedShape();
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(crossImg);

    // Zoom In
    const zoomIn = document.createElement("IMG");
    zoomIn.setAttribute('src', './images/icon-dot-sselection.png');
    zoomIn.className = "zoom-in-control map-custom-control";
    zoomIn.addEventListener("click", (() => {
        return e => {
            map.setZoom(map.getZoom() + 1);
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(zoomIn);

    // Zoom Out
    const zoomOut = document.createElement("IMG");
    zoomOut.setAttribute('src', './images/icon-zoomout.png');
    zoomOut.className = "zoom-out-control map-custom-control";
    zoomOut.addEventListener("click", (() => {
        return e => {
            map.setZoom(map.getZoom() - 1);
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(zoomOut);

    // Full Zoom
    const fullZoomIn = document.createElement("IMG");
    fullZoomIn.setAttribute('src', './images/icon-all-dots.png');
    fullZoomIn.className = "full-zoom-in-control map-custom-control";
    fullZoomIn.addEventListener("click", (() => {
        return e => {
            if (destinationMarkers && destinationMarkers.length > 0) {
                var bounds = new google.maps.LatLngBounds();
                $.each(destinationMarkers, function(key, value) {
                    let position = value.marker.getPosition();
                    bounds.extend(position);
                });
                map.fitBounds(bounds);
            } else {
                map.setZoom(7);
            }

        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(fullZoomIn);
    //End Custom Controls 

    // Set On Selected Shap
    map.addListener('zoom_changed', function() {
        if (selectedShape) {
            if (selectedShape.type == 'polygon') {
                var bounds = new google.maps.LatLngBounds();
                selectedShape.getPath().forEach(function(path, index) {
                    bounds.extend(path);
                });
                map.setCenter(bounds.getCenter());
            } else if (selectedShape.type == 'circle') {
                map.setCenter(selectedShape.getBounds().getCenter());
            } else if (selectedShape.type == 'rectangle') {
                map.setCenter(selectedShape.getBounds().getCenter());
            }
        }
    });

}

// Get Rectangle Info
function rectangleInfo(event) {

    infowindowId += 1;
    let infoWindowRec;
    let shapeNumber = ZoneId;
    var rectangle = event.overlay;
    var bounds = rectangle.getBounds();
    let ne = rectangle.getBounds().getNorthEast();
    let sw = rectangle.getBounds().getSouthWest();
    let nw = new google.maps.LatLng(ne.lat(), sw.lng());
    let se = new google.maps.LatLng(sw.lat(), ne.lng());
    let swLat = sw.lat();
    let swLng = sw.lng();
    let neLat = ne.lat();
    let neLng = ne.lng();
    recSW = { swLat, swLng };
    recNE = { neLat, neLng };


    //height
    var heightVal = google.maps.geometry.spherical.computeDistanceBetween(
        new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().east),
        new google.maps.LatLng(rectangle.bounds.toJSON().south, rectangle.bounds.toJSON().east)
    );

    //width
    var widthVal = google.maps.geometry.spherical.computeDistanceBetween(
        new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().east),
        new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().west)
    );

    rectangleWidth = Math.round(widthVal / 1000); // in Km
    rectangleHeight = Math.round(heightVal / 1000); // in Km
    rectangleArea = rectangleWidth * rectangleHeight;
    

    let dCount = getDeliveryCount(ZoneId);
    var contentString = '<div class="custom-info-window text-center" style="color:' + selectedInfowindowTextColor + '";>' +
        '<p class="map-infowindow-zonename">' + getMapControlTitles().zone + ZoneId + '</p>' +
        '<p class="map-infowindow-deliveryday">' + getMapControlTitles().deliveryDay + ': ' + ' </p>' +
        '<p class="map-infowindow-comment2">$0</p>' +
        '<p class="map-infowindow-totalarea">Km: ' + commaNum(rectangleArea) + ' Km2</p>' +
        '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
        '</div>';
    var textCenter = rectangle.getBounds().getCenter();

    if (infoWindowRec) {
        infoWindowRec.close();
    }

    infoWindowRec = new google.maps.InfoWindow();
    infoWindowRec.setPosition(textCenter);
    infoWindowRec.setContent(contentString);
    infoWindowRec.setOptions({
        pixelOffset: new google.maps.Size(0, 50)
    });

	infoWindowRec.open(map, rectangle);
    allInfowindows.push({ 'infowindow': infoWindowRec, 'id': ZoneId });

    setTimeout(() => {
        $(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
    }, 30);

    //Event Listners
    google.maps.event.addListener(rectangle, 'drag', function() {
        infoWindowRec.setPosition(rectangle.getBounds().getCenter());
        infoWindowRec.close(map, rectangle);
    });

    google.maps.event.addListener(rectangle, 'dragend', function() {
        setSelection(rectangle);
        infoWindowRec.setPosition(rectangle.getBounds().getCenter());
        setInfoWindowText(infoWindowRec, false);
		infoWindowRec.open(map, rectangle);
    });

    rectangle.addListener('bounds_changed', (function() {

        var timer;
        return function() {
            clearTimeout(timer);
            timer = setTimeout(function() {
                let ne = rectangle.getBounds().getNorthEast();
                let sw = rectangle.getBounds().getSouthWest();
                let nw = new google.maps.LatLng(ne.lat(), sw.lng());
                let se = new google.maps.LatLng(sw.lat(), ne.lng());
				let swLat = sw.lat();
				let swLng = sw.lng();
				let neLat = ne.lat();
				let neLng = ne.lng();
				recSW = { swLat, swLng };
				recNE = { neLat, neLng };
				
                if (allShapesData.zones) {
                    $.each(allShapesData.zones, function(key, val) {
						
                        if (val.id == shapeNumber && val.type == 'rectangle') {

							val.map_details.southWestCoordinate = recSW;					
                            val.map_details.northEastCoordinate = recNE;

                            //height
                            var heightVal = google.maps.geometry.spherical.computeDistanceBetween(
                                new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().east),
                                new google.maps.LatLng(rectangle.bounds.toJSON().south, rectangle.bounds.toJSON().east)
                            );

                            //width
                            var widthVal = google.maps.geometry.spherical.computeDistanceBetween(
                                new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().east),
                                new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().west)
                            );

                            val.map_details.width = Math.round((widthVal / 1000)); //in Km
                            val.map_details.height = Math.round((heightVal / 1000)); // in Km
                            val.map_details.area = (val.map_details.height * val.map_details.width); //in Km
                            val.map_details.center_lat_lng.lat = rectangle.getBounds().getCenter().lat();
                            val.map_details.center_lat_lng.lng = rectangle.getBounds().getCenter().lng();

                            $("#xLatitude").val(val.map_details.center_lat_lng.lat);
                            $("#length").val(val.map_details.center_lat_lng.lng);
                            $("#height").val(val.map_details.height);
                            $("#width").val(val.map_details.width);
                            $(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');

                            setSelection(rectangle);
                            getDetectedMarkers(rectangle, val.id);
                            getUndetectedMarkers();

                            infoWindowRec.setPosition(rectangle.getBounds().getCenter());

                            let dCount = getDeliveryCount(val.id);
                            let textInfo = { 'total_area': Math.round(parseFloat(val.map_details.area)), 'deliverycount': dCount };
                            setInfoWindowText(infoWindowRec, textInfo);
							infoWindowRec.open(map, rectangle);
                        }
                    });
                    saveMapData();
                }
            }, 50);
        }
    }()));
}

// Get Circle Info
function circleInfo(event) {
    let infoWindowCir;
    let shapeNumber = ZoneId;
    var circle = event.overlay;
    let rd = circle.getRadius() / 1000; // In KM
    let cirRadius = Math.round(rd);
    circleArea = Math.round(Math.PI * cirRadius * cirRadius);
    var textCenter = circle.getBounds().getCenter();
    let dCount = getDeliveryCount(ZoneId);
    var contentString = '<div class="custom-info-window text-center" style="color:' + selectedInfowindowTextColor + '";>' +
        '<p class="map-infowindow-zonename">' + getMapControlTitles().zone + ZoneId + '</p>' +
        '<p class="map-infowindow-deliveryday">' + getMapControlTitles().deliveryDay + ': ' + ' </p>' +
        '<p class="map-infowindow-comment2">$0</p>' +
        '<p class="map-infowindow-totalarea">Km: ' + commaNum(circleArea) + ' Km2</p>' +
        '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
        '</div>';

    if (infoWindowCir) {
        infoWindowCir.close();
    }

    infoWindowCir = new google.maps.InfoWindow();
    infoWindowCir.setPosition(textCenter);
    infoWindowCir.setContent(contentString);
    infoWindowCir.setOptions({
        pixelOffset: new google.maps.Size(0, 50)
    });
	
	infoWindowCir.open(map, circle);
    allInfowindows.push({ 'infowindow': infoWindowCir, 'id': ZoneId });

    setTimeout(() => {
        $(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
    }, 30);

    //Event Listners
    google.maps.event.addListener(circle, 'drag', function() {
        infoWindowCir.close(map, circle);
    });

    google.maps.event.addListener(circle, 'dragend', function() {
        setSelection(circle);
        infoWindowCir.setPosition(circle.getBounds().getCenter());
        setInfoWindowText(infoWindowCir, false);
		infoWindowCir.open(map, circle);
    });

    circle.addListener('bounds_changed', (function() {

        var timer;
        return function() {
            clearTimeout(timer);
            timer = setTimeout(function() {

                if (allShapesData.zones) {
                    $.each(allShapesData.zones, function(key, val) {
                        if (val.id == shapeNumber && val.type == 'circle') {
                            val.map_details.center_lat_lng = { "lat": circle.getCenter().lat(), "lng": circle.getCenter().lng() };
                            val.map_details.radius = circle.getRadius(); //original
                            val.map_details.bg_color = circle.fillColor;
                            let rd = Math.round(circle.getRadius() / 1000); //in Km
                            val.map_details.area = Math.round(Math.PI * rd * rd); //in Km

                            $("#height, #width, #polygone").val('');
                            $("#radius").val(rd);
                            $("#xLatitude").val(val.map_details.center_lat_lng.lat);
                            $("#length").val(val.map_details.center_lat_lng.lng);
                            $(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');


                            setSelection(circle);
                            getDetectedMarkers(circle, val.id);
                            getUndetectedMarkers();

                            infoWindowCir.setPosition(circle.getBounds().getCenter());

                            let dCount = getDeliveryCount(val.id);
                            let textInfo = { 'total_area': Math.round(parseFloat(val.map_details.area)), 'deliverycount': dCount };

                            setInfoWindowText(infoWindowCir, textInfo);
							infoWindowCir.open(map, circle);
                        }
                    });
                    saveMapData();
                }
            }, 50);
        }
    }()));

    circle.addListener('radius_changed', (function() {
        var timer;
        return function() {
            clearTimeout(timer);
            timer = setTimeout(function() {

                if (allShapesData.zones) {
                    $.each(allShapesData.zones, function(key, val) {
                        if (val.id == shapeNumber && val.type == 'circle') {
                            val.map_details.center_lat_lng = { "lat": circle.getCenter().lat(), "lng": circle.getCenter().lng() };
                            val.map_details.radius = circle.getRadius(); //original
                            val.map_details.bg_color = circle.fillColor;
                            let rd = Math.round(circle.getRadius() / 1000); //in Km
                            val.map_details.area = Math.round(Math.PI * rd * rd); //in Km

                            $("#height, #width, #polygone").val('');
                            $("#radius").val(rd);
                            $("#xLatitude").val(val.map_details.center_lat_lng.lat);
                            $("#length").val(val.map_details.center_lat_lng.lng);
                            $(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
                            setSelection(circle);
                            getDetectedMarkers(circle, val.id);
                            getUndetectedMarkers();

                            infoWindowCir.setPosition(circle.getBounds().getCenter());

                            let dCount = getDeliveryCount(val.id);
                            let textInfo = { 'total_area': Math.round(parseFloat(val.map_details.area)), 'deliverycount': dCount };

                            setInfoWindowText(infoWindowCir, textInfo);
							infoWindowCir.open(map, circle);
                        }
                    });
                    saveMapData();
                }
            }, 50);
        }
    }()));
}

// Get Polygon Info
function polygonInfo(event) {
    let infoWindowPol;
    let shapeNumber = ZoneId;
    var polygon = event.overlay;
    var area = google.maps.geometry.spherical.computeArea(polygon.getPath());
    polygonArea = Math.round(area / (1000 * 1000)); // In KM
    var bounds = new google.maps.LatLngBounds();
    polygon.getPath().forEach(function(element, index) {
        bounds.extend(element)
    });
    var textCenter = bounds.getCenter();
    let dCount = getDeliveryCount(ZoneId);
    var contentString = '<div class="custom-info-window text-center" style="color:' + selectedInfowindowTextColor + '";>' +
        '<p class="map-infowindow-zonename">' + getMapControlTitles().zone + ZoneId + '</p>' +
        '<p class="map-infowindow-deliveryday">' + getMapControlTitles().deliveryDay + ': ' + ' </p>' +
        '<p class="map-infowindow-comment2">$0</p>' +
        '<p class="map-infowindow-totalarea">Km: ' + commaNum(polygonArea) + ' Km2</p>' +
        '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
        '</div>';

    if (infoWindowPol) {
        infoWindowPol.close();
    }

    infoWindowPol = new google.maps.InfoWindow();
    infoWindowPol.setPosition(textCenter);
    infoWindowPol.setContent(contentString);
    infoWindowPol.setOptions({
        pixelOffset: new google.maps.Size(0, 50)
    });
	
	infoWindowPol.open(map, polygon);
    allInfowindows.push({ 'infowindow': infoWindowPol, 'id': ZoneId });

    setTimeout(() => {
        $(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
    }, 30);

    //Event Listners
    google.maps.event.addListener(polygon, 'drag', function() {
        infoWindowPol.close(map, polygon);
    });

    google.maps.event.addListener(polygon, 'click', function() {
        google.maps.event.addListener(polygon.getPath(), 'insert_at', function() {
			//console.log('insert_at');
            editOrDragPolygon(true);
            saveMapData();
        });

        google.maps.event.addListener(polygon.getPath(), 'set_at', function() {
			//console.log('set_at');
            editOrDragPolygon(true);
			saveMapData();
        });
    });

    google.maps.event.addListener(polygon, 'dragend', function() {
        setSelection(polygon);
        if (allShapesData.zones) {
            $.each(allShapesData.zones, function(key, val) {
                if (val.id == shapeNumber && val.type == 'polygon') {
                    $("#xLatitude").val(val.map_details.center_lat_lng.lat);
                    $("#length").val(val.map_details.center_lat_lng.lng);
                    $("#polygone").val(val.map_details.sides);
                    $(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
                    getDetectedMarkers(polygon, val.id);
                    getUndetectedMarkers();

                    bounds = new google.maps.LatLngBounds();
                    polygon.getPath().forEach(function(element, index) {
                        bounds.extend(element)
                    });

                    infoWindowPol.setPosition(bounds.getCenter());

                    let dCount = getDeliveryCount(val.id);
                    let textInfo = { 'total_area': Math.round(parseFloat(val.map_details.area)), 'deliverycount': dCount };

                    setInfoWindowText(infoWindowPol, textInfo);
					infoWindowPol.open(map, polygon);
                }
            });
            saveMapData();
        }
    });

    function editOrDragPolygon(param) {
        setSelection(polygon);
		//console.log('editOrDragPolygon');
        let polyCoord = polygon.getPath().getArray();
        let bounds = new google.maps.LatLngBounds();
        let sides2 = [];
        for (var i = 0; i < polygon.getPath().getLength(); i++) {
            // for each side in path, compute center and length
            let start = polygon.getPath().getAt(i);
            let end = polygon.getPath().getAt(i < polygon.getPath().getLength() - 1 ? i + 1 : 0);
            let sideLength = google.maps.geometry.spherical.computeDistanceBetween(start, end);
            sideLength = Math.round(sideLength / 1000); //in Km
            sides2.push(sideLength);
        }
        polygon.getPath().forEach(function(path, index) {
            bounds.extend(path);
        });

        if (allShapesData.zones) {
            $.each(allShapesData.zones, function(key, val) {
                if (val.id == shapeNumber && val.type == 'polygon') {
                    val.map_details.coordinates = [];
                    $.each(polyCoord, function(key, value) {
                        val.map_details.coordinates.push({ "lat": value.lat(), "lng": value.lng() });
                    });
                    val.map_details.area = Math.round(google.maps.geometry.spherical.computeArea(polygon.getPath()) / (1000 * 1000)); //in Km
                    val.map_details.center_lat_lng.lat = bounds.getCenter().lat();
                    val.map_details.center_lat_lng.lng = bounds.getCenter().lng();
                    val.map_details.sides = sides2;

                    if (param != true) {
                        $("#xLatitude").val(val.map_details.center_lat_lng.lat);
                        $("#length").val(val.map_details.center_lat_lng.lng);
                        $("#polygone").val(val.map_details.sides);
                        $(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
                        getDetectedMarkers(polygon, val.id);
                        getUndetectedMarkers();

                        infoWindowPol.setPosition(bounds.getCenter());

                        let dCount = getDeliveryCount(val.id);
                        let textInfo = { 'total_area': Math.round(parseFloat(val.map_details.area)), 'deliverycount': dCount };

                        setInfoWindowText(infoWindowPol, textInfo);
						infoWindowPol.open(map, polygon);
                    }
                }
            });
        }
    }
}

// Clear Selections
function clearSelection() {
    if (selectedShape) {
        if (selectedShape.type !== 'marker') {
            selectedShape.setEditable(false);
        }
        selectedShape = null;
    }
}

function setSelection(shape) {
    if (shape.type !== 'marker') {
        clearSelection();
        shape.setEditable(true);
    }

    selectedShape = shape;
    selectedShapeName = shape.type;
    $("#selected-shape").find('option').removeAttr('selected');
    if (selectedShapeName == 'rectangle') {
        selectedShapeCenter = selectedShape.getBounds().getCenter();
        $(".circle-title, .polygon-title").hide();
        $(".rectangle-title").show();
    } else if (selectedShapeName == 'circle') {
        selectedShapeCenter = selectedShape.getCenter();
        $(".rectangle-title, .polygon-title").hide();
        $(".circle-title").show();
    } else if (selectedShapeName == 'polygon') {
        let bounds = new google.maps.LatLngBounds();
        shape.getPath().forEach(function(path, index) {
            bounds.extend(path);
        });
        selectedShapeCenter = bounds.getCenter();
        $(".rectangle-title, .circle-title").hide();
        $(".polygon-title").show();
    }
    // Show Zone Data
    displayZoneData();

}

// Remove Selected Shapes Using Delete And Backspace Key
function deleteSelectedShapeUsingBtn(e) {
    if (e.keyCode == 8 || e.keyCode == 46) {
        deleteSelectedShape();
    }
}

// Remove Selected Shapes Using Cross Btn
function deleteSelectedShape() {
    if (selectedShape) {

        if (selectedShape.type == 'rectangle') {
            let center = selectedShape.getBounds().getCenter();
            removeZonesData(center, selectedShape);
        }
        if (selectedShape.type == 'circle') {
            let center = selectedShape.getCenter();
            removeZonesData(center, selectedShape);
        }
        if (selectedShape.type == 'polygon') {
            let bounds = new google.maps.LatLngBounds();
            selectedShape.getPath().forEach(function(path, index) {
                bounds.extend(path);
            });
            let center = bounds.getCenter();
            removeZonesData(center, selectedShape);
        }
        selectedShape.setMap(null);
        saveMapData();
    } else {
        let errMsg = $("#shapeSelectRemoveErr").text();
        errorAlert(errMsg, "");
    }
}


var rad = function(x) {
    return x * Math.PI / 180;
};

var getDistance = function(p1, p2) {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2.lat - p1.lat);
    var dLong = rad(p2.lng - p1.lng);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    //return d; // returns radius distance in meter
    return d / 1000; // returns radius distance in kilometer
};

function blobToFile(theBlob, fileName) {
    let date = new Date();
    theBlob.lastModifiedDate = date;
    theBlob.lastModified = date.getTime();
    theBlob.name = fileName;
    theBlob.webkitRelativePath = "";
    return theBlob;
}

// Change Background Color Of All Shapes According To Selected Title
function changeBgColorOfShapes(color, shapeName) {
    if (selectedShape) {
        if (selectedShape.type == 'rectangle') {
            var lat = selectedShape.getBounds().getCenter().lat();
            var lng = selectedShape.getBounds().getCenter().lng();
        } else if (selectedShape.type == 'circle') {
            var lat = selectedShape.getCenter().lat();
            var lng = selectedShape.getCenter().lng();
        } else if (selectedShape.type == 'polygon') {
            var bounds = new google.maps.LatLngBounds();
            selectedShape.getPath().forEach(function(path, index) {
                bounds.extend(path);
            });
            var lat = bounds.getCenter().lat();
            var lng = bounds.getCenter().lng();
        }

        selectedShape.set('fillColor', color);
        selectedShape.set('strokeColor', color);
        $.each(allShapesData.zones, function(key, val) {
            if (val.type == 'rectangle' && selectedShape.type == 'rectangle') {
                if (lat == val.map_details.center_lat_lng.lat && lng == val.map_details.center_lat_lng.lng) {
                    val.map_details.bg_color = color;
                }
            } else if (val.type == 'circle' && selectedShape.type == 'circle') {
                if (lat == val.map_details.center_lat_lng.lat && lng == val.map_details.center_lat_lng.lng) {
                    val.map_details.bg_color = color;
                }
            } else if (val.type == 'polygon' && selectedShape.type == 'polygon') {
                if (lat == val.map_details.center_lat_lng.lat && lng == val.map_details.center_lat_lng.lng) {
                    val.map_details.bg_color = color;
                }
            }
        });
    }
    saveMapData();
}

// Change Infowindow Text Color
function changeInfowindowTextColor(colorName, shapeName) {
    if (selectedShape) {
        if (selectedShape.type == 'rectangle') {
            var lat = selectedShape.getBounds().getCenter().lat();
            var lng = selectedShape.getBounds().getCenter().lng();
        } else if (selectedShape.type == 'circle') {
            var lat = selectedShape.getCenter().lat();
            var lng = selectedShape.getCenter().lng();
        } else if (selectedShape.type == 'polygon') {
            var bounds = new google.maps.LatLngBounds();
            selectedShape.getPath().forEach(function(path, index) {
                bounds.extend(path);
            });
            var lat = bounds.getCenter().lat();
            var lng = bounds.getCenter().lng();
        }

        $.each(allShapesData.zones, function(key, val) {
            if (lat == val.map_details.center_lat_lng.lat && lng == val.map_details.center_lat_lng.lng) {

                $.each(allInfowindows, function(key2, val2) {

                    if (val2.id == val.id) {

                        let oldContent = val2.infowindow.getContent();
                        $("#infoWindowOldContent").html(oldContent);
                        let textColor = colorName;
                        let zoneName = $("#infoWindowOldContent p.map-infowindow-zonename").html();
                        let deliveryday = $("#infoWindowOldContent p.map-infowindow-deliveryday").html();
                        let comment2 = $("#infoWindowOldContent p.map-infowindow-comment2").html();
                        let totalarea = $("#infoWindowOldContent p.map-infowindow-totalarea").html();
                        let dCount = getDeliveryCount(val.id);

                        let newContent = '';
						newContent = '<div class="custom-info-window text-center" style="color:' + textColor + ';">';

                        newContent += '<p class="map-infowindow-zonename">' + zoneName + '</p>' +
                            '<p class="map-infowindow-deliveryday">' + deliveryday + '</p>' +
                            '<p class="map-infowindow-comment2">' + comment2 + '</p>' +
                            '<p class="map-infowindow-totalarea">' + totalarea + '</p>' +
                            '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
                            '</div>';

                        val2.infowindow.setContent(newContent);
                        val.map_details.text_color = colorName;
                    }
                });
            }
        });
        saveMapData();
    }
}

// Active or Inactive Statistics
function activeOrInactiveStatistics(param) {
    statistics = param;
}

// Remove Data of Selected Zone
function removeZonesData(center, shape) {
    if (allShapesData.zones) {
        $.each(allShapesData.zones, function(key, value) {
            if (value.type == shape.type) {
                let center2 = value.map_details.center_lat_lng;
                if (center.lat() == center2.lat && center.lng() == center2.lng) {
                    DeletedShapeDetectMarkers(shape, value.id);
                    getUndetectedMarkers();
                    allShapesData.zones.splice(key, 1);
                    if (shapes) {
                        $.each(shapes, function(key2, value2) {
                            if (value2.ZoneId == value.id) {
                                shapes.splice(key2, 1);
                                return false;
                            }
                        });
                    }
                    return false;
                }
            }
        });
    }

    let xLatitude = $("#xLatitude").val();
    let length = $("#length").val();
    if (center.lat() == xLatitude && center.lng() == length) {
        $("#updateZoneForm-1 input").val('');
        $("#updateZoneForm-2 input").val('');
        $(".total-area").text('');
    }
}

// Display Zone Data in Form
function displayZoneData() {
    $.each(allShapesData.zones, function(key, value) {
        let lat = value.map_details.center_lat_lng.lat;
        let lng = value.map_details.center_lat_lng.lng;
        if (lat == selectedShapeCenter.lat() && lng == selectedShapeCenter.lng()) {
            $("#zoneName").val(value.name);
            $("#comment1").val(value.comment1);
            $("#comment2").val(value.comment2);
            $("#xLatitude").val(lat);
            $("#length").val(lng);
            $(".total-area").text(commaNum(Math.round(parseFloat(value.map_details.area))) + ' Km2');
			
            if (selectedShapeName == 'rectangle' && value.type == 'rectangle') {
                $("#radius, #polygon").val('');
                $("#height").val(value.map_details.height);
                $("#width").val(value.map_details.width);
            }
            if (selectedShapeName == 'circle' && value.type == 'circle') {
                $("#height, #width, #polygon").val('');
                let rd = Math.round(parseFloat(value.map_details.radius) / 1000);
                $("#radius").val(rd);
            }
            if (selectedShapeName == 'polygon' && value.type == 'polygon') {
                $("#height, #width, #radius").val('');
                $("#polygone").val(value.map_details.sides);
            }
        }
    });
    // saveMapData();
}

// Update Selected Zone
function updateSelectedZone(zoneInfo) {
	
    $.each(allShapesData.zones, function(key, value) {
		
        if (value.type == selectedShapeName) {
            let lat = value.map_details.center_lat_lng.lat;
            let lng = value.map_details.center_lat_lng.lng;
			
            if (lat == selectedShapeCenter.lat() && lng == selectedShapeCenter.lng()) {
				
                if (zoneInfo != null) {
                    value.name = zoneInfo.zoneName;
                    value.comment1 = zoneInfo.comment1;
                    value.comment2 = zoneInfo.comment2;
                }

                if (selectedShapeName == 'rectangle') {

                    let height = parseFloat(zoneInfo.height);
                    let width = parseFloat(zoneInfo.width);
                    let recArea = Math.round(height * width);//ironman
                    let widthM = width * 1000;
                    let heightM = height * 1000;

                    var rectBounds = new google.maps.LatLngBounds(

                        new google.maps.LatLng(
                            google.maps.geometry.spherical.computeOffset(selectedShape.getBounds().getNorthEast(), heightM, 180).lat(), // S
                            google.maps.geometry.spherical.computeOffset(selectedShape.getBounds().getNorthEast(), widthM, 270).lng() //W
                        ),
                        selectedShape.getBounds().getNorthEast() //NE

                    );

                    selectedShape.setBounds(rectBounds);

                    let center = { "lat": selectedShape.getBounds().getCenter().lat(), "lng": selectedShape.getBounds().getCenter().lng() };

                    value.map_details.height = height;
                    value.map_details.width = width;
                    value.map_details.area = recArea;
                    value.map_details.center_lat_lng = center;
					
					let ne = selectedShape.getBounds().getNorthEast();
					let sw = selectedShape.getBounds().getSouthWest();
					let nw = new google.maps.LatLng(ne.lat(), sw.lng());
					let se = new google.maps.LatLng(sw.lat(), ne.lng());
					let swLat = sw.lat();
					let swLng = sw.lng();
					let neLat = ne.lat();
					let neLng = ne.lng();
					recSW = { swLat, swLng };
					recNE = { neLat, neLng };
				
					value.map_details.southWestCoordinate = recSW;					
                    value.map_details.northEastCoordinate = recNE;
                }

                if (selectedShapeName == 'circle') {
                    let rd = parseFloat(zoneInfo.radius) * 1000; //in Meters
                    selectedShape.setRadius(rd);
                    value.map_details.radius = rd;
                    let cirRadius = Math.round(rd);
                    let cirArea = Math.round(Math.PI * zoneInfo.radius * zoneInfo.radius);
                    value.map_details.area = cirArea;
                }			

				if (selectedShapeName == 'polygon') {
					saveMapData();
				}
				getDetectedMarkers(selectedShape, value.id);
				getUndetectedMarkers();
					
                $.each(allInfowindows, function(key2, val2) {

                    if (val2.id == value.id) {

                        let oldContent = val2.infowindow.getContent();
                        $("#infoWindowOldContent").html(oldContent);
                        let textColor = $("#infoWindowOldContent div.custom-info-window").css('color');
                        let zoneName = value.name;
                        let deliveryday = getMapControlTitles().deliveryDay + ': ' + value.comment1;
                        let comment2 = '$' + value.comment2;
                        let totalarea = 'Km: ' + $(".total-area").text();
                        let dCount = getDeliveryCount(value.id);;
                        let newContent = '';
						newContent = '<div class="custom-info-window text-center" style="color:' + textColor + ';">';

                        newContent +=
                            '<p class="map-infowindow-zonename">' + zoneName + '</p>' +
                            '<p class="map-infowindow-deliveryday">' + deliveryday + '</p>' +
                            '<p class="map-infowindow-comment2">' + comment2 + '</p>' +
                            '<p class="map-infowindow-totalarea">' + totalarea + '</p>' +
                            '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
                            '</div>';

                        val2.infowindow.setContent(newContent);

                        if (selectedShapeName == 'rectangle') {
                            val2.infowindow.setPosition(selectedShape.getBounds().getCenter());
                        }
                    }
                });
            }
        }
    });
}

// Set Delivery Data
function setDeliveryDataOnMap(data) {
	if (typeof google === 'object' && typeof google.maps === 'object'){
		if (data) {
			removeDestinationMarkers();
			$.each(data, function(key, value) {
				if (value != null) {
					let latitude = value.order_latitude_TR1;
					let longitude = value.order_longitud_TR1;
					let visit_destination_name = value.visit_destination_name;
					let tooltipTitle = visit_destination_name;
					let marker = new google.maps.Marker({
						map: map,
						position: new google.maps.LatLng(latitude, longitude),
						icon: './../images/destination-icon.png',
						title: tooltipTitle
					});
					marker.setMap(map);
					let dest_class = visit_destination_name.replace(/[\. ,)(:-]+/g, '');
					let order_pieces = commaNum(value.order_pieces);
					let order_volume = commaNum(value.order_volume);
					let order_weight = commaNum(value.order_weight);
					let order_cost_goods = commaNum(value.order_cost_goods);
					
					let contentString = '<div class="custom-info-window-destination units '+dest_class+' text-center">' +
						'<p class="map-infowindow-destination-name">' + value.visit_destination_name + '</p>' +
						'<p class="map-infowindow-pc">'+getMapControlTitles().order_pieces_shortform+': ' + order_pieces + '</p>' +
						'<p class="map-infowindow-m3">M3: ' + order_volume + '</p>' +
						'<p class="map-infowindow-kg">Kg: ' + order_weight + '</p>' +
						'<p class="map-infowindow-cost">$: ' + order_cost_goods + '</p>' +
						'</div>';
					let infoWindow = new google.maps.InfoWindow();
					infoWindow.setContent(contentString);
					infoWindow.open(map, marker);
					destinationMarkers.push({ 'visit_destination_name': visit_destination_name,'marker': marker });
					destinationMarkerIds.push(visit_destination_name);
					deliveryDayFilterDestinations.push(visit_destination_name);
				}
			});
		}
    }else{
		alert("Google Maps is not loaded properly. Please refresh this page.");
	}
}

// Remove Destination Markers
function removeDestinationMarkers() {
    $.each(destinationMarkers, function(key, value) {
        value.marker.setMap(null);
    });
    destinationMarkers = [];
    destinationMarkerIds = [];
}

function getDetectedMarkers(myShape, myZoneId) {

    if (myShape.type === 'polygon') {

        if (destinationMarkers && destinationMarkers.length > 0) {
            let zoneDelIds = [];

			if(deliveryDayFilterDestinations && deliveryDayFilterDestinations.length > 0){
				$.each(destinationMarkers, function(key, value) {
					
					if(deliveryDayFilterDestinations.includes(value.visit_destination_name)){
						let marker = value.marker;

						if (google.maps.geometry.poly.containsLocation(marker.getPosition(), myShape)) {
							zoneDelIds.push(value.visit_destination_name);
						}						
					}					
				});				
			}
			
            if (zoneDelIds) {
                ZoneWiseDeliveryRows[myZoneId] = zoneDelIds;
                zoneWiseDeliveryCount[myZoneId] = zoneDelIds;
            }
        }
    } else {

        let bounds = myShape.getBounds();

        if (destinationMarkers && destinationMarkers.length > 0) {
            let zoneDelIds = [];
			
			if(deliveryDayFilterDestinations && deliveryDayFilterDestinations.length > 0){
				$.each(destinationMarkers, function(key, value) {
					
					if(deliveryDayFilterDestinations.includes(value.visit_destination_name)){
						let marker = value.marker;
						
						if (bounds.contains(marker.getPosition())) {
							zoneDelIds.push(value.visit_destination_name);
						}						
					}
				});		
			}

            if (zoneDelIds) {
                ZoneWiseDeliveryRows[myZoneId] = zoneDelIds;
                zoneWiseDeliveryCount[myZoneId] = zoneDelIds;
            }
        }
    }
}


function DeletedShapeDetectMarkers(myShape, myZoneId) {

    if (ZoneWiseDeliveryRows) {
        if (ZoneWiseDeliveryRows[myZoneId]) {
            delete ZoneWiseDeliveryRows[myZoneId];
            delete zoneWiseDeliveryCount[myZoneId];
        }
    }
}


// Remove Shapes
function removeShapes() {
    $.each(shapes, function(key, value) {
        value.shape.setMap(null);
    });
    shapes = [];
}


function removeA(arr) {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


function setModelTableAfterDelivery() {
    if (shapes && shapes.length > 0) {
        $.each(shapes, function(key, value) {
            let myShape = value.shape;
            let zoneId = value.ZoneId;
            getDetectedMarkers(myShape, zoneId);
            getUndetectedMarkers();
        });
    }
}


// Remove Existing Zones from Model File when new Model File is Opened
function removeZonesForModelFile() {
    allShapesData.zones = [];
    removeShapes();
    ZoneWiseDeliveryRows = [];
    NoZoneDeliveryRows = [];
    ZoneId = 0;
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function getUndetectedMarkers() {

    let nozoneMarker = [];

    if (zoneWiseDeliveryCount) {
        let allZoneDeliveries = [];
        $.each(zoneWiseDeliveryCount, function(key, value) {
            if (value) {
                allZoneDeliveries = $.merge(allZoneDeliveries, value);
            }
        });

        if (allZoneDeliveries) {
			
			if(deliveryDayFilterDestinations && deliveryDayFilterDestinations.length > 0){
				nozoneMarker = $.grep(deliveryDayFilterDestinations, function(element) {
					return $.inArray(element, allZoneDeliveries) == -1;
				});					
			}
        }
    }

    if (nozoneMarker) {
        NoZoneDeliveryRows = nozoneMarker;
    }
}

function setInfoWindowText(myInfoWindow, textInfo) {

    let oldContent = myInfoWindow.getContent();
    $("#infoWindowOldContent").html(oldContent);
    let textColor = $("#infoWindowOldContent div.custom-info-window").css('color');
    let zoneName = $("#infoWindowOldContent p.map-infowindow-zonename").html();
    let deliveryday = $("#infoWindowOldContent p.map-infowindow-deliveryday").html();
    let comment2 = $("#infoWindowOldContent p.map-infowindow-comment2").html();
    let totalarea = $("#infoWindowOldContent p.map-infowindow-totalarea").html();
    let deliverycount = $("#infoWindowOldContent p.map-infowindow-deliverycount").html();
    if (textInfo) {
        totalarea = 'Km: ' + commaNum(textInfo.total_area) + ' Km2';//captain
        deliverycount = getMapControlTitles().count + ': ' + textInfo.deliverycount;
    }

    let newContent = '';
	newContent = '<div class="custom-info-window text-center" style="color:' + textColor + ';">';

    newContent += '<p class="map-infowindow-zonename">' + zoneName + '</p>' +
        '<p class="map-infowindow-deliveryday">' + deliveryday + '</p>' +
        '<p class="map-infowindow-comment2">' + comment2 + '</p>' +
        '<p class="map-infowindow-totalarea">' + totalarea + '</p>' +
        '<p class="map-infowindow-deliverycount">' + deliverycount + '</p>' +
        '</div>';

    myInfoWindow.setContent(newContent);

}

function getDeliveryCount(id) {
    if (zoneWiseDeliveryCount && zoneWiseDeliveryCount[id]) {
        return zoneWiseDeliveryCount[id].length;
    } else {
        return 0;
    }
}

function commaNum(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function checkForOverLappingZones() {

    isZoneOverlapped = false;
    $('#SaveModelFile').find('button.custom-btn').removeClass('zoneDisabled');
	
    if (shapes && shapes.length > 0) {
		let zones = shapes;
			
        $.each(shapes, function(key, value) {
            let myShape = value.shape;
			let bounds1;
			
            if (value.shapename == 'polygon') {
                bounds1 = new google.maps.LatLngBounds();
                myShape.getPath().forEach(function(path, index) {
                    bounds1.extend(path);
                });
            }else{
				bounds1 = myShape.getBounds();
			}			
			
			$.each(zones, function(key2, value2) {
				let thisShape = value2.shape;
				let bounds2;
				
				if (value2.shapename == 'polygon') {
					bounds2 = new google.maps.LatLngBounds();
					thisShape.getPath().forEach(function(path, index) {
						bounds2.extend(path);
					});
				}else{
					bounds2 = thisShape.getBounds();
				}				
				
				if(value.ZoneId != value2.ZoneId){
					
					if(value.shapename == 'polygon' && value2.shapename == 'polygon'){

						thisShape.getPath().forEach(function(path, index) {
							//console.log('path',path);
							
							if (google.maps.geometry.poly.containsLocation(path, myShape)) {
								//console.log('polygon condition');					
								isZoneOverlapped = true;
								$('#SaveModelFile').find('button.custom-btn').addClass('zoneDisabled');
								return false;							
							}						
						});
					}
					else{
						if(bounds1.intersects(bounds2) == true){
						//console.log('myShape zoneId=',value.ZoneId);
						//console.log('thisShape zoneId=',value2.ZoneId);						
							isZoneOverlapped = true;
							$('#SaveModelFile').find('button.custom-btn').addClass('zoneDisabled');
							return false;
						}				
					}				
				}
			});
        });
    }
}

// Delivery Filters Units
function deliveryFiltersUnits(data, unit) {
	if(data){
		
		$.each(data, function(key, value) {
			var heatmap = [];
			var color = value.color;//string
			let coordinatesData = value.data;//array
				
			$.each(coordinatesData, function(key2, value2) {
				heatmap.push(new google.maps.LatLng(parseFloat(value2.latitude), parseFloat(value2.longitude)));
			});
			HeatMapData.push({'color':color, 'coordinates':heatmap, 'heatmapData':[]});
			
		});
	}
	
	if(HeatMapData){

		$.each(HeatMapData, function(key, value) {
			let myColor = value.color;
			let pointColor = [
                'rgba(255, 255, 0, 0)',
                myColor.toString()
            ];
            let hmap = new google.maps.visualization.HeatmapLayer({
                data: value.coordinates,
                map: map,
                radius: 40,
                opacity: 1
            });
            hmap.set('gradient', pointColor);
			value.heatmapData = hmap;
		});
	}
}

function removeHeatmapDelivery() {
    if (HeatMapData) { 
		$.each(HeatMapData, function(key, value) {
			let heatmap = value.heatmapData;
			
			if(heatmap){
				heatmap.setMap(null);
			}
		});
		HeatMapData = [];
	}
}

function displayWarehouseMarkers(data){
		//Remove Old Markers
		removeWarehouseMarkers();
		
	if(data && data.length > 0){		
        $.each(data, function(key, value) {
            if (value != null) {
                let latitude = value.latitude;
                let longitude = value.longitude;
                let warehouse_name = value.warehouse_name;
                let tooltipTitle = warehouse_name;
                let marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(latitude, longitude),
                    icon: './../images/warehouse_icon.png',
                    title: tooltipTitle
                });
                marker.setMap(map);
                warehouseMarkers.push({ 'warehouse_name': warehouse_name,'marker': marker });
            }
        });		
	}
}

function removeWarehouseMarkers(){
	if(warehouseMarkers && warehouseMarkers.length > 0){
		$.each(warehouseMarkers, function(key, value) {
			value.marker.setMap(null);
		});
		warehouseMarkers = [];
	}	
}

function deliveryDayFilterDestinationsCheck(){
	
	if(statistics == true){

		if(destinationMarkers && deliveryDayFilterDestinations){
			$.each(destinationMarkers, function(key, value) {
				let visit_destination_name = value.visit_destination_name;
				let marker = value.marker;
				let dest_class = visit_destination_name.replace(/[\. ,)(:-]+/g, '');
				
				if(deliveryDayFilterDestinations.includes(visit_destination_name)){
					$('.custom-info-window-destination.units.'+dest_class).show();
				}else{
					$('.custom-info-window-destination.units.'+dest_class).hide();
				}
			});
		}else{
			$('.custom-info-window-destination.units').hide();
		}	
	}else{
		$('.custom-info-window-destination.units').hide();
	}
}


function updateAllZoneInfoWindowData() {
	
	if(allShapesData && allShapesData.zones){
		
		$.each(allShapesData.zones, function(key, value) {

			$.each(allInfowindows, function(key2, val2) {

				if (val2.id == value.id) {

					let oldContent = val2.infowindow.getContent();
					$("#infoWindowOldContent").html(oldContent);
					let textColor = $("#infoWindowOldContent div.custom-info-window").css('color');
					let zoneName = value.name;
					let deliveryday = getMapControlTitles().deliveryDay + ': ' + value.comment1;
					let comment2 = '$' + value.comment2;
					let totalarea = 'Km: ' + $(".total-area").text();
					let dCount = getDeliveryCount(value.id);
					let newContent = '';
					newContent = '<div class="custom-info-window text-center" style="color:' + textColor + ';">';

					newContent +=
						'<p class="map-infowindow-zonename">' + zoneName + '</p>' +
						'<p class="map-infowindow-deliveryday">' + deliveryday + '</p>' +
						'<p class="map-infowindow-comment2">' + comment2 + '</p>' +
						'<p class="map-infowindow-totalarea">' + totalarea + '</p>' +
						'<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
						'</div>';

					val2.infowindow.setContent(newContent);
				}
			});
		});
	}
}