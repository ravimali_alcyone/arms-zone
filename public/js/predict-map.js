/*  Website Name: ARMS Zone
 *  Author: Megual Morengo
 *  Description: All Map JS For Predict Screen
 */

// Initiate Map
var map, drawingManager, data, markerCount;
var searchDestinationMarker, rectangle, circle, polygon;
var shapes = [];
var markerDetectStatus = false;
//var locationMarker = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('predictMap'), {
        zoom: 7,
        center: {
            lat: 19.432608,
            lng: -99.133209
        },
        mapTypeId: 'roadmap',
        mapTypeControl: false,
        zoomControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_RIGHT
        },
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
        gestureHandling: 'cooperative',
        styles: [
            { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{ color: '#263c3f' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#6b9a76' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{ color: '#38414e' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#212a37' }]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#9ca5b3' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{ color: '#746855' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#1f2835' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#f3d19c' }]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{ color: '#2f3948' }]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{ color: '#17263c' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#515c6d' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{ color: '#17263c' }]
            }
        ]
    });

    var geocoder = new google.maps.Geocoder();

    document.getElementById('findBtn').addEventListener('click', function() {
        geocodeAddress(geocoder, map);
    });

    // Add Custom Cntrols On Map
    // Zoom In
    const zoomIn = document.createElement("IMG");
    zoomIn.setAttribute('src', './images/icon-dot-sselection.png');
    zoomIn.className = "zoom-in-control map-custom-control";
    zoomIn.addEventListener("click", (() => {
        return e => {
            map.setZoom(map.getZoom() + 1);
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(zoomIn);

    // Zoom Out
    const zoomOut = document.createElement("IMG");
    zoomOut.setAttribute('src', './images/icon-zoomout.png');
    zoomOut.className = "zoom-out-control map-custom-control";
    zoomOut.addEventListener("click", (() => {
        return e => {
            map.setZoom(map.getZoom() - 1);
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(zoomOut);

    // Full Zoom
    const fullZoomIn = document.createElement("IMG");
    fullZoomIn.setAttribute('src', './images/icon-all-dots.png');
    fullZoomIn.className = "full-zoom-in-control map-custom-control";
    fullZoomIn.addEventListener("click", (() => {
        return e => {
            map.setZoom(5);
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(fullZoomIn);

    // Autocomplete For Address Field
    // var input = document.getElementById('address');
    // var autocomplete = new google.maps.places.Autocomplete(input);

    // // Bind the map's bounds (viewport) property to the autocomplete object,
    // // so that the autocomplete requests use the current map bounds for the
    // // bounds option in the request.
    // autocomplete.bindTo('bounds', map);

    // // Set the data fields to return when the user selects a place.
    // autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

    // var marker = new google.maps.Marker({
    //     map: map,
    //     anchorPoint: new google.maps.Point(0, -29),
    //     icon: './../images/predict-location-icon.png'
    // });

    // autocomplete.addListener('place_changed', function() {
    //     $(document).on('click', '#findBtn', function(e) {
    //         e.preventDefault();
    //         marker.setVisible(false);
    //         var place = autocomplete.getPlace();
    //         if (!place.geometry) {
    //             // User entered the name of a Place that was not suggested and
    //             // pressed the Enter key, or the Place Details request failed.
    //             window.alert("No details available for input: '" + place.name + "'");
    //             return;
    //         }

    //         // If the place has a geometry, then present it on a map.
    //         if (place.geometry.viewport) {
    //             map.fitBounds(place.geometry.viewport);
    //         } else {
    //             map.setCenter(place.geometry.location);
    //             map.setZoom(17); // Why 17? Because it looks good.
    //         }
    //         marker.setPosition(place.geometry.location);
    //         marker.setVisible(true);
    //         searchDestinationMarker = marker;
    //         map.setZoom(7);

    //         var address = '';
    //         if (place.address_components) {
    //             address = [
    //                 (place.address_components[0] && place.address_components[0].short_name || ''),
    //                 (place.address_components[1] && place.address_components[1].short_name || ''),
    //                 (place.address_components[2] && place.address_components[2].short_name || '')
    //             ].join(' ');
    //         }
    // 		getDetectedMarker();
    //     });
    // });
    //$(".loader").css('display', 'none');
}

// Read JSON File Click on Open Btn of Model Section
function readJson(data) {

    // Remove Previous Destination Markers and Shapes
    removeShapes();

    // Draw Shapes
    $.each(data.data, function(key, value) {
       // console.log(value);
       // console.log(value.map_details);
        // Rectangle
        if (value.type == 'rectangle') {
            rectangle = new google.maps.Rectangle({
                strokeColor: value.map_details.bg_color,
                fillColor: value.map_details.bg_color,
                strokeOpacity: 1,
                strokeWeight: 1,
                fillOpacity: 0.40,
                strokeOpacity: 1,
                strokeWeight: 2,
                fillOpacity: 0.40,
                zIndex: 1,
                map: map,
                bounds: new google.maps.LatLngBounds(
                        new google.maps.LatLng(value.map_details.southWestCoordinate.swLat, value.map_details.southWestCoordinate.swLng), // SW
                        new google.maps.LatLng(value.map_details.northEastCoordinate.neLat, value.map_details.northEastCoordinate.neLng)) // NE
            });
            rectangle.setMap(map);
            shapes.push({ 'shapename': 'rectangle', 'shape': rectangle, 'info': value });
            //console.log('shapes',shapes);
        }

        // Circle
        if (value.type == 'circle') {
            let latitude = parseFloat(value.map_details.center_lat_lng.lat);
            let longitude = parseFloat(value.map_details.center_lat_lng.lng);
            circle = new google.maps.Circle({
                strokeColor: value.map_details.bg_color,
                fillColor: value.map_details.bg_color,
                strokeOpacity: 1,
                strokeWeight: 1,
                fillOpacity: 0.40,
                strokeOpacity: 1,
                strokeWeight: 2,
                fillOpacity: 0.40,
                zIndex: 1,
                map: map,
                center: { lat: parseFloat(latitude.toFixed(6)), lng: parseFloat(longitude.toFixed(6)) },
                radius: parseFloat(value.map_details.radius)
            });
            circle.setMap(map);
            shapes.push({ 'shapename': 'circle', 'shape': circle, 'info': value });
        }

        // Polygon
        if (value.type == 'polygon') {
            var polygonCoordinates = [];
            $.each(value.map_details.coordinates, function(key, value) {
                polygonCoordinates.push({ lat: parseFloat(value.lat), lng: parseFloat(value.lng) });
            });

            polygon = new google.maps.Polygon({
                paths: polygonCoordinates,
                strokeColor: value.map_details.bg_color,
                fillColor: value.map_details.bg_color,
                strokeOpacity: 1,
                strokeWeight: 1,
                fillOpacity: 0.40,
                strokeOpacity: 1,
                strokeWeight: 2,
                fillOpacity: 0.40,
                zIndex: 1,
            });
            polygon.setMap(map);
            shapes.push({ 'shapename': 'polygon', 'shape': polygon, 'info': value });
        }
    });

    getDetectedMarker();
}

// Remove Destination icons and Shapes on Google Map
function removeShapes() {
    // Remove Shapes
    $.each(shapes, function(key, value) {
        value.shape.setMap(null);
    });
    shapes = [];
}


function getDetectedMarker() {
   // console.log('in getDetectedMarker');
    markerDetectStatus = false;
   // console.log('shapes',shapes);
    if (shapes) {
        $.each(shapes, function(key, value) {
            let myShape = value.shape;
            let type = value.shapename;
            if (searchDestinationMarker) {
                let marker = searchDestinationMarker;
				if (type === 'polygon') {
					if (google.maps.geometry.poly.containsLocation(marker.getPosition(), myShape)) {
						markerDetectStatus = true;
						//console.log('value.info',value.info);
						displayMarkerDetectInfo(value.info);
						return false;
					} else {
						displayMarkerDetectInfo();
					}
				}else{
					if (myShape.getBounds().contains(marker.getPosition())) {
						markerDetectStatus = true;
						//console.log('value.info',value.info);
						displayMarkerDetectInfo(value.info);
						return false;
					} else {
						displayMarkerDetectInfo();
					}
					
				}
            }
        });
    }

}
var marker;
// Find Places Using Geocode
function geocodeAddress(geocoder, resultsMap) {
    /*     // For Remove Previous Marker
        if (locationMarker.length >= 1) {
            for (i = 0; i < locationMarker.length; i++) {
                locationMarker[i].setMap(null);
                console.log(locationMarker)
            }
        } */

    // Show Marker on New Location
    var address = document.getElementById('address').value;
    if (!isNaN(address) && address.length == 5) {
        address = 'CP ' + address;
    }
    // console.log(address);
    $("#address").val(address);
    geocoder.geocode({ 'address': address, 'componentRestrictions': { 'country': 'MX' } }, function(results, status) {
        if (status === 'OK') {
            searchDestinationMarker = false;
            resultsMap.setCenter(results[0].geometry.location);

            // Remove marker on change location
            if (marker && marker.setMap) {
                marker.setMap(null);
            }

            marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location,
                anchorPoint: new google.maps.Point(0, -29),
                icon: './../images/predict-location-icon.png'
            });
            // locationMarker.push(marker);
            searchDestinationMarker = marker;
            getDetectedMarker();
        } else {
            // alert('Geocode was not successful for the following reason: ' + status);
            warningAlert(getMapTitles().address_not_found);
        }
    });
}