// Read JSON File Click on Open Btn of Model Section
function readJsonForPredict(data) {
	//console.log('in readJson',data);
    // Draw Shapes
	
    $.each(data, function(key, value) {
        ZoneId += 1;
        value.id = ZoneId;			
        allShapesData.zones.push(value);

        // Rectangle
        if (value.type == 'rectangle') {
			infowindowId += 1;
			let infoWindowRec;			
            let shapeNumber = ZoneId;
            var rectangle = new google.maps.Rectangle({
                strokeColor: value.map_details.bg_color,
                fillColor: value.map_details.bg_color,
                strokeOpacity: 1,
                strokeWeight: 1,
                fillOpacity: 0.40,
                strokeOpacity: 1,
                strokeWeight: 2,
                fillOpacity: 0.40,
                //editable: true,
                draggable: true,
                clickable: true,
                suppressUndo: true,
                geodesic: true,
                zIndex: 1,
                map: map,
                bounds: new google.maps.LatLngBounds(
                        new google.maps.LatLng(value.map_details.southWestCoordinate.swLat, 
						value.map_details.southWestCoordinate.swLng), // SW
                        new google.maps.LatLng(value.map_details.northEastCoordinate.neLat, 
						value.map_details.northEastCoordinate.neLng)) // NE
            });
            rectangle.type = value.type;
            rectangle.setMap(map);
			
			shapes.push({ 'shapename': 'rectangle', 'shape': rectangle, 'ZoneId': ZoneId });
			getDetectedMarkers(rectangle, ZoneId);
			getUndetectedMarkers();	
		
			google.maps.event.addListener(rectangle, 'click', function(e) {
				setSelection(rectangle);
			});
		
            // InfoWindow
			if (infoWindowRec) {
				infoWindowRec.close();
			}
	
            infoWindowRec = new google.maps.InfoWindow();
			let dCount = getDeliveryCount(ZoneId);	
            var contentString = '<div class="custom-info-window text-center" style="color:' + value.text_color + '";>' +
                '<p class="map-infowindow-zonename">' + value.name + '</p>' +
                '<p class="map-infowindow-deliveryday">' + getMapControlTitles().deliveryDay + ': ' + value.comment1 + '</p>' +
                '<p class="map-infowindow-comment2">$' + value.comment2 + '</p>' +
                '<p class="map-infowindow-totalarea">Km: ' + commaNum(Math.round(parseFloat(value.map_details.area))) + ' Km2</p>' +
                '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
                '</div>';
		
            infoWindowRec.setPosition(rectangle.getBounds().getCenter());
            infoWindowRec.setContent(contentString);
            infoWindowRec.setOptions({
                pixelOffset: new google.maps.Size(0, 50)
            });

			if (statistics == true) {
				infoWindowRec.open(map, rectangle);
			} else {
				infoWindowRec.open(map, rectangle);
				setTimeout(function() {
					$(".custom-info-window").hide();
				}, 10);
			}

			allInfowindows.push({ 'infowindow': infoWindowRec, 'id': ZoneId });
	
			setTimeout(() => {
				$(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
			}, 30);
	
            // Event Listeners
			google.maps.event.addListener(rectangle, 'drag', function() {
				infoWindowRec.setPosition(rectangle.getBounds().getCenter());
				infoWindowRec.close(map, rectangle);
			});

			google.maps.event.addListener(rectangle, 'dragend', function() {
				setSelection(rectangle);
				infoWindowRec.setPosition(rectangle.getBounds().getCenter());
				setInfoWindowText(infoWindowRec,false);
				
				if (statistics == true) {
					infoWindowRec.open(map, rectangle);
				} else {
					infoWindowRec.open(map, rectangle);
					setTimeout(function() {
						$(".custom-info-window").hide();
					}, 10);
				}

			});

			rectangle.addListener('bounds_changed', (function() {
				//rkmali
				var timer;
				return function() {
					clearTimeout(timer);
					timer = setTimeout(function() {
						var ne = rectangle.getBounds().getNorthEast();
						var sw = rectangle.getBounds().getSouthWest();
						var nw = new google.maps.LatLng(ne.lat(), sw.lng());
						var se = new google.maps.LatLng(sw.lat(), ne.lng());
								
						if(allShapesData.zones){
							$.each(allShapesData.zones, function(key, val) {
								if (val.id == shapeNumber && val.type == 'rectangle') {
									val.map_details.northEastCoordinate = recNE;
									val.map_details.southWestCoordinate = recSW;
									
									//height
									var heightVal = google.maps.geometry.spherical.computeDistanceBetween(
									  new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().east),
									  new google.maps.LatLng(rectangle.bounds.toJSON().south, rectangle.bounds.toJSON().east)
									);
									
									//width
									var widthVal = google.maps.geometry.spherical.computeDistanceBetween(
									  new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().east),
									  new google.maps.LatLng(rectangle.bounds.toJSON().north, rectangle.bounds.toJSON().west)
									);
									
									val.map_details.width = Math.round((widthVal/1000)); //in Km
									val.map_details.height = Math.round((heightVal/1000)); // in Km
									val.map_details.area =  (val.map_details.height * val.map_details.width); //in Km
									val.map_details.center_lat_lng.lat = rectangle.getBounds().getCenter().lat();
									val.map_details.center_lat_lng.lng = rectangle.getBounds().getCenter().lng();

									$("#xLatitude").val(val.map_details.center_lat_lng.lat);
									$("#length").val(val.map_details.center_lat_lng.lng);
									$("#height").val(val.map_details.height);
									$("#width").val(val.map_details.width);
									$(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
									
									setSelection(rectangle);
									getDetectedMarkers(rectangle, val.id);
									getUndetectedMarkers();
									
									infoWindowRec.setPosition(rectangle.getBounds().getCenter());
									
									let dCount = getDeliveryCount(val.id);
									let textInfo = {'total_area':Math.round(parseFloat(val.map_details.area)),'deliverycount':dCount};
									setInfoWindowText(infoWindowRec,textInfo);
																
									if (statistics == true) {
										infoWindowRec.open(map, rectangle);
									} else {
										infoWindowRec.open(map, rectangle);
										setTimeout(function() {
											$(".custom-info-window").hide();
										}, 10);
									}							
								}
							});
							
							saveMapData();
						}
					}, 500);
				}
			}()));
        }

        // Circle
        if (value.type == 'circle') {
			infowindowId += 1;
			let infoWindowCir;	
			let shapeNumber = ZoneId;
            let latitude = parseFloat(value.map_details.center_lat_lng.lat);
            let longitude = parseFloat(value.map_details.center_lat_lng.lng);

            var circle = new google.maps.Circle({
                strokeColor: value.map_details.bg_color,
                fillColor: value.map_details.bg_color,
                strokeOpacity: 1,
                strokeWeight: 1,
                fillOpacity: 0.40,
                strokeOpacity: 1,
                strokeWeight: 2,
                fillOpacity: 0.40,
                // editable: true,
                draggable: true,
                clickable: true,
                suppressUndo: true,
                geodesic: true,
                zIndex: 1,

                map: map,
                center: { lat: parseFloat(latitude), lng: parseFloat(longitude) },
                radius: parseFloat(value.map_details.radius)
            });
            circle.type = value.type;
            circle.setMap(map);
			
			shapes.push({ 'shapename': 'circle', 'shape': circle, 'ZoneId': ZoneId });
			getDetectedMarkers(circle, ZoneId);
			getUndetectedMarkers();			

			google.maps.event.addListener(circle, 'click', function(e) {
				setSelection(circle);
			});
						
            // InfoWindow
			if (infoWindowCir) {
				infoWindowCir.close();
			}
			
            infoWindowCir = new google.maps.InfoWindow();
			let dCount = getDeliveryCount(ZoneId);	
			
            var contentString = '<div class="custom-info-window text-center" style="color:' + value.text_color + '";>' +
                '<p class="map-infowindow-zonename">' + value.name + '</p>' +
                '<p class="map-infowindow-deliveryday">' + getMapControlTitles().deliveryDay + ': ' + value.comment1 + '</p>' +
                '<p class="map-infowindow-comment2">$' + value.comment2 + '</p>' +
                '<p class="map-infowindow-totalarea">Km: ' + commaNum(Math.round(parseFloat(value.map_details.area))) + ' Km2</p>' +
                '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
                '</div>';
            infoWindowCir.setPosition(circle.getCenter());
            infoWindowCir.setContent(contentString);
            infoWindowCir.setOptions({
                pixelOffset: new google.maps.Size(0, 50)
            });

			if (statistics == true) {
				infoWindowCir.open(map, circle);
			} else {
				infoWindowCir.open(map, circle);
				setTimeout(function() {
					$(".custom-info-window").hide();
				}, 10);
			}

			allInfowindows.push({ 'infowindow': infoWindowCir, 'id': ZoneId });

			setTimeout(() => {
				$(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
			}, 30);

			//Event Listners
			google.maps.event.addListener(circle, 'drag', function() {
				infoWindowCir.close(map, circle);
			});

			google.maps.event.addListener(circle, 'dragend', function() {
				setSelection(circle);
				infoWindowCir.setPosition(circle.getBounds().getCenter());
				setInfoWindowText(infoWindowCir,false);

				if (statistics == true) {
					infoWindowCir.open(map, circle);
				} else {
					infoWindowCir.open(map, circle);
					setTimeout(function() {
						$(".custom-info-window").hide();
					}, 10);
				}
			
			});

			circle.addListener('bounds_changed', (function() {

				var timer;
				return function() {
					clearTimeout(timer);
					timer = setTimeout(function() {
						
						if(allShapesData.zones){
							$.each(allShapesData.zones, function(key, val) {
								if (val.id == shapeNumber && val.type == 'circle') {
									val.map_details.center_lat_lng = { "lat": circle.getCenter().lat(), "lng": circle.getCenter().lng() };
									val.map_details.radius = circle.getRadius(); //original
									val.map_details.bg_color = circle.fillColor;
									let rd = Math.round(circle.getRadius()/1000); //in Km
									val.map_details.area = Math.round(Math.PI * rd * rd); //in Km

									$("#height, #width, #polygone").val('');
									$("#radius").val(rd);
									$("#xLatitude").val(val.map_details.center_lat_lng.lat);
									$("#length").val(val.map_details.center_lat_lng.lng);
									$(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
									setSelection(circle);
									getDetectedMarkers(circle, val.id);
									getUndetectedMarkers();
									
									infoWindowCir.setPosition(circle.getBounds().getCenter());
									
									let dCount = getDeliveryCount(val.id);
									let textInfo = {'total_area':Math.round(parseFloat(val.map_details.area)),'deliverycount':dCount};
									
									setInfoWindowText(infoWindowCir,textInfo);
														
									if (statistics == true) {
										infoWindowCir.open(map, circle);
									} else {
										infoWindowCir.open(map, circle);
										setTimeout(function() {
											$(".custom-info-window").hide();
										}, 10);
									}						
								}
							});
							saveMapData();
						}
					}, 500);
				}
			}()));
			
			circle.addListener('radius_changed', (function() {
				//rkmali
				var timer;
				return function() {
					clearTimeout(timer);
					timer = setTimeout(function() {
			
						if(allShapesData.zones){
							$.each(allShapesData.zones, function(key, val) {
								if (val.id == shapeNumber && val.type == 'circle') {
									val.map_details.center_lat_lng = { "lat": circle.getCenter().lat(), "lng": circle.getCenter().lng() };
									val.map_details.radius = circle.getRadius(); //original
									val.map_details.bg_color = circle.fillColor;
									let rd = Math.round(circle.getRadius()/1000); //in Km
									val.map_details.area = Math.round(Math.PI * rd * rd); //in Km

									$("#height, #width, #polygone").val('');
									$("#radius").val(rd);
									$("#xLatitude").val(val.map_details.center_lat_lng.lat);
									$("#length").val(val.map_details.center_lat_lng.lng);
									$(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
									setSelection(circle);
									getDetectedMarkers(circle, val.id);
									getUndetectedMarkers();
									
									infoWindowCir.setPosition(circle.getBounds().getCenter());
									
									let dCount = getDeliveryCount(val.id);
									let textInfo = {'total_area':Math.round(parseFloat(val.map_details.area)),'deliverycount':dCount};
									
									setInfoWindowText(infoWindowCir,textInfo);
														
									if (statistics == true) {
										infoWindowCir.open(map, circle);
									} else {
										infoWindowCir.open(map, circle);
										setTimeout(function() {
											$(".custom-info-window").hide();
										}, 10);
									}							
								}
							});
							
							saveMapData();
						}
					}, 500);
				}
			}()));		
        }

        // Polygon
        if (value.type == 'polygon') {
			infowindowId += 1;
			let infoWindowPol;				
            var polygonCoordinates = [];
            let shapeNumber = ZoneId;
			
            $.each(value.map_details.coordinates, function(key, value) {
                polygonCoordinates.push({ lat: parseFloat(value.lat), lng: parseFloat(value.lng) });
            });
            var polygon = new google.maps.Polygon({
                paths: polygonCoordinates,
                strokeColor: value.map_details.bg_color,
                fillColor: value.map_details.bg_color,
                strokeOpacity: 1,
                strokeWeight: 1,
                fillOpacity: 0.40,
                strokeOpacity: 1,
                strokeWeight: 2,
                fillOpacity: 0.40,
                //editable: true,
                draggable: true,
                clickable: true,
                suppressUndo: true,
                geodesic: true,
                zIndex: 1,
            });
            polygon.type = value.type;
            polygon.setMap(map);

			shapes.push({ 'shapename': 'polygon', 'shape': polygon, 'ZoneId': ZoneId });
			getDetectedMarkers(polygon, ZoneId);
			getUndetectedMarkers();
			
			google.maps.event.addListener(polygon, 'click', function(e) {
				setSelection(polygon);
			});
			
            // InfoWindow
            let bounds = new google.maps.LatLngBounds();
            polygon.getPath().forEach(function(element, index) {
                bounds.extend(element)
            });
			
			if (infoWindowPol) {
				infoWindowPol.close();
			}
			
            infoWindowPol = new google.maps.InfoWindow();
			
			let dCount = getDeliveryCount(ZoneId);	
			
            var contentString = '<div class="custom-info-window text-center" style="color:' + value.text_color + '";>' +
                '<p class="map-infowindow-zonename">' + value.name + '</p>' +
                '<p class="map-infowindow-deliveryday">' + getMapControlTitles().deliveryDay + ': ' + value.comment1 + '</p>' +
                '<p class="map-infowindow-comment2">$' + value.comment2 + '</p>' +
                '<p class="map-infowindow-totalarea">Km: ' + commaNum(Math.round(parseFloat(value.map_details.area))) + ' Km2</p>' +
                '<p class="map-infowindow-deliverycount">' + getMapControlTitles().count + ': ' + dCount + '</p>' +
                '</div>';
            infoWindowPol.setPosition(bounds.getCenter());
            infoWindowPol.setContent(contentString);
            infoWindowPol.setOptions({
                pixelOffset: new google.maps.Size(0, 50)
            });

			if (statistics == true) {
				infoWindowPol.open(map, polygon);
			} else {
				infoWindowPol.open(map, polygon);
				setTimeout(function() {
					$(".custom-info-window").hide();
				}, 10);
			}
			
			allInfowindows.push({ 'infowindow': infoWindowPol, 'id': ZoneId });
	
			setTimeout(() => {
				$(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
			}, 30);
	
			//Event Listners
			google.maps.event.addListener(polygon, 'drag', function() {
				infoWindowPol.close(map, polygon);
			});

			polygon.getPaths().forEach(function(path, index) {
				google.maps.event.addListener(path, 'insert_at', function() {
					editOrDragPolygon(false);
					//saveMapData();
				});

				google.maps.event.addListener(path, 'set_at', function() {
					editOrDragPolygon(true);			
				});
			});

			google.maps.event.addListener(polygon, 'dragend', function() {
				
				if(allShapesData.zones){
					$.each(allShapesData.zones, function(key, val) {
						if (val.id == shapeNumber && val.type == 'polygon') {
							$("#xLatitude").val(val.map_details.center_lat_lng.lat);
							$("#length").val(val.map_details.center_lat_lng.lng);
							$("#polygone").val(val.map_details.sides);
							$(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
							setSelection(polygon);
							getDetectedMarkers(polygon, val.id);
							getUndetectedMarkers();
							
							bounds = new google.maps.LatLngBounds();
							polygon.getPath().forEach(function(element, index) {
								bounds.extend(element)
							});
							
							infoWindowPol.setPosition(bounds.getCenter());
							
							let dCount = getDeliveryCount(val.id);
							let textInfo = {'total_area':Math.round(parseFloat(val.map_details.area)),'deliverycount':dCount};
							
							setInfoWindowText(infoWindowPol,textInfo);
												
							if (statistics == true) {
								infoWindowPol.open(map, polygon);
							} else {
								infoWindowPol.open(map, polygon);
								setTimeout(function() {
									$(".custom-info-window").hide();
								}, 10);
							}					
						}
					});
								
					saveMapData();
				}
			});

			function editOrDragPolygon(param) {		
				
				let polyCoord = polygon.getPath().getArray();
				let bounds = new google.maps.LatLngBounds();
				let sides2 = [];
				for (var i = 0; i < polygon.getPath().getLength(); i++) {
					// for each side in path, compute center and length
					let start = polygon.getPath().getAt(i);
					let end = polygon.getPath().getAt(i < polygon.getPath().getLength() - 1 ? i + 1 : 0);
					let sideLength = google.maps.geometry.spherical.computeDistanceBetween(start, end);
					sideLength = Math.round(sideLength/1000);//in Km
					sides2.push(sideLength);
				}
				polygon.getPath().forEach(function(path, index) {
					bounds.extend(path);
				});
				
				if(allShapesData.zones){
					$.each(allShapesData.zones, function(key, val) {
						if (val.id == shapeNumber && val.type == 'polygon') {
							val.map_details.coordinates = [];
							$.each(polyCoord, function(key, value) {
								val.map_details.coordinates.push({ "lat": value.lat(), "lng": value.lng() });
							});
							val.map_details.area = Math.round(google.maps.geometry.spherical.computeArea(polygon.getPath()) / (1000 * 1000));  //in Km
							val.map_details.center_lat_lng.lat = bounds.getCenter().lat();
							val.map_details.center_lat_lng.lng = bounds.getCenter().lng();
							val.map_details.sides = sides2;

							if (param != true) {
								$("#xLatitude").val(val.map_details.center_lat_lng.lat);
								$("#length").val(val.map_details.center_lat_lng.lng);
								$("#polygone").val(val.map_details.sides);
								$(".total-area").text(commaNum(Math.round(parseFloat(val.map_details.area))) + ' Km2');
								setSelection(polygon);
								getDetectedMarkers(polygon, val.id);
								getUndetectedMarkers();
								
								infoWindowPol.setPosition(bounds.getCenter());
								
								let dCount = getDeliveryCount(val.id);
								let textInfo = {'total_area':Math.round(parseFloat(val.map_details.area)),'deliverycount':dCount};
								
								setInfoWindowText(infoWindowPol,textInfo);
													
								if (statistics == true) {
									infoWindowPol.open(map, polygon);
								} else {
									infoWindowPol.open(map, polygon);
									setTimeout(function() {
										$(".custom-info-window").hide();
									}, 10);
								}
					
							}
						}
					});
				}
			}
        }
    });
    saveMapData();
}

function commaNum(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}