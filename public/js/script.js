/*  Website Name: ARMS Zone
 *  Author: Megual Morengo
 *  Description: Main JS File
 */

// Change Select-box Arrows
$(".custom-select-box-wrapper>select").click(function() {
    $(".custom-select-box-wrapper").removeClass('active');
    $(this).closest('.custom-select-box-wrapper').addClass('active');
});
$(".custom-select-box-wrapper>select").focusout(function() {
    $(".custom-select-box-wrapper").removeClass('active');
});

// For Color Picker
$(".color-picker-btn>img").click(function() {
    if ($(this).closest('.picker-wrapper').find('.color-picker').is(":visible")) {
        $(this).closest('.picker-wrapper').find('.color-picker').fadeOut();
    } else {
        $(this).closest('.picker-wrapper').find('.color-picker').fadeIn();
    }
});

// For Map Popup
$(".map-popup-option").click(function() {
    if (!$(".map-main-popup").is(":visible")) {
        $(".map-popup-option span:first-child").text(getMapControlTitles().hide_text);
        $(this).addClass('active');
        setTimeout(function() {
            $('head').append('<style>.zoom-in-control, .zoom-out-control, .full-zoom-in-control{right: 230px !important; }</style>');
        }, 50);
    } else {
        $(".map-popup-option span:first-child").text(getMapControlTitles().show_text);
        $(this).removeClass('active');
        setTimeout(function() {
            $('head').append('<style>.zoom-in-control, .zoom-out-control, .full-zoom-in-control{right: 16px !important; }</style>');
        }, 200);
    }

    $(".map-main-popup, .statistics-on-map").fadeToggle();
});

// For Accordian
$(".card .card-header button").click(function() {
    if ($(this).hasClass("active")) {
        $(this).removeClass("active");
    } else {
        $(this).addClass("active");
    }
});

// Success Snackbar Toast
function successAlert(message) {
    $.toast({
        heading: '<img src="./../images/icon-check-circle.png" alt="icon-check-circle" width="20" class="check-circle">' + message,
        bgColor: "#4caf50",
        position: 'top-right',
        hideAfter: false
    })
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, 5000);
}


// Error Snackbar Toast
function errorAlert(message) {
    $.toast({
        heading: '<img src="./../images/icon-warning.png" alt="icon-warning" width="20" class="check-circle">' + message,
        showHideTransition: 'fade',
        bgColor: "#a94442",
        position: 'top-right',
        hideAfter: false,
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, 5000);
}

// Warning Snackbar Toast
function warningAlert(message) {
    $.toast({
        heading: '<img src="./../images/icon-warning.png" alt="icon-warning" width="20" class="check-circle">' + message,
        showHideTransition: 'fade',
        bgColor: "#ffc107",
        position: 'top-right',
        hideAfter: false,
    });
    setTimeout(function() {
        $(".jq-toast-single").css('display', 'none');
    }, 5000);
}