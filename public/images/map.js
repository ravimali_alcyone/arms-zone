/*  Website Name: ARMS Zone
 *  Author: Megual Morengo
 *  Description: All Map JS For Zones Screen
 */

var drawingManager, selectedShape, recSW, recNE;
var circleArea, rectabgleArea, polygonArea, rectangleWidth, rectangleHeight;
var rectangleInfowindowTextColor, circleInfowindowTextColor, polygonInfowindowTextColor;
var statistics = true;
var allRectangles = [];
var allCircles = [];
var allPolygons = [];
var allInfowindows = [];
var infowindowId = 0;
// var allShapesData = {
//     "rectangles": {
//         "data": [],
//         "bgColor": "#fd6d21",
//         "textColor": "#fff"
//     },
//     "circles": {
//         "data": [],
//         "bgColor": "#fd6d21",
//         "textColor": "#fff"
//     },
//     "polygons": {
//         "data": [],
//         "bgColor": "#fd6d21",
//         "textColor": "#fff"
//     }
// };
var allShapesData = {
    "zones": []
};
var shapeCount = 0;

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: {
            lat: 19.432608,
            lng: -99.133209
        },
        mapTypeId: 'roadmap',
        mapTypeControl: false,
        zoomControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_RIGHT
        },
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
        gestureHandling: 'cooperative',
        styles: [
            { elementType: 'geometry', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.stroke', stylers: [{ color: '#242f3e' }] },
            { elementType: 'labels.text.fill', stylers: [{ color: '#746855' }] },
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{ color: '#263c3f' }]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#6b9a76' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{ color: '#38414e' }]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#212a37' }]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#9ca5b3' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{ color: '#746855' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{ color: '#1f2835' }]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#f3d19c' }]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{ color: '#2f3948' }]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#d59563' }]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{ color: '#17263c' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{ color: '#515c6d' }]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{ color: '#17263c' }]
            }
        ]
    });

    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: false,
        drawingControl: false,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT,
            style: google.maps.MapTypeControlStyle.DEFAULT,
            drawingModes: ['rectangle', 'circle', 'polygon']
        },
        polygonOptions: {
            strokeColor: '#fd6d21',
            strokeOpacity: 1,
            strokeWeight: 2,
            fillColor: '#fd6d21',
            fillOpacity: 0.40,
            editable: true,
            draggable: true,
            suppressUndo: true,
            geodesic: true,
            zIndex: 1
        },
        circleOptions: {
            strokeColor: '#fd6d21',
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: '#fd6d21',
            fillOpacity: 0.40,
            editable: true,
            draggable: true,
            suppressUndo: true,
            geodesic: true,
            zIndex: 1
        },
        rectangleOptions: {
            strokeColor: '#fd6d21',
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: '#fd6d21',
            fillOpacity: 0.40,
            editable: true,
            draggable: true,
            suppressUndo: true,
            geodesic: true,
            zIndex: 1
        }
    });
    drawingManager.setMap(map);
    $(".loader").css('display', 'none');

    // Process After Overlay Complete
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
        var newShape = e.overlay;

        if (e.type == 'rectangle') {
            allRectangles.push(e);
            google.maps.event.addListener(e.overlay, 'dragend', function() {
                rectangleInfo(e);
            });
            rectangleInfo(e);

            allShapesData.zones.push({
                "id": shapeCount,
                "name": "Zone-1",
                "type": e.type,
                "comment1": "Thursday",
                "comment2": "Friday",
                "map_details": {
                    "southWestCoordinate": recSW,
                    "northEastCoordinate": recNE,
                    "center_lat_lng": { "lat": "123456", "lng": "123456" },
                    "height": rectangleHeight,
                    "width": rectangleWidth,
                    "area": rectabgleArea,
                    "bg_color": e.overlay.fillColor,
                    "text_color": "#fff"
                }
            });
            // console.log(allShapesData.zones)
        }

        if (e.type == 'circle') {
            allCircles.push(e);
            google.maps.event.addListener(e.overlay, 'dragend', function() {
                circleInfo(e);
            });
            circleInfo(e);

            allShapesData.zones.push({
                "id": shapeCount,
                "name": "Zone-2",
                "type": e.type,
                "comment1": "Thursday",
                "comment2": "Friday",
                "map_details": {
                    "radius": (e.overlay.getRadius() / 1000),
                    "center_lat_lng": { "lat": e.overlay.getCenter().lat(), "lng": e.overlay.getCenter().lng() },
                    "area": circleArea,
                    "bg_color": e.overlay.fillColor,
                    "text_color": "#fff"
                }
            });
            // console.log(allShapesData.zones)
        }

        if (e.type == 'polygon') {
            allPolygons.push(e);
            google.maps.event.addListener(e.overlay, 'dragend', function() {
                polygonInfo(e);
            });
            polygonInfo(e);

            let polygonCoordinates = [];
            let paths = e.overlay.getPath().getArray();
            $.each(paths, function(key, value) {
                polygonCoordinates.push({ "lat": value.lat(), "lng": value.lng() });
            });

            allShapesData.zones.push({
                'id': shapeCount,
                "name": "Zone-3",
                "type": e.type,
                "comment1": "Thursday",
                "comment2": "Friday",
                "map_details": {
                    'coordinates': polygonCoordinates,
                    "area": polygonArea,
                    "bg_color": e.overlay.fillColor,
                    "text_color": "#fff",
                    "center_lat_lng": { "lat": "123456", "lng": "123456" },
                    "sides": [
                        "400",
                        "200",
                        "100",
                        "300"
                    ],
                }
            });
            // console.log(allShapesData.zones)
        }

        newShape.type = e.type;
        if (e.type !== google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setDrawingMode(null);
        }
        // Add an event listener that selects the newly-drawn shape when the user mouses down on it.
        google.maps.event.addListener(newShape, 'click', function(e) {
            setSelection(newShape);
        });
        setSelection(newShape);
    });

    // Clear The Current Selection When The Drawing Mode Is Changed
    google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
    google.maps.event.addListener(map, 'click', clearSelection);
    document.getElementById("map").addEventListener('keyup', deleteSelectedShapeUsingBtn);

    // ***** Add Custom Cntrols On Map ***** //
    // Screen Capture
    const cameraImg = document.createElement("IMG");
    cameraImg.setAttribute('src', './images/icon-screenshot.png');
    cameraImg.setAttribute('id', 'screenCaptureBtn');
    cameraImg.setAttribute('data-html2canvas-ignore', 'true');
    cameraImg.className = 'camera-control map-custom-control';
    cameraImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(cameraImg);

    // Polygon
    const polyImg = document.createElement("IMG");
    polyImg.setAttribute('src', './images/icon-hexagon.png');
    polyImg.setAttribute('data-html2canvas-ignore', 'true');
    polyImg.className = "polygone-control map-custom-control";
    polyImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(polyImg);

    // Circle
    const circleImg = document.createElement("IMG");
    circleImg.setAttribute('src', './images/icon-circle.png');
    circleImg.setAttribute('data-html2canvas-ignore', 'true');
    circleImg.className = "circle-control map-custom-control";
    circleImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.CIRCLE);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(circleImg);

    // Rectangle
    const rectangleImg = document.createElement("IMG");
    rectangleImg.setAttribute('src', './images/icon-rectangle.png');
    rectangleImg.setAttribute('data-html2canvas-ignore', 'true');
    rectangleImg.className = "rectangle-control map-custom-control";
    rectangleImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.RECTANGLE);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(rectangleImg);

    // Stop Drawing
    const pointerImg = document.createElement("IMG");
    pointerImg.setAttribute('src', './images/icon-mouse-pointer.png');
    pointerImg.setAttribute('data-html2canvas-ignore', 'true');
    pointerImg.className = "mouse-pointer-control map-custom-control";
    pointerImg.addEventListener("click", (() => {
        return e => {
            drawingManager.setDrawingMode(google.maps.drawing.OverlayType.FALSE);
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(pointerImg);

    // Remove Shape
    const crossImg = document.createElement("IMG");
    crossImg.setAttribute('src', './images/icon-cross.png');
    crossImg.setAttribute('data-html2canvas-ignore', 'true');
    crossImg.setAttribute('id', 'delete-button');
    crossImg.className = "cross-control map-custom-control";
    crossImg.addEventListener("click", (() => {
        return e => {
            deleteSelectedShape();
        };
    })());
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(crossImg);

    // Zoom In
    const zoomIn = document.createElement("IMG");
    zoomIn.setAttribute('src', './images/icon-dot-sselection.png');
    zoomIn.className = "zoom-in-control map-custom-control";
    zoomIn.addEventListener("click", (() => {
        return e => {
            map.setZoom(map.getZoom() + 1);
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(zoomIn);

    // Zoom Out
    const zoomOut = document.createElement("IMG");
    zoomOut.setAttribute('src', './images/icon-zoomout.png');
    zoomOut.className = "zoom-out-control map-custom-control";
    zoomOut.addEventListener("click", (() => {
        return e => {
            map.setZoom(map.getZoom() - 1);
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(zoomOut);

    // Full Zoom
    const fullZoomIn = document.createElement("IMG");
    fullZoomIn.setAttribute('src', './images/icon-all-dots.png');
    fullZoomIn.className = "full-zoom-in-control map-custom-control";
    fullZoomIn.addEventListener("click", (() => {
        return e => {
            map.setZoom(5);
            // if (window.innerWidth <= 1280) {
            //     map.setZoom(5);
            // }
        };
    })());
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(fullZoomIn);
    // ***** End Custom Controls ***** //

    // Set On Selected Shap
    map.addListener('zoom_changed', function() {
        if (selectedShape) {
            if (selectedShape.type == 'polygon') {
                var bounds = new google.maps.LatLngBounds();
                selectedShape.getPath().forEach(function(path, index) {
                    bounds.extend(path);
                });
                map.setCenter(bounds.getCenter());
            } else if (selectedShape.type == 'circle') {
                map.setCenter(selectedShape.getBounds().getCenter());
            } else if (selectedShape.type == 'rectangle') {
                map.setCenter(selectedShape.getBounds().getCenter());
            }
        }
    });
}

// Get Rectangle Info
function rectangleInfo(event) {
    infowindowId += 1;
    let infoWindowRec;
    var rectangle = event.overlay;
    var bounds = rectangle.getBounds();
    var ne = rectangle.getBounds().getNorthEast();
    var sw = rectangle.getBounds().getSouthWest();
    var nw = new google.maps.LatLng(ne.lat(), sw.lng());

    let swLat = sw.lat();
    let swLng = sw.lng();
    let neLat = ne.lat();
    let neLng = ne.lng();
    recSW = { swLat, swLng };
    recNE = { neLat, neLng };

    var se = new google.maps.LatLng(sw.lat(), ne.lng());
    var p1 = {
        lat: ne.lat(),
        lng: ne.lng()
    };
    var p2 = {
        lat: sw.lat(),
        lng: sw.lng()
    };
    var p3 = {
        lat: nw.lat(),
        lng: nw.lng()
    };
    var p4 = {
        lat: se.lat(),
        lng: se.lng()
    };
    var widthVal = Math.round(getDistance(p1, p3));
    var lengthVal = Math.round(getDistance(p1, p4));
    var recArea = Math.round(lengthVal * widthVal);
    rectabgleArea = recArea;
    rectangleWidth = widthVal;
    rectangleHeight = lengthVal;

    var contentString = '<div class="custom-info-window rectangle-infowindow text-center" style="color:' + rectangleInfowindowTextColor + '";>' +
        '<p>Zone: Zone1</p>' +
        '<p>Delivery Day: Tuesday</p>' +
        '<p>$0</p>' +
        '<p>Km: 234 km2</p>' +
        '<p>Count: 34</p>' +
        '</div>';
    var textCenter = rectangle.getBounds().getCenter();

    if (infoWindowRec) {
        infoWindowRec.close();
    }

    infoWindowRec = new google.maps.InfoWindow();
    infoWindowRec.setPosition(textCenter);
    infoWindowRec.setContent(contentString);
    infoWindowRec.setOptions({
        pixelOffset: new google.maps.Size(0, 50)
    });

    if (statistics == true) {
        infoWindowRec.open(map, rectangle);
    } else {
        infoWindowRec.open(map, rectangle);
        setTimeout(function() {
            $(".custom-info-window").hide();
        }, 10);
    }

    shapeCount += 1;
    let shapeNumber = shapeCount;
    allInfowindows.push({ 'infowindow': infoWindowRec, 'id': infowindowId });

    setTimeout(() => {
        $(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
    }, 30);

    google.maps.event.addListener(rectangle, 'drag', function() {
        infoWindowRec.close(map, rectangle);
    });
    rectangle.addListener('bounds_changed', function() {
        let ne = rectangle.getBounds().getNorthEast();
        let sw = rectangle.getBounds().getSouthWest();
        let nw = new google.maps.LatLng(ne.lat(), sw.lng());
        let p1 = { lat: ne.lat(), lng: ne.lng() };
        let p3 = { lat: nw.lat(), lng: nw.lng() };
        let p4 = { lat: se.lat(), lng: se.lng() };
        $.each(allShapesData.zones, function(key, val) {
            if (val.id == shapeNumber) {
                val.map_details.northEastCoordinate = recNE;
                val.map_details.southWestCoordinate = recSW;
                val.map_details.width = Math.round(getDistance(p1, p3));
                val.map_details.height = Math.round(getDistance(p1, p4));
                val.map_details.area = Math.round((Math.round(getDistance(p1, p4))) * (Math.round(getDistance(p1, p3))));
            }
        });
    });
}

// Get Circle Info
function circleInfo(event) {
    let infoWindowCir;
    var circle = event.overlay;
    let rd = circle.getRadius() / 1000; // In KM
    let cirRadius = Math.round(rd);
    let cirArea = Math.round(Math.PI * rd * rd);
    circleArea = cirArea;

    var textCenter = circle.getBounds().getCenter();
    var contentString = '<div class="custom-info-window circle-infowindow text-center" style="color:' + circleInfowindowTextColor + '";>' +
        '<p>Zone: Zone1</p>' +
        '<p>Delivery Day: Tuesday</p>' +
        '<p>$0</p>' +
        '<p>Km: 234 km2</p>' +
        '<p>Count: 34</p>' +
        '</div>';

    if (infoWindowCir) {
        infoWindowCir.close();
    }

    shapeCount += 1;
    let shapeNumber = shapeCount;

    infoWindowCir = new google.maps.InfoWindow();
    infoWindowCir.setPosition(textCenter);
    infoWindowCir.setContent(contentString);
    infoWindowCir.setOptions({
        pixelOffset: new google.maps.Size(0, 50)
    });

    if (statistics == true) {
        infoWindowCir.open(map, circle);
    } else {
        infoWindowCir.open(map, circle);
        setTimeout(function() {
            $(".custom-info-window").hide();
        }, 10);
    }

    setTimeout(() => {
        $(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
    }, 30);

    google.maps.event.addListener(circle, 'drag', function() {
        infoWindowCir.close(map, circle);
    });

    circle.addListener('bounds_changed', function() {
        $.each(allShapesData.zones, function(key, val) {
            if (val.id == shapeNumber) {
                val.map_details.center_lat_lng = { "lat": circle.getCenter().lat(), "lng": circle.getCenter().lng() };
                val.map_details.radius = circle.getRadius() / 1000;
                val.map_details.bg_color = circle.fillColor;
                val.map_details.area = Math.round(Math.PI * (circle.getRadius() / 1000) * (circle.getRadius() / 1000));
            }
        });
    });
}

// Get Polygon Info
function polygonInfo(event) {
    let infoWindowPol;
    var polygon = event.overlay;
    var area = google.maps.geometry.spherical.computeArea(polygon.getPath());
    let polyArea = area / (1000 * 1000); // In KM
    polygonArea = polyArea;
    // area = Math.round(area);
    var bounds = new google.maps.LatLngBounds();
    polygon.getPath().forEach(function(element, index) {
        bounds.extend(element)
    });
    var textCenter = bounds.getCenter();
    var contentString = '<div class="custom-info-window polygon-infowindow text-center" style="color:' + polygonInfowindowTextColor + '";>' +
        '<p>Zone: Zone1</p>' +
        '<p>Delivery Day: Tuesday</p>' +
        '<p>$0</p>' +
        '<p>Km: 234 km2</p>' +
        '<p>Count: 34</p>' +
        '</div>';

    if (infoWindowPol) {
        infoWindowPol.close();
    }

    shapeCount += 1;
    let shapeNumber = shapeCount;

    infoWindowPol = new google.maps.InfoWindow();
    infoWindowPol.setPosition(textCenter);
    infoWindowPol.setContent(contentString);
    infoWindowPol.setOptions({
        pixelOffset: new google.maps.Size(0, 50)
    });

    if (statistics == true) {
        infoWindowPol.open(map, polygon);
    } else {
        infoWindowPol.open(map, polygon);
        setTimeout(function() {
            $(".custom-info-window").hide();
        }, 10);
    }

    setTimeout(() => {
        $(".custom-info-window").closest('.gm-style-iw.gm-style-iw-c').css('width', 'auto');
    }, 30);

    google.maps.event.addListener(polygon, 'drag', function() {
        infoWindowPol.close(map, polygon);
    });

    polygon.getPaths().forEach(function(path, index) {
        google.maps.event.addListener(path, 'insert_at', function() {
            editOrDragPolygon();
        });

        google.maps.event.addListener(path, 'set_at', function() {
            editOrDragPolygon();
        });
    });

    function editOrDragPolygon() {
        let polyCoord = polygon.getPath().getArray();
        $.each(allShapesData.zones, function(key, val) {
            if (val.id == shapeNumber) {
                val.map_details.coordinates = [];
                $.each(polyCoord, function(key, value) {
                    val.map_details.coordinates.push({ "lat": value.lat(), "lng": value.lng() });
                });
                val.map_details.area = google.maps.geometry.spherical.computeArea(polygon.getPath()) / (1000 * 1000);
            }
        });
        // console.log(allShapesData.zones);
    }
}

// Clear Selections
function clearSelection() {
    if (selectedShape) {
        if (selectedShape.type !== 'marker') {
            selectedShape.setEditable(false);
        }
        selectedShape = null;
    }
}

function setSelection(shape) {
    if (shape.type !== 'marker') {
        clearSelection();
        shape.setEditable(true);
    }

    selectedShape = shape;

}

// Remove Selected Shapes Using Delete And Backspace Key
function deleteSelectedShapeUsingBtn(e) {
    if (e.keyCode == 8 || e.keyCode == 46) {
        deleteSelectedShape();
    }
}

// Remove Selected Shapes Using Cross Btn
function deleteSelectedShape() {
    if (selectedShape) {
        selectedShape.setMap(null);
    } else {
        alert('Please select atleast one shape to remove.');
    }

    // if (selectedShape.type == 'rectangle') {
    //     console.log(selectedShape);
    // }
}


var rad = function(x) {
    return x * Math.PI / 180;
};

var getDistance = function(p1, p2) {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2.lat - p1.lat);
    var dLong = rad(p2.lng - p1.lng);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    //return d; // returns radius distance in meter
    return d / 1000; // returns radius distance in kilometer
};

function blobToFile(theBlob, fileName) {
    let date = new Date();
    theBlob.lastModifiedDate = date;
    theBlob.lastModified = date.getTime();
    theBlob.name = fileName;
    theBlob.webkitRelativePath = "";
    return theBlob;
}

// Change Background Color Of All Shapes According To Selected Title
function changeBgColorOfShapes(color, shapeName) {
    if (shapeName == 'rectangle') {
        var rectangleOptions = drawingManager.get('rectangleOptions');
        rectangleOptions.fillColor = color;
        rectangleOptions.strokeColor = color;
        drawingManager.set('rectangleOptions', rectangleOptions);

        for (var i = 0; i < allRectangles.length; i++) {
            allRectangles[i].overlay.set('strokeColor', color);
            allRectangles[i].overlay.set('fillColor', color);
        }
    } else if (shapeName == 'circle') {
        var circleOptions = drawingManager.get('circleOptions');
        circleOptions.fillColor = color;
        circleOptions.strokeColor = color;
        drawingManager.set('circleOptions', circleOptions);

        for (var i = 0; i < allCircles.length; i++) {
            allCircles[i].overlay.set('strokeColor', color);
            allCircles[i].overlay.set('fillColor', color);
        }
    } else if (shapeName == 'polygon') {
        var polygonOptions = drawingManager.get('polygonOptions');
        polygonOptions.fillColor = color;
        polygonOptions.strokeColor = color;
        drawingManager.set('polygonOptions', polygonOptions);

        for (var i = 0; i < allPolygons.length; i++) {
            allPolygons[i].overlay.set('strokeColor', color);
            allPolygons[i].overlay.set('fillColor', color);
        }
    }
}

// Change Infowindow Text Color
function changeInfowindowTextColor(colorName, shapeName) {
    if (shapeName == 'rectangle') {
        rectangleInfowindowTextColor = colorName;
        // allShapesData.rectangles.textColor = colorName;
    } else if (shapeName == 'circle') {
        circleInfowindowTextColor = colorName;
        // allShapesData.circles.textColor = colorName;
    } else if (shapeName == 'polygon') {
        polygonInfowindowTextColor = colorName;
        // allShapesData.polygons.textColor = colorName;
    }
}

// Active or Inactive Statistics
function activeOrInactiveStatistics(param) {
    statistics = param;
}