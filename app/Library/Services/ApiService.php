<?php
namespace App\Library\Services;
use Session;
use App\User;
use App\DeliveryData;
use App\MapData;
use App\ZoneData;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;

use Google\Cloud\Storage\StorageClient;

class ApiService
{

	var $zones_login;
	var $zones_storage_list;
	var $order_view_select;
	
    public function __construct() {
				
		if(App::environment('DEV')) {			
			$this->zones_login = 'https://us-central1-arete-arms4-zones-dev.cloudfunctions.net/zones_login';
			$this->zones_storage_list = 'https://us-central1-arete-arms4-zones-dev.cloudfunctions.net/zones_storage_list';
			$this->order_view_select = 'https://us-central1-arete-arms4-dev-270903.cloudfunctions.net/order_view_select';	
			
		}elseif(App::environment('BETA')) {
			$this->zones_login = 'https://us-central1-arete-arms4-zones-beta.cloudfunctions.net/zones_login';
			$this->zones_storage_list = 'https://us-central1-arete-arms4-zones-beta.cloudfunctions.net/zones_storage_list';
			$this->order_view_select = 'https://us-central1-arete-arms4-beta-272818.cloudfunctions.net/order_view_select';
			
		}elseif(App::environment('PROD')) {
			$this->zones_login = 'https://us-central1-arete-arms4-zones-prod.cloudfunctions.net/zones_login';
			$this->zones_storage_list = 'https://us-central1-arete-arms4-zones-prod.cloudfunctions.net/zones_storage_list';
			$this->order_view_select = 'https://us-central1-arete-arms5-prod.cloudfunctions.net/order_view_select';		
		}
    }
	
	public function loginApi($request){
		$user_email = $request['user_email'];
		$user_name = $request['user_name'];
		$body = '{"user_mail": "'.$user_email.'","user_name": "'.$user_name.'"}';		
		$url = $this->zones_login;
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>$body,
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Content-Type: text/plain"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($response);
		return $result;
		
	}
	
	public function getFileListAPI($request){
		$folder = $request['folder'];
		$command = 'list';
		$company_warehouse = $request['company'].'-'.$request['warehouse'];
		$user_email = $request['user_email'];
		$body = '{"user_mail": "'.$user_email.'","company_warehouse": "'.$company_warehouse.'","folder": "'.$folder.'","command": "'.$command.'"}';
		$url = $this->zones_storage_list;
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>$body,
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Content-Type: text/plain"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($response);
		return $result;
		
	}	
	
	public function getDatafromJson($url){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		$result=curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result, true);	
		
		return $response;
	}
	
	public function order_select($request){

		$company = $request['company'];
		$warehouse = $request['warehouse'];
		$user_email = $request['user_email'];
		$from_date = $request['from_date'];
		$to_date = $request['to_date'];
		$uuid = $request['uuid'];
		$today_date = $request['today_date'];
	
	
		$body = '{"uuid":"'.$uuid.'","date":"'.$today_date.'","company":"'.$company.'","warehouse":"'.$warehouse.'","user_mail":"'.$user_email.'","fields":["order_invoice","order_visit_TR1","order_alias","order_volume","order_weight","order_cost_goods","order_pieces","order_latitude_TR1","order_longitud_TR1","order_delivery_date_TR1","order_id","order_trip_id","order_visit_id","trip_tot_cost_trip","trip_total_km","trip_volume_occupation_vehicle","trip_weight_occupation_vehicle","visit_destination_name"],"where":"trip_company_name = \''.$company.'\' AND trip_warehouse_takeoff_name = \''.$warehouse.'\' AND order_delivery_date_TR1 >= \''.$from_date.'\' AND order_delivery_date_TR1 <= \''.$to_date.'\'","sortBy":{"order_id":"ASC"},"groupBy":[]}';
		
		$url = $this->order_view_select;
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>$body,
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Content-Type: text/plain"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($response);

		return $result;
	}
	
	function uploadFile($bucketName, $fileContent, $cloudPath) {
		
		$url = asset('gcs_key.json');
		
		$privateKeyFileContent = file_get_contents($url);
		
	   // $privateKeyFileContent = $GLOBALS['privateKeyFileContent'];
		// connect to Google Cloud Storage using private key as authentication
		try {
			$storage = new StorageClient([
				'keyFile' => json_decode($privateKeyFileContent, true)
			]);
		} catch (Exception $e) {
			// maybe invalid private key ?
			print $e;
			return false;
		}

		// set which bucket to work in
		$bucket = $storage->bucket($bucketName);

		// upload/replace file 
		$storageObject = $bucket->upload(
				$fileContent,
				['name' => $cloudPath]
				// if $cloudPath is existed then will be overwrite without confirmation
				// NOTE: 
				// a. do not put prefix '/', '/' is a separate folder name  !!
				// b. private key MUST have 'storage.objects.delete' permission if want to replace file !
		);

		// is it succeed ?
		return $storageObject != null;
	}
	
	public function flushOldData($user_id){
		DeliveryData::where('user_id', $user_id)->delete();
		MapData::where('user_id', $user_id)->delete();
		ZoneData::where('user_id', $user_id)->delete();
	}
	
	public function flushDeliveryData($user_id){
		DeliveryData::where('user_id', $user_id)->delete();
	}	
	
	public function flushMapData($user_id){
		MapData::where('user_id', $user_id)->delete();
	}	
	
	public function flushZoneData($user_id){
		ZoneData::where('user_id', $user_id)->delete();
	}		
	
	public function getDeliveryData($user_id){
		$data = array();
		$rows = array();
		$comments = '';
		$commentsArr = array();
		$results = DeliveryData::where('user_id',$user_id)->get();
		if($results && $results->count() > 0){
			
			foreach($results as $row){
				
				if($row->delivery_row_json){
					$rowArr = json_decode($row->delivery_row_json,true);
					
					if($rowArr){
						array_push($rows,$rowArr);
					}
					
					if($row->comments && $row->comments != ''){
						$commentsArr[] = $row->comments;
					}
				}
			}
			
			if($commentsArr){
				$commentsArr = array_values(array_unique($commentsArr));
				$comments = implode(', ',$commentsArr);
			}
			
			$data['data'] = $rows;
			$data['comments'] = $comments;
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function getMapData($user_id){
		$data = array();
		$rows = array();
		$comments = '';
		$commentsArr = array();
		$results = MapData::where('user_id',$user_id)->get();
		
		if($results && $results->count() > 0){
			$i=0;
			foreach($results as $row){
				
				if($row->zone_row_json){
					$rowArr = json_decode($row->zone_row_json,true);
					
					if($rowArr){
						$rows[$i] = $rowArr;
					}
					
					if($row->comments && $row->comments != ''){
						$commentsArr[] = $row->comments;
					}
					
					$i++;
				}
			}
			
			if($commentsArr){
				$commentsArr = array_values(array_unique($commentsArr));
				$comments = implode(', ',$commentsArr);
			}
			
			$data['data'] = $rows;
			$data['comments'] = $comments;
			return $data;
		}else{
			return false;
		}		
	}	
	
	public function getZoneData($user_id){
		$data = array();
		$zones = array();
		$no_zone = array();
		$results = ZoneData::where('user_id',$user_id)->get();
		if($results && $results->count() > 0){
			
			foreach($results as $row){
				
				if($row->is_zone == 1){
					
					if($row->del_row_json){
						$rowArr = json_decode($row->del_row_json,true);
						
						if($rowArr){
							$zones[$row->zone_id][] = $rowArr;
						}
					}
				}else{
					
					if($row->del_row_json){
						$rowArr2 = json_decode($row->del_row_json,true);
						
						if($rowArr2){
							array_push($no_zone,$rowArr2);
						}
					}					
				}
				
			}
			$data['zones'] = $zones;
			$data['no_zone'] = $no_zone;
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function addDeliveryData($user_id,$delivery_data,$warehouse){
		
		$row_data = $delivery_data['data'];
		$comments = $delivery_data['comments'];
		
		if($row_data){
			$bulk_data  = array();
			foreach($row_data as $row){
				$delivery_row_json = json_encode($row,true);
				if($delivery_row_json){
					
					$input_data = array(
						'user_id' => $user_id,
						'order_id' => $row['order_id'],
						'delivery_row_json' => $delivery_row_json,
						'comments' => $comments,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
						'warehouse' => $warehouse,
						'visit_destination_name' => $row['visit_destination_name'],
					);
					$bulk_data[] = $input_data;
					
				}
			}
			
			if(count($bulk_data) > 120){
				$new_bulk_data = array_chunk($bulk_data,120);
				
				if($new_bulk_data){
					for($i=0; $i<count($new_bulk_data); $i++){
						DeliveryData::insert($new_bulk_data[$i]);
					}
				}
			}else{
				DeliveryData::insert($bulk_data);
			}
			
		}
	}

	public function addMapData($user_id,$map_data){
		$row_data = $map_data['data'];
		$comments = $map_data['comments'];
		
		if($row_data){
			$bulk_data = array();
			foreach($row_data as $row){
 				$zone_row_json = json_encode($row,true);
				
 				if($zone_row_json){
 					$input_data = array(
						'user_id' => $user_id,
						'zone_id' => $row['id'],
						'zone_row_json' => $zone_row_json,
						'comments' => $comments,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),						
					); 
					$bulk_data[] = $input_data;
				}  
			}
			if(count($bulk_data) > 150){
				$new_bulk_data = array_chunk($bulk_data,150);
				
				if($new_bulk_data){
					for($i=0; $i<count($new_bulk_data); $i++){
						MapData::insert($new_bulk_data[$i]);
					}
				}
			}else{
				MapData::insert($bulk_data);
			}
		}
	}	
	
	public function addZoneData($user_id,$zone_data){
		
		$zones = $zone_data['zones'];
		$no_zone = $zone_data['no_zone'];
		
		if($zones){
			
			$bulk_data = array();
			foreach($zones as $id => $row){
				
				if($row){
					$destNameArr = array();
					foreach($row as $key => $value){
						$destNameArr[] = $value;					
					}
					
					if($destNameArr){
						$results = DeliveryData::where('user_id',$user_id)->whereIn('visit_destination_name', $destNameArr)->get();
					
						if($results && $results->count() > 0){
							foreach($results as $row2){
								$del_row_json = $row2->delivery_row_json;
								
								if($del_row_json){
									$input_data = array(
										'user_id' => $user_id,
										'zone_id' => $id,
										'del_row_json' => $del_row_json,
										'is_zone' => 1,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s'),
									);
									$bulk_data[] = $input_data;								
								}
							}
						}
					}
				}
			}
			
			
			if(count($bulk_data) > 100){
				$new_bulk_data = array_chunk($bulk_data,100);
				
				if($new_bulk_data){
					for($i=0; $i<count($new_bulk_data); $i++){
						ZoneData::insert($new_bulk_data[$i]);
					}
				}
			}else{
				ZoneData::insert($bulk_data);
			}			
		}
		
		if($no_zone){
			$bulk_data2 = array();
			$destNameArr = array();
			foreach($no_zone as $row){
				$destNameArr[] = $row;						
			}
			
			if($destNameArr){
				$results = DeliveryData::where('user_id',$user_id)->whereIn('visit_destination_name', $destNameArr)->get();
				
				if($results && $results->count() > 0){
					foreach($results as $row2){
						$del_row_json = $row2->delivery_row_json;
						
						if($del_row_json){
							$input_data = array(
								'user_id' => $user_id,
								'zone_id' => 0,
								'del_row_json' => $del_row_json,
								'is_zone' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s'),						
							);
							$bulk_data2[] = $input_data;
						}						
					}
				}
			}

			if(count($bulk_data2) > 100){
				$new_bulk_data2 = array_chunk($bulk_data2,100);
				
				if($new_bulk_data2){
					for($i=0; $i<count($new_bulk_data2); $i++){
						ZoneData::insert($new_bulk_data2[$i]);
					}
				}
			}else{
				ZoneData::insert($bulk_data2);
			}	
			
		}		
	}
	
	public function getDeliveryDataByWarehouse($user_id,$warehouse){
		
		if($warehouse == 'all'){
			$results = DeliveryData::select('visit_destination_name')->where('user_id',$user_id)->get();
		}elseif($warehouse == 'none'){
			$results = false;
		}elseif($warehouse == 'selected'){
			
			if(Session::get('lastWarehouseSelected')){
				$warehouse = Session::get('lastWarehouseSelected');
				$results = DeliveryData::select('visit_destination_name')->where('user_id',$user_id)->where('warehouse',$warehouse)->get();				
			}else{
				$results = false;
			}
		}
				
		if($results && $results->count() > 0){
			return $results;
		}else{
			return false;
		}
	}
	
	public function removeComma($string){
		$b = str_replace( ',', '', $string );
		if(is_numeric($b)){ $string = $b; }		
		return $string;
	}
	
}


?>