<?php
namespace App\Library\Services;
use Session;
use App\User;
use App\DeliveryData;
use App\MapData;
use App\ZoneData;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;

use Google\Cloud\Storage\StorageClient;

class ApiService
{

	var $zones_login;
	var $zones_storage_list;
	var $order_view_select;
	
    public function __construct() {
				
		if(App::environment('DEV')) {			
			$this->zones_login = 'https://us-central1-arete-arms4-zones-dev.cloudfunctions.net/zones_login';
			$this->zones_storage_list = 'https://us-central1-arete-arms4-zones-dev.cloudfunctions.net/zones_storage_list';
			$this->order_view_select = 'https://us-central1-arete-arms4-dev-270903.cloudfunctions.net/order_view_select';	
			
		}elseif(App::environment('BETA')) {
			$this->zones_login = 'https://us-central1-arete-arms4-zones-beta.cloudfunctions.net/zones_login';
			$this->zones_storage_list = 'https://us-central1-arete-arms4-zones-beta.cloudfunctions.net/zones_storage_list';
			$this->order_view_select = 'https://us-central1-arete-arms4-beta-272818.cloudfunctions.net/order_view_select';
			
		}elseif(App::environment('PROD')) {
			$this->zones_login = 'https://us-central1-arete-arms4-zones-prod.cloudfunctions.net/zones_login';
			$this->zones_storage_list = 'https://us-central1-arete-arms4-zones-prod.cloudfunctions.net/zones_storage_list';
			$this->order_view_select = 'https://us-central1-arete-arms5-prod.cloudfunctions.net/order_view_select';		
		}
    }
	
	public function loginApi($request){
		$user_email = $request['user_email'];
		$user_name = $request['user_name'];
		$body = '{"user_mail": "'.$user_email.'","user_name": "'.$user_name.'"}';		
		$url = $this->zones_login;
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>$body,
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Content-Type: text/plain"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($response);
		return $result;
		
	}
	
	public function getFileListAPI($request){
		$folder = $request['folder'];
		$command = 'list';
		$company_warehouse = $request['company'].'-'.$request['warehouse'];
		$user_email = $request['user_email'];
		$body = '{"user_mail": "'.$user_email.'","company_warehouse": "'.$company_warehouse.'","folder": "'.$folder.'","command": "'.$command.'"}';
		$url = $this->zones_storage_list;
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>$body,
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Content-Type: text/plain"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($response);
		return $result;
		
	}	
	
	public function getDatafromJson($url){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		$result=curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result, true);	
		
		return $response;
	}
	
	public function order_select($request){

		$company = $request['company'];
		$warehouse = $request['warehouse'];
		$user_email = $request['user_email'];
		$from_date = $request['from_date'];
		$to_date = $request['to_date'];
		$uuid = $request['uuid'];
		$today_date = $request['today_date'];
	
	
		$body = '{"uuid":"'.$uuid.'","date":"'.$today_date.'","company":"'.$company.'","warehouse":"'.$warehouse.'","user_mail":"'.$user_email.'","fields":["order_invoice","order_visit_TR1","order_alias","order_volume","order_weight","order_cost_goods","order_pieces","order_latitude_TR1","order_longitud_TR1","order_delivery_date_TR1","order_id","order_trip_id","order_visit_id","trip_tot_cost_trip","trip_total_km","trip_volume_occupation_vehicle","trip_weight_occupation_vehicle","visit_destination_name"],"where":"trip_company_name = \''.$company.'\' AND trip_warehouse_takeoff_name = \''.$warehouse.'\' AND order_delivery_date_TR1 >= \''.$from_date.'\' AND order_delivery_date_TR1 <= \''.$to_date.'\'","sortBy":{"order_id":"ASC"},"groupBy":[]}';
		
		$url = $this->order_view_select;
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>$body,
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Content-Type: text/plain"
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($response);

		return $result;
	}
	
	function uploadFile($bucketName, $fileContent, $cloudPath) {
	
		$privateKeyFileContent = '{
			"type": "service_account",
			"project_id": "arete-arms4-zones-dev",
			"private_key_id": "e4dd7ca7d9a3b4df3ae509d9dd502476a205351b",
			"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC34S8ccX9SMOFA\nUpbZtvxqFKfqD91fXuYkIbArvNC8ldAkVdgNnIdZ/4P3oRw+Ti8HEXaG9FYzOkOK\ntdG8HoFD6HjWPrAos0WbbMOsNJPa+rTR7X87inIoqCeVsUOrA4pzBPaeqNVAmSdF\nVRlfSJI31Ndr7VDQLYoVUegcQ8w8BJZTfWeyQWqX0rSNmymJR7tBJ7Inw2TWr9tF\n9n7UNdGDdOmg5p9Tb6t/s3S+NHmBO2HwELemn29vQdgPjtijrY10DrLjqwsP/7pY\nivegEouG8uy8TxNsQvZqM679dmUUop/mxzlApuNmGm9bU8kY0V1KM4JpuTj9h2wr\nl1HQY531AgMBAAECggEAA4a+hAxLKvTZWltFyFQW4CPtmnKxG03zGkNJISBEXTF+\n+zYS6CPrK+yCB+LflfRNjj76p8OSJEuxlZHqEpz/0Uu+htakmIs47QOiIbB6cMB5\nF5jh5URA7zWrrgERxqWmkeZzolROOVMWlTZOFRfhB1ATd4mRZgSm5Boc2kSUlfFf\njDKayySQLR+njh8wlswS/NFhXtTOQhx9F7t9JFjBCVQ8xBlHrJlSPmNwctpstPwJ\nssh7RU2Adyk3+YjttHJWumsz9kQzN0O4uUCooTaCfkFtEMTZySgMbsyPk6iqHAf2\nareYn/a0vRKu537Gq/fwFqYmo3dUdQGZHPs52uWSywKBgQDv40sIYk4EM0yR9+ED\nmlVkr2YNeLe+XyV9rqrR1N/GVIQkLpoPUEmBJokU3gtHWwc1Eq9PEJ77V9u6WqMl\nM9l5/oDyARyQdk3dtH53G97G31yXUE0lw20f6HrFm39o7X9UcnEe38yxsliMs9uN\nvBrNwX+etnizyuWeiCGR8IM85wKBgQDEOt4mVN5SQe6Fn0AhhdhYFcWtvRkTSVI+\nCt8AK0BxWWMn5PcYKt5ukxO7CJ5WWyV4+mcz4AUve9jDjsh7cs6TJvHbEJ/l0Ih7\nYsHf+D29pCzK6MhXgdRCFZDpnAjAvQlV1vxgmqHEBy7UDeMiH4RXIQueqRurL0s/\nAgDVM+C2wwKBgQCq1eOJhPiqZR4SlcTZG//URFUZIu830+qPJuTfqgny5B125vkw\nDq1KHHdjNP4zQtBPK49NbzjJJjiE7idhRxuJlYWe0svU1Wt+iBvIF0D/v/HgkH9g\ndVwxbQ6By8tgnkIfkaCdrCPKKIiHK08gEOXFwhdwn+I1qDj+PVQq6m2L4QKBgQC8\n0oSnz+yqrKTv0YDtgRTgFftFJnw1kc1gxCc3y2bUa5Y9clbO25l0P3Sx+iYapNRX\nKGUchsmWmo7IBuIl4CCleskp6kJi6xoZ2LsEYKRQkuVDT2JQyi5MdOIf2TSW+pA8\nGIZ8Eu/saJkIFmDkmf1HthWvU7y7KeQ3Oh9BCQ2H7wKBgQDgIrkThf1kJZ6DEULb\negvLDo5D1v1ybLsGTCgtwGu/F3+J+P+wm/qveuHJwJykxuYG/6+qeHF89JtnztP4\n4Y4WLlYOibu5XXE306/+QZay/UrmIgOpBU5qr0tQLXGbRZPDwAg0uQxhBURZ8Our\ndEPNdbner4QtgInejGXejofP1w==\n-----END PRIVATE KEY-----\n",
			"client_email": "zones-storage@arete-arms4-zones-dev.iam.gserviceaccount.com",
			"client_id": "111715224050383542620",
			"auth_uri": "https://accounts.google.com/o/oauth2/auth",
			"token_uri": "https://oauth2.googleapis.com/token",
			"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
			"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/zones-storage%40arete-arms4-zones-dev.iam.gserviceaccount.com"
		}';
		
	   // $privateKeyFileContent = $GLOBALS['privateKeyFileContent'];
		// connect to Google Cloud Storage using private key as authentication
		try {
			$storage = new StorageClient([
				'keyFile' => json_decode($privateKeyFileContent, true)
			]);
		} catch (Exception $e) {
			// maybe invalid private key ?
			print $e;
			return false;
		}

		// set which bucket to work in
		$bucket = $storage->bucket($bucketName);

		// upload/replace file 
		$storageObject = $bucket->upload(
				$fileContent,
				['name' => $cloudPath]
				// if $cloudPath is existed then will be overwrite without confirmation
				// NOTE: 
				// a. do not put prefix '/', '/' is a separate folder name  !!
				// b. private key MUST have 'storage.objects.delete' permission if want to replace file !
		);

		// is it succeed ?
		return $storageObject != null;
	}
	
	public function flushOldData($user_id){
		DeliveryData::where('user_id', $user_id)->delete();
		MapData::where('user_id', $user_id)->delete();
		ZoneData::where('user_id', $user_id)->delete();
	}
	
	public function flushDeliveryData($user_id){
		DeliveryData::where('user_id', $user_id)->delete();
	}	
	
	public function flushMapData($user_id){
		MapData::where('user_id', $user_id)->delete();
	}	
	
	public function flushZoneData($user_id){
		ZoneData::where('user_id', $user_id)->delete();
	}		
	
	public function getDeliveryData($user_id){
		$data = array();
		$rows = array();
		$comments = '';
		$commentsArr = array();
		$results = DeliveryData::where('user_id',$user_id)->get();
		if($results && $results->count() > 0){
			
			foreach($results as $row){
				
				if($row->delivery_row_json){
					$rowArr = json_decode($row->delivery_row_json,true);
					
					if($rowArr){
						array_push($rows,$rowArr);
					}
					
					if($row->comments && $row->comments != ''){
						$commentsArr[] = $row->comments;
					}
				}
			}
			
			if($commentsArr){
				$commentsArr = array_values(array_unique($commentsArr));
				$comments = implode(', ',$commentsArr);
			}
			
			$data['data'] = $rows;
			$data['comments'] = $comments;
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function getMapData($user_id){
		$data = array();
		$rows = array();
		$comments = '';
		$commentsArr = array();
		$results = MapData::where('user_id',$user_id)->get();
		
		if($results && $results->count() > 0){
			$i=0;
			foreach($results as $row){
				
				if($row->zone_row_json){
					$rowArr = json_decode($row->zone_row_json,true);
					
					if($rowArr){
						$rows[$i] = $rowArr;
					}
					
					if($row->comments && $row->comments != ''){
						$commentsArr[] = $row->comments;
					}
					
					$i++;
				}
			}
			
			if($commentsArr){
				$commentsArr = array_values(array_unique($commentsArr));
				$comments = implode(', ',$commentsArr);
			}
			
			$data['data'] = $rows;
			$data['comments'] = $comments;
			return $data;
		}else{
			return false;
		}		
	}	
	
	public function getZoneData($user_id){
		$data = array();
		$zones = array();
		$no_zone = array();
		$results = ZoneData::where('user_id',$user_id)->get();
		if($results && $results->count() > 0){
			
			foreach($results as $row){
				
				if($row->is_zone == 1){
					
					if($row->del_row_json){
						$rowArr = json_decode($row->del_row_json,true);
						
						if($rowArr){
							$zones[$row->zone_id][] = $rowArr;
						}
					}
				}else{
					
					if($row->del_row_json){
						$rowArr2 = json_decode($row->del_row_json,true);
						
						if($rowArr2){
							array_push($no_zone,$rowArr2);
						}
					}					
				}
				
			}
			$data['zones'] = $zones;
			$data['no_zone'] = $no_zone;
			return $data;
		}else{
			return false;
		}
		
	}
	
	public function addDeliveryData($user_id,$delivery_data,$warehouse){
		
		$row_data = $delivery_data['data'];
		$comments = $delivery_data['comments'];
		
		if($row_data){
			$bulk_data  = array();
			foreach($row_data as $row){
				$delivery_row_json = json_encode($row,true);
				if($delivery_row_json){
					
					$input_data = array(
						'user_id' => $user_id,
						'order_id' => $row['order_id'],
						'delivery_row_json' => $delivery_row_json,
						'comments' => $comments,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
						'warehouse' => $warehouse,
						'visit_destination_name' => $row['visit_destination_name'],
					);
					$bulk_data[] = $input_data;
					
				}
			}
			
			if(count($bulk_data) > 120){
				$new_bulk_data = array_chunk($bulk_data,120);
				
				if($new_bulk_data){
					for($i=0; $i<count($new_bulk_data); $i++){
						DeliveryData::insert($new_bulk_data[$i]);
					}
				}
			}else{
				DeliveryData::insert($bulk_data);
			}
			
		}
	}

	public function addMapData($user_id,$map_data){
		$row_data = $map_data['data'];
		$comments = $map_data['comments'];
		
		if($row_data){
			$bulk_data = array();
			foreach($row_data as $row){
 				$zone_row_json = json_encode($row,true);
				
 				if($zone_row_json){
 					$input_data = array(
						'user_id' => $user_id,
						'zone_id' => $row['id'],
						'zone_row_json' => $zone_row_json,
						'comments' => $comments,
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),						
					); 
					$bulk_data[] = $input_data;
				}  
			}
			if(count($bulk_data) > 150){
				$new_bulk_data = array_chunk($bulk_data,150);
				
				if($new_bulk_data){
					for($i=0; $i<count($new_bulk_data); $i++){
						MapData::insert($new_bulk_data[$i]);
					}
				}
			}else{
				MapData::insert($bulk_data);
			}
		}
	}	
	
	public function addZoneData($user_id,$zone_data){
		
		$zones = $zone_data['zones'];
		$no_zone = $zone_data['no_zone'];
		
		if($zones){
			
			$bulk_data = array();
			foreach($zones as $id => $row){
				
				if($row){
					$destNameArr = array();
					foreach($row as $key => $value){
						$destNameArr[] = $value;					
					}
					
					if($destNameArr){
						$results = DeliveryData::where('user_id',$user_id)->whereIn('visit_destination_name', $destNameArr)->get();
					
						if($results && $results->count() > 0){
							foreach($results as $row2){
								$del_row_json = $row2->delivery_row_json;
								
								if($del_row_json){
									$input_data = array(
										'user_id' => $user_id,
										'zone_id' => $id,
										'del_row_json' => $del_row_json,
										'is_zone' => 1,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s'),
									);
									$bulk_data[] = $input_data;								
								}
							}
						}
					}
				}
			}
			
			
			if(count($bulk_data) > 100){
				$new_bulk_data = array_chunk($bulk_data,100);
				
				if($new_bulk_data){
					for($i=0; $i<count($new_bulk_data); $i++){
						ZoneData::insert($new_bulk_data[$i]);
					}
				}
			}else{
				ZoneData::insert($bulk_data);
			}			
		}
		
		if($no_zone){
			$bulk_data2 = array();
			$destNameArr = array();
			foreach($no_zone as $row){
				$destNameArr[] = $row;						
			}
			
			if($destNameArr){
				$results = DeliveryData::where('user_id',$user_id)->whereIn('visit_destination_name', $destNameArr)->get();
				
				if($results && $results->count() > 0){
					foreach($results as $row2){
						$del_row_json = $row2->delivery_row_json;
						
						if($del_row_json){
							$input_data = array(
								'user_id' => $user_id,
								'zone_id' => 0,
								'del_row_json' => $del_row_json,
								'is_zone' => 0,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s'),						
							);
							$bulk_data2[] = $input_data;
						}						
					}
				}
			}

			if(count($bulk_data2) > 100){
				$new_bulk_data2 = array_chunk($bulk_data2,100);
				
				if($new_bulk_data2){
					for($i=0; $i<count($new_bulk_data2); $i++){
						ZoneData::insert($new_bulk_data2[$i]);
					}
				}
			}else{
				ZoneData::insert($bulk_data2);
			}	
			
		}		
	}
	
	public function getDeliveryDataByWarehouse($user_id,$warehouse){
		
		if($warehouse == 'all'){
			$results = DeliveryData::select('visit_destination_name')->where('user_id',$user_id)->get();
		}elseif($warehouse == 'none'){
			$results = false;
		}elseif($warehouse == 'selected'){
			
			if(Session::get('lastWarehouseSelected')){
				$warehouse = Session::get('lastWarehouseSelected');
				$results = DeliveryData::select('visit_destination_name')->where('user_id',$user_id)->where('warehouse',$warehouse)->get();				
			}else{
				$results = false;
			}
		}
				
		if($results && $results->count() > 0){
			return $results;
		}else{
			return false;
		}
	}
	
	public function removeComma($string){
		$b = str_replace( ',', '', $string );
		if(is_numeric($b)){ $string = $b; }		
		return $string;
	}
	
}


?>