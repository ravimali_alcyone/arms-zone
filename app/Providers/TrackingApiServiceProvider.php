<?php

namespace App\Providers;
  
use Illuminate\Support\ServiceProvider;
use App\Library\Services\ApiService;
  
class TrackingApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }
  	
    public function register()
    {
        $this->app->bind('App\Library\Services\ApiService', function ($app) {
          return new ApiService();
        });
    }	
}
