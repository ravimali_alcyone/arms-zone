<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Services\ApiService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;
use App;
use Redirect;
use App\Traits\UserTrait;
use Response;
use Auth;
use Hash;
use App\Exports\ZonesExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

class ZoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


	// Login2
    public function index()
    {
		if(Session::has('user')){
			$data['user'] = Session::get('user');
			return view('auth.login',array('data'=>$data));
		}else{
			return redirect('/login');
		}
    }

	// Zone
    public function zone(ApiService $api)
    {
		if(Session::has('user')){
			$data['user'] = Session::get('user');

			if(Session::has('default_options')){
				$data['default_options'] = Session::get('default_options');
			}else{
				$data['default_options'] = array();
			}

			$data['delivery_data'] = array();
			$data['deliveryTableData'] = '';
			$data['delivery_data_comments'] = '';
			$delResData = $this->getDeliveryDataFromLocalDB($api);

			if($delResData && $delResData['status'] == true){
				$data['delivery_data_comments'] = $delResData['data']['all']['comments'];
				$data['delivery_data'] = $delResData['data']['unique'];
			//	echo '<pre>';print_R($data['delivery_data']);die;
				$data['deliveryTableData'] = $delResData['data']['table'];
			}

			$data['model_data'] = array();
			$data['modelTableData'] = '';
			$data['model_data_comments'] = '';
			$data['zone_count'] = 0;
			$modelResData = $this->getMapDataFromLocalDB($api);

			if($modelResData && $modelResData['status'] == true){

				$data['model_data_comments'] = $modelResData['data']['all']['comments'];
				$data['model_data'] = $modelResData['data']['all']['data'];
				//echo '<pre>';print_r($data['model_data']);die;
				$data['modelTableData'] = $modelResData['data']['table'];
				$data['zone_count'] = $modelResData['data']['zone_count'];
			}

			return view('zone.zone',array('data'=>$data));
		}else{
			return redirect('/login');
		}
    }

	// Predict
    public function predict(){

		//echo '<pre>';print_R(Session::get('exportZoneData')); die;
		if(Session::has('user')){
			$data['user'] = Session::get('user');

			if(Session::has('default_options')){
				$data['default_options'] = Session::get('default_options');
			}else{
				$data['default_options'] = array();
			}

			return view('zone.predict',array('data'=>$data));
		}else{
			return redirect('/login');
		}
    }

	public function getDataFromAPI(Request $request, ApiService $api){
		$delivery_data = array();
		$req = $request->all();
		$user = Session::get('user');
		$type = $req['db_check_type'];//add or replace
		$data['user_email'] = $user_email = $user['email'];
		$data['user_id'] = $user_id = $user['user_id'];
		$data['company'] = $company = $req['company'];
		$data['warehouse'] = $warehouse = $req['warehouse'];
		Session::put('lastCompanySelected', $company);
		Session::put('lastWarehouseSelected', $warehouse);
		$from_date = date('Y-m-d',strtotime(trim($req['from_date'])));
		$to_date = date('Y-m-d',strtotime(trim($req['to_date'])));
		$data['from_date'] = $from_date.' 00:00';
		$data['to_date'] = $to_date.' 23:59';
		$data['uuid'] = 1111;
		$data['today_date'] = date('Y-m-d H:i');

		$result = $api->order_select($data);
		
		if($result){
			if($result->status == false){
				$delResData['status'] = false;
				$delResData['data'] = array();
				$delResData['error_type'] = 1; //user not authorize and other
				echo json_encode($delResData);
				exit;

			}
			elseif($result->error == '' && $result->result && $result->status == true){

				$response = $result->result;
				//change object nodes into array
				if($response){
					$response = json_decode(json_encode($response), true);
				}

				//echo '<pre>';print_r($response);die;
				$delResData['status'] = true;

				if($type == 'add'){

					//insert into DeliveryData
					$new_delivery_data['data'] = $response;
					$new_delivery_data['comments'] = '';
					$api->addDeliveryData($user_id,$new_delivery_data,$warehouse);

					//get all delivery data
					$delivery_data = $api->getDeliveryData($user_id);

				}elseif($type == 'replace'){

					//delete old DeliveryData
					$api->flushDeliveryData($user_id);

					//insert into DeliveryData
					$new_delivery_data['data'] = $response;
					$new_delivery_data['comments'] = '';
					$api->addDeliveryData($user_id,$new_delivery_data,$warehouse);

					//get all delivery data
					$delivery_data = $api->getDeliveryData($user_id);

				}

				$delivery_data_unique = array();

				if($delivery_data && $delivery_data['data']){
					$order_invoice_array = [];
					
					foreach($delivery_data['data'] as  $row){
						
						$destination = $row['visit_destination_name'];
											
						$order_invoice_array[$destination]['visit_destination_name'] = $destination;
						$order_invoice_array[$destination]['order_latitude_TR1'] = $row['order_latitude_TR1'];
						$order_invoice_array[$destination]['order_longitud_TR1'] = $row['order_longitud_TR1'];
						$order_invoice_array[$destination]['order_weight'][] = $row['order_weight'];
						$order_invoice_array[$destination]['order_volume'][] = $row['order_volume'];
						$order_invoice_array[$destination]['order_pieces'][] = $row['order_pieces'];;
						$order_invoice_array[$destination]['order_cost_goods'][] = $row['order_cost_goods'];
					}

					if($order_invoice_array){
						$i=0;
						foreach($order_invoice_array as $row){		
						
							$delivery_data_unique[$i]['visit_destination_name'] = $row['visit_destination_name'];						
							$delivery_data_unique[$i]['order_latitude_TR1'] = $row['order_latitude_TR1'];
							$delivery_data_unique[$i]['order_longitud_TR1'] = $row['order_longitud_TR1'];
							
							$order_weight_total = array_sum($row['order_weight']);
							$delivery_data_unique[$i]['order_weight'] = round($order_weight_total,2);
							
							$order_volume_total = array_sum($row['order_volume']);
							$delivery_data_unique[$i]['order_volume'] = round($order_volume_total,2);
							
							$order_pieces_total = array_sum($row['order_pieces']);
							$delivery_data_unique[$i]['order_pieces'] = $order_pieces_total;
							
							$order_cost_goods_total = array_sum($row['order_cost_goods']);
							$delivery_data_unique[$i]['order_cost_goods'] = round($order_cost_goods_total,2);						
							$i++;
						}
					}
				}

				$delResData['data']['all'] = $delivery_data;
				$delResData['data']['unique'] = $delivery_data_unique;
				$delResData['data']['table'] = $this->deliveryTableData($api);
				$delResData['error_type'] = 0;
				echo json_encode($delResData);

			}
			elseif($result->error == '' && !$result->result && $result->status == true){
				$delResData['status'] = false;
				$delResData['data'] = array();
				$delResData['error_type'] = 2; //date range error
				echo json_encode($delResData);
			}
		}else{
			$delResData['status'] = false;
			$delResData['data'] = array();
			$delResData['error_type'] = 2; //date type error
			echo json_encode($delResData);
			exit;			
		}
	}

	public function getDeliveryFiles(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$data['user_email'] = $user['email'];
		$data['company'] = $company = $req['company'];
		$data['warehouse'] = $warehouse = $req['warehouse'];
		Session::put('lastCompanySelected', $company);
		Session::put('lastWarehouseSelected', $warehouse);
		$data['folder'] = 'VISIT';
		$jsonNames = array();
		$result = $api->getFileListAPI($data);

		if($result && $result->status == 1){
			$res = $result->result;

			foreach($res as $row){
				if($row->name != ''){
					$jsonNames[] = $row->name;
				}
			}
			$delResData = array(
			'status' => true,
			'data' => $jsonNames
			);
			echo json_encode($delResData);
		}else{
			$delResData = array(
			'status' => false,
			'data' => array()
			);
			echo json_encode($delResData);
		}
	}

	public function getDataFromDeliveryFile(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$data['user_email'] = $user_email = $user['email'];
		$data['user_id'] = $user_id = $user['user_id'];
		$data['company'] = $req['company'];
		$data['warehouse'] = $warehouse = $req['warehouse'];
		$delivery_data = array();
		$type = $req['delivery_check_type'];
		$data['folder'] = $folder = 'VISIT';
		$jsonName = rawurlencode(trim($req['fileName']));
		$gcsBucket_url = 'https://storage.googleapis.com/arms4-zonificacion-dev';
		$cw_name = rawurlencode($data['company'].'-'.$data['warehouse']);
		$jsonURL = $gcsBucket_url.'/'.$cw_name.'/'.$folder.'/'.$jsonName;
		//echo $jsonURL;die;
		$response = $api->getDatafromJson($jsonURL);
		//echo '<pre>'; print_R($response);die;
		if($response){
			$delResData['status'] = true;

 			if($type == 'add'){

				//insert into DeliveryData
				$new_delivery_data = $response;
				$api->addDeliveryData($user_id,$new_delivery_data,$warehouse);

				//get all delivery data
				$delivery_data = $api->getDeliveryData($user_id);

			}elseif($type == 'replace'){
				$delivery_data = $response;

				//delete old DeliveryData
				$api->flushDeliveryData($user_id);

				//insert into DeliveryData
				$api->addDeliveryData($user_id,$delivery_data,$warehouse);

				//get all delivery data
				$delivery_data = $api->getDeliveryData($user_id);
			}

			$delivery_data_unique = array();

			if($delivery_data && $delivery_data['data']){
				$order_invoice_array = [];
				
				foreach($delivery_data['data'] as  $row){
					
					$destination = $row['visit_destination_name'];
					
					$order_invoice_array[$destination]['visit_destination_name'] = $destination;
					$order_invoice_array[$destination]['order_latitude_TR1'] = $row['order_latitude_TR1'];
					$order_invoice_array[$destination]['order_longitud_TR1'] = $row['order_longitud_TR1'];
 					$order_invoice_array[$destination]['visit_destination_name'] = $destination;
					$order_invoice_array[$destination]['order_latitude_TR1'] = $row['order_latitude_TR1'];
					$order_invoice_array[$destination]['order_longitud_TR1'] = $row['order_longitud_TR1'];
					$order_invoice_array[$destination]['order_weight'][] = $row['order_weight'];
					$order_invoice_array[$destination]['order_volume'][] = $row['order_volume'];
					$order_invoice_array[$destination]['order_pieces'][] = $row['order_pieces'];;
					$order_invoice_array[$destination]['order_cost_goods'][] = $row['order_cost_goods'];						
					
				}

				if($order_invoice_array){
					$i=0;
					foreach($order_invoice_array as $row){						
						$delivery_data_unique[$i]['visit_destination_name'] = $row['visit_destination_name'];						
						$delivery_data_unique[$i]['order_latitude_TR1'] = $row['order_latitude_TR1'];
						$delivery_data_unique[$i]['order_longitud_TR1'] = $row['order_longitud_TR1'];
						
						$order_weight_total = array_sum($row['order_weight']);
						$delivery_data_unique[$i]['order_weight'] = round($order_weight_total,2);
						
						$order_volume_total = array_sum($row['order_volume']);
						$delivery_data_unique[$i]['order_volume'] = round($order_volume_total,2);
						
						$order_pieces_total = array_sum($row['order_pieces']);
						$delivery_data_unique[$i]['order_pieces'] = $order_pieces_total;
						
						$order_cost_goods_total = array_sum($row['order_cost_goods']);
						$delivery_data_unique[$i]['order_cost_goods'] = round($order_cost_goods_total,2);					
						$i++;
					}
				}
			}

			$delResData['data']['all'] = $delivery_data;
			$delResData['data']['unique'] = $delivery_data_unique;
			$delResData['data']['table'] = $this->deliveryTableData($api);
		}else{
			$delResData['status'] = false;
			$delResData['data'] = false;
		}
		echo json_encode($delResData);
	}

	public function getDeliveryDataFromLocalDB($api){
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];
		$delivery_data = $api->getDeliveryData($user_id);
		$delivery_data_unique = array();

		if($delivery_data && $delivery_data['data']){
			$order_invoice_array = [];
			
			foreach($delivery_data['data'] as  $row){
				
				$destination = $row['visit_destination_name'];
				
				$order_invoice_array[$destination]['visit_destination_name'] = $destination;
				$order_invoice_array[$destination]['order_latitude_TR1'] = $row['order_latitude_TR1'];
				$order_invoice_array[$destination]['order_longitud_TR1'] = $row['order_longitud_TR1'];
				$order_invoice_array[$destination]['order_weight'][] = $row['order_weight'];
				$order_invoice_array[$destination]['order_volume'][] = $row['order_volume'];
				$order_invoice_array[$destination]['order_pieces'][] = $row['order_pieces'];;
				$order_invoice_array[$destination]['order_cost_goods'][] = $row['order_cost_goods'];										
				
			}

			if($order_invoice_array){
				$i=0;
				foreach($order_invoice_array as $row){						
					$delivery_data_unique[$i]['visit_destination_name'] = $row['visit_destination_name'];						
					$delivery_data_unique[$i]['order_latitude_TR1'] = $row['order_latitude_TR1'];
					$delivery_data_unique[$i]['order_longitud_TR1'] = $row['order_longitud_TR1'];

					$order_weight_total = array_sum($row['order_weight']);
					$delivery_data_unique[$i]['order_weight'] = round($order_weight_total,2);
					
					$order_volume_total = array_sum($row['order_volume']);
					$delivery_data_unique[$i]['order_volume'] = round($order_volume_total,2);
					
					$order_pieces_total = array_sum($row['order_pieces']);
					$delivery_data_unique[$i]['order_pieces'] = $order_pieces_total;
					
					$order_cost_goods_total = array_sum($row['order_cost_goods']);
					$delivery_data_unique[$i]['order_cost_goods'] = round($order_cost_goods_total,2);	
						
					$i++;
				}
			}
			$delResData['status'] = true;
			$delResData['data']['all'] = $delivery_data;
			$delResData['data']['unique'] = $delivery_data_unique;
			$delResData['data']['table'] = $this->deliveryTableData($api);

			return $delResData;

		}else{
			return false;
		}
	}

	public function getMapDataFromLocalDB($api){
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];
		$map_data = $api->getMapData($user_id);

		if($map_data){
			$mapResData['status'] = true;
			$mapResData['data']['all'] = $map_data;
			$mtdata = $this->modelTableData($api);

			$mapResData['data']['table'] = $mtdata['output'];
			$mapResData['data']['zone_count'] = $mtdata['zone_count'];
			return $mapResData;
		}else{
			return false;
		}
	}

	public function deliveryTableData($api){
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		$DeliveryData = $api->getDeliveryData($user_id);

		if($DeliveryData){

			$dData = $DeliveryData;
			$delivery_data = $dData['data'];

			$td_cost = 0;
			$trip_count = 0;
			$trip_total_km = 0;
			$order_pieces = 0;
			$order_cost_goods = 0;
			$order_weight = 0;
			$order_volume = 0;
			$total_per_volume_occupation = 0;
			$total_per_weight_occupation = 0;
			$total_deliveries = 0;
			$total_unique_destinations = 0;
			$tudArr = [];
			$tdcostArr = [];
			$ttKmArr = [];
			$tvovArr = [];
			$twovArr = [];
			$opArr = [];
			$ocgArr = [];
			$owArr = [];
			$ovArr = [];

			if($delivery_data){
				foreach($delivery_data as $row){
					$tdcostArr[$row['order_trip_id']] = $row['trip_tot_cost_trip'];
					$ttKmArr[$row['order_trip_id']] = $row['trip_total_km'];
					$tvovArr[$row['order_trip_id']] = $row['trip_volume_occupation_vehicle'];
					$twovArr[$row['order_trip_id']] = $row['trip_weight_occupation_vehicle'];
					$opArr[$row['order_invoice']] = $row['order_pieces'];
					$ocgArr[$row['order_invoice']] = $row['order_cost_goods'];
					$owArr[$row['order_invoice']] = $row['order_weight'];
					$ovArr[$row['order_invoice']] = $row['order_volume'];
					if(isset($row['visit_destination_name'])){
						$tudArr[] = $row['visit_destination_name'];
					}
					
				}
			}
		
			if($tdcostArr){
				foreach($tdcostArr as $key => $value){
					$td_cost += $value;
					$trip_count += 1;
				}
			}

			if($ttKmArr){
				foreach($ttKmArr as $key => $value){
					$trip_total_km += $value;
				}
			}

			if($tvovArr){
				foreach($tvovArr as $key => $value){
					$total_per_volume_occupation += $value;
				}
				if($total_per_volume_occupation != 0){
					$total_per_volume_occupation = $total_per_volume_occupation / count($tvovArr);
				}
			}

			if($opArr){
				foreach($opArr as $key => $value){
					$order_pieces += $value;
					$total_deliveries += 1;
				}
			}

			if($ocgArr){
				foreach($ocgArr as $key => $value){
					$order_cost_goods += $value;
				}
			}

			if($owArr){
				foreach($owArr as $key => $value){
					$order_weight += $value;
				}
			}

			if($ovArr){
				foreach($ovArr as $key => $value){
					$order_volume += $value;
				}
			}

			if($twovArr){
				foreach($twovArr as $key => $value){
					$total_per_weight_occupation += $value;
				}
				if($total_per_weight_occupation != 0){
					$total_per_weight_occupation = $total_per_weight_occupation / count($twovArr);
				}
			}

			if($tudArr){
				$total_unique_destinations = count(array_unique($tudArr));
			}

			$data = array();
			$data['total_deliveries'] = $total_deliveries;
			$data['total_unique_destinations'] = $total_unique_destinations;
			$data['total_deliveriey_cost'] = $td_cost;
			$data['total_trips'] = $trip_count;
			$data['total_kms'] = $trip_total_km;
			$data['total_pieces'] = $order_pieces;
			$data['total_goods'] = $order_cost_goods;
			$data['total_weight'] = $order_weight;
			$data['total_volume'] = $order_volume;
			$data['total_per_volume_occupation'] = $total_per_volume_occupation;
			$data['total_per_weight_occupation'] = $total_per_weight_occupation;

			$output = view('zone.delivery_table',array('data'=>$data))->render();
			return $output;
		}
	}

	public function saveDeliveryFile(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		$DeliveryData = $api->getDeliveryData($user_id);

		if($DeliveryData){
			$delivery_data = $DeliveryData;
		}else{
			$delivery_data = array();
			$delResData['status'] = false;
			echo json_encode($delResData);
			exit;
		}
		$company = $req['company'];
		$warehouse = $req['warehouse'];
		$folder = 'VISIT';
		$fileName = $req['fileName'];
		$comments = $req['comments'];
		$cw_name = $company.'-'.$warehouse;
		$targetDir = 'json_files/'; //local

		if($delivery_data){
			$newData['data'] = $delivery_data['data'];
			$newData['comments'] = $comments;
			$jsondata = json_encode($newData, JSON_PRETTY_PRINT);
		}else{
			$jsondata = '';
		}
		$myFile = $fileName.'.json';
		$filePath = $targetDir.'/'.$folder.'/'.urlencode($myFile);

		if(file_put_contents($filePath, $jsondata)) {

			$bucketName = env('GCS_BUCKET');

			if($fileContent = file_get_contents($filePath)){
				$cloudPath = $cw_name.'/'.$folder.'/'.$myFile;
				$isSucceed = $api->uploadFile($bucketName, $fileContent, $cloudPath);

				if ($isSucceed == true) {
					$delResData['status'] = true;
				}
			}else{
				$delResData['status'] = false;
			}

		}else{
			$delResData['status'] = false;
		}
		echo json_encode($delResData);
	}

	public function getModelFiles(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$data['user_email'] = $user['email'];
		$data['company'] = $req['company'];
		$data['warehouse'] = $req['warehouse'];
		$data['folder'] = 'ZONES';
		$jsonNames = array();
		$result = $api->getFileListAPI($data);

		if($result && $result->status == 1){
			$res = $result->result;

			foreach($res as $row){
				if($row->name != ''){
					$jsonNames[] = $row->name;
				}
			}
			$delResData = array(
			'status' => true,
			'data' => $jsonNames
			);
			echo json_encode($delResData);
		}else{
			$delResData = array(
			'status' => false,
			'data' => array()
			);
			echo json_encode($delResData);
		}
	}

	public function getDataFromModelFile(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$delivery_data = array();
		$data['user_email'] = $user_email = $user['email'];
		$data['user_id'] = $user_id = $user['user_id'];
		$data['company'] = $req['company'];
		$data['warehouse'] = $req['warehouse'];
		$type = $req['model_check_type'];
		$data['folder'] = $folder = 'ZONES';
		$jsonName = rawurlencode(trim($req['fileName']));
		$gcsBucket_url = 'https://storage.googleapis.com/arms4-zonificacion-dev';
		$cw_name = rawurlencode($data['company'].'-'.$data['warehouse']);
		$jsonURL = $gcsBucket_url.'/'.$cw_name.'/'.$folder.'/'.$jsonName;
		$response = $api->getDatafromJson($jsonURL);
		$model_data = array();

		if($response){
			$delResData['status'] = true;

			if($type == 'add'){

				//insert into MapData
				$new_model_data = $response;
				$api->addMapData($user_id,$new_model_data);

				//get all map data
				$model_data = $api->getMapData($user_id);

			}elseif($type == 'replace'){
				$model_data = $response;

				//delete old MapData
				$api->flushMapData($user_id);

				//insert into MapData
				$new_model_data = $response;
				$api->addMapData($user_id,$new_model_data);

				//get all map data
				$model_data = $api->getMapData($user_id);
			}

			$delResData['data']['all'] = $model_data;
			$mtdata = $this->modelTableData($api);
			$delResData['data']['table'] = $mtdata['output'];
			$delResData['data']['zone_count'] = $mtdata['zone_count'];
		}else{
			$delResData['status'] = false;
			$delResData['data'] = false;
		}
		echo json_encode($delResData);
	}

	public function modelTableData($api){
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		$ZoneData = $api->getZoneData($user_id);

		//echo '<pre>';print_R($ZoneData);die;

		if($ZoneData){
			$zone_data = $ZoneData;
		}else{
			$zone_data = array();
		}

		$myZoneArr = array();
		$modelData = array();
		if($zone_data){
			$i=0;
			// Zone wise Delivery Data
			if($zone_data['zones']){

				foreach($zone_data['zones'] as $key => $zone){
					array_push($myZoneArr,$key);
					if($key == 2){
						
					}
					if($key != 0){

						$mapRow = $this->getFromMapDataRow($key,$api);

						if($mapRow){
							$zoneName = $mapRow['name'];
							$comment1 = $mapRow['comment1'];
							$comment2 = $mapRow['comment2'];
							
							$delivery_count = 0;
							$order_pieces = 0;
							$order_cost_goods = 0;
							$order_weight = 0;
							$order_volume = 0;
							$dateArr = array();
							$day_name = array();
							
							foreach($zone as $row){
								$delivery_count = count($zone);
								$order_weight += $row['order_weight'];
								$order_volume += $row['order_volume'];
								$order_pieces += $row['order_pieces'];
								$order_cost_goods += $row['order_cost_goods'];

								if($row['order_delivery_date_TR1']){
									$dateArr[] = strtolower(date('l',strtotime($row['order_delivery_date_TR1'])));
								}
							}
							if($dateArr){
								$dateArr = array_values(array_unique($dateArr));
								//$day_name = implode(', ',$dateArr);
								$day_name = $dateArr;
							}

							$modelData[$i]['zone_name'] = $zoneName;
							$modelData[$i]['comment1'] = $comment1;
							$modelData[$i]['comment2'] = $comment2;
							$modelData[$i]['delivery_count'] = $delivery_count;
							$modelData[$i]['order_weight'] = $order_weight;
							$modelData[$i]['order_volume'] = $order_volume;
							$modelData[$i]['order_pieces'] = $order_pieces;
							$modelData[$i]['order_cost_goods'] = $order_cost_goods;
							$modelData[$i]['day_name'] = $day_name;
							$i++;
						}
					}
				}
			}

			// Empty Zone

			$MapData = $api->getMapData($user_id);

			if($MapData){
				$map_data = $MapData;
				if($map_data && $map_data['data']){
					$delivery_count = 0;
					$order_pieces = 0;
					$order_cost_goods = 0;
					$order_weight = 0;
					$order_volume = 0;
					$day_name = array();
					$dateArr = array();

					foreach($map_data['data'] as $row){
						if(!in_array($row['id'], $myZoneArr)){
							$modelData[$i]['zone_name'] = $row['name'];
							$modelData[$i]['comment1'] = $row['comment1'];
							$modelData[$i]['comment2'] = $row['comment2'];
							$modelData[$i]['delivery_count'] = 0;
							$modelData[$i]['order_weight'] = 0;
							$modelData[$i]['order_volume'] = 0;
							$modelData[$i]['order_pieces'] = 0;
							$modelData[$i]['order_cost_goods'] = 0;
							$modelData[$i]['day_name'] = array();
							$i++;
						}
					}
				}
			}

			// No Zone Delivery Data
			$j = $i+1;
			if($zone_data['no_zone']){
				$delivery_count = 0;
				$order_pieces = 0;
				$order_cost_goods = 0;
				$order_weight = 0;
				$order_volume = 0;
				$day_name = array();
				$dateArr = array();

				//echo '<Pre>';print_r($zone_data['no_zone']);die;
				foreach($zone_data['no_zone'] as $row){

					$delivery_count = count($zone_data['no_zone']);
					$order_weight += $row['order_weight'];
					$order_volume += $row['order_volume'];
					$order_pieces += $row['order_pieces'];
					$order_cost_goods += $row['order_cost_goods'];

					if($row['order_delivery_date_TR1']){
						$dateArr[] = strtolower(date('l',strtotime($row['order_delivery_date_TR1'])));
					}

					if($dateArr){
						$dateArr = array_values(array_unique($dateArr));
						//$day_name = implode(', ',$dateArr);
						$day_name = $dateArr;
					}

					$zoneName = __('messages.zone.no_zone');
					$comment1 = '';
					$comment2 = 0;

					$modelData[$j]['zone_name'] = $zoneName;
					$modelData[$j]['comment1'] = $comment1;
					$modelData[$j]['comment2'] = $comment2;
					$modelData[$j]['delivery_count'] = $delivery_count;
					$modelData[$j]['order_weight'] = $order_weight;
					$modelData[$j]['order_volume'] = $order_volume;
					$modelData[$j]['order_pieces'] = $order_pieces;
					$modelData[$j]['order_cost_goods'] = $order_cost_goods;
					$modelData[$j]['day_name'] = $day_name;
				}
			}

			//echo '<Pre>';print_R($modelData);die;
			$output = view('zone.model_table',array('modelData'=>$modelData))->render();
			if($zone_data['no_zone']){
				$res['zone_count'] = count($modelData) - 1;
			}else{
				$res['zone_count'] = count($modelData);
			}

			$res['output'] = $output;
			return $res;
		}
	}

	public function getFromMapDataRow($id,$api){
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		$MapData = $api->getMapData($user_id);

		if($MapData){
			$map_data = $MapData;
		}else{
			$map_data = array();
		}

		if($map_data && $map_data['data']){
			foreach($map_data['data'] as $row){
				if($row['id'] == $id){
					return $row;
				}
			}
		}
		return false;
	}

	public function saveModelFile(Request $request, ApiService $api){
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		$req = $request->all();
		$company = $req['company'];
		$warehouse = $req['warehouse'];
		$folder = 'ZONES';
		$fileName = $req['fileName'];
		$comments = $req['comments'];
		$cw_name = $company.'-'.$warehouse;
		$targetDir = 'json_files/'; //local
		$MapData = $api->getMapData($user_id);

		if($MapData){
			$map_data = $MapData;
		}else{
			$map_data = array();
		}
		if($map_data){
			$newData['data'] = $map_data['data'];
			$newData['comments'] = $comments;
			$jsondata = json_encode($newData, JSON_PRETTY_PRINT);
			//echo $jsondata;die;
		}else{
			$jsondata = '';
		}
		$myFile = $fileName.'.json';
		$filePath = $targetDir.'/'.$folder.'/'.urlencode($myFile);

		if(file_put_contents($filePath, $jsondata)) {
			$bucketName = "arms4-zonificacion-dev";

			if($fileContent = file_get_contents($filePath)){
				$cloudPath = $cw_name.'/'.$folder.'/'.$myFile;
				$isSucceed = $api->uploadFile($bucketName, $fileContent, $cloudPath);

				if ($isSucceed == true) {
					$delResData['status'] = true;
				}
			}else{
				$delResData['status'] = false;
			}

		}else{
			$delResData['status'] = false;
		}
		echo json_encode($delResData);
	}

	public function saveMapData(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		if(isset($req['shape_data'])){
			//echo '<Pre>';print_R($req['shape_data']);die;
			$jsondata = json_encode($req['shape_data'],true);
		}else{
			$jsondata = '';
		}


		$map_data = array();

		if(isset($req['ZoneWiseDeliveryRows'])){
				$ZoneWiseDeliveryRows = $req['ZoneWiseDeliveryRows'];
				//unset($ZoneWiseDeliveryRows[0]); // remove empty 0 element
				$ZoneWiseDeliveryRows = array_filter($ZoneWiseDeliveryRows);
		}else{
			$ZoneWiseDeliveryRows = array();
		}
		//echo '<Pre>';print_R($ZoneWiseDeliveryRows);die;
		if(isset($req['NoZoneDeliveryRows'])){
				$NoZoneDeliveryRows = $req['NoZoneDeliveryRows'];
				$NoZoneDeliveryRows = array_filter($NoZoneDeliveryRows);
		}else{
			$NoZoneDeliveryRows = array();
		}

		if(isset($req['model_comments'])){
				$model_comments = $req['model_comments'];
				$model_comments = trim($model_comments);
		}else{
			$model_comments = '';
		}

		$zone_data = array(
			'zones' => $ZoneWiseDeliveryRows,
			'no_zone' => $NoZoneDeliveryRows
		);

		//echo '<Pre>'; print_r($jsondata);die;
		if($jsondata){
			$jsonArr = json_decode($jsondata,true);
			$map_data['data']  = $jsonArr['zones'];
			$map_data['comments'] = $model_comments;

			//echo '<pre>'; print_R($map_data);die;

			$api->flushMapData($user_id);
			//insert into MapData
			$api->addMapData($user_id,$map_data);
			//only map data, will be used for model_data/file (it is combination of opened model data files)

		}else{
			$api->flushMapData($user_id);

		}
		//echo '<pre>';print_r($zone_data);die;
		$api->flushZoneData($user_id);
		$api->addZoneData($user_id,$zone_data);
		// it contains zone wise delivery coordinates and order_id, for zone table only, it will not saved based on delivery data, if delivery data not avaible then it is empty.

		$delResData['status'] = true;
		$mtdata = $this->modelTableData($api);
		$delResData['data']['table'] = $mtdata['output'];
		$delResData['data']['zone_count'] = $mtdata['zone_count'];
		$this->seDataForExportFiles($api); //for Export data;
		echo json_encode($delResData);
	}

	public function ExportCSVZoneWise(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		$prefix = trim($req['fileName']);
		$header = array('id','Zone Name','');

		$ZoneData = $api->getZoneData($user_id);

		if($ZoneData){
			$zone_data = $ZoneData;
		}else{
			$zone_data = array();
		}
		$myZoneArr = array();
		$modelData = array();
		if($zone_data){
			$i=0;
			// Zone wise Delivery Data
			if($zone_data['zones']){
				$delivery_count = 0;
				$order_pieces = 0;
				$order_cost_goods = 0;
				$order_weight = 0;
				$order_volume = 0;
				$day_name = '';
				$dateArr = array();

				foreach($zone_data['zones'] as $key => $zone){
					array_push($myZoneArr,$key);
					if($key != 0){

						$mapRow = $this->getFromMapDataRow($key);

						if($mapRow){
							$zoneName = $mapRow['name'];
							$comment1 = $mapRow['comment1'];
							$comment2 = $mapRow['comment2'];

							foreach($zone as $row){
								$delivery_count = count($zone);
								$order_weight += $row['order_weight'];
								$order_volume += $row['order_volume'];
								$order_pieces += $row['order_pieces'];
								$order_cost_goods += $row['order_cost_goods'];

								if($row['order_delivery_date_TR1']){
									$dateArr[] = strtolower(date('l',strtotime($row['order_delivery_date_TR1'])));
								}
							}
							if($dateArr){
								$dateArr = array_values(array_unique($dateArr));
								$day_name = implode(', ',$dateArr);
							}

							$modelData[$i]['zone_name'] = $zoneName;
							$modelData[$i]['comment1'] = $comment1;
							$modelData[$i]['comment2'] = $comment2;
							$modelData[$i]['delivery_count'] = $delivery_count;
							$modelData[$i]['order_weight'] = $order_weight;
							$modelData[$i]['order_volume'] = $order_volume;
							$modelData[$i]['order_pieces'] = $order_pieces;
							$modelData[$i]['order_cost_goods'] = $order_cost_goods;
							$modelData[$i]['day_name'] = $day_name;
							$i++;
						}
					}
				}
			}

			// Empty Zone
			$MapData = $api->getMapData($user_id);

			if($MapData){
				$map_data = $MapData;
				if($map_data && $map_data['data']){
					$delivery_count = 0;
					$order_pieces = 0;
					$order_cost_goods = 0;
					$order_weight = 0;
					$order_volume = 0;
					$day_name = '';
					$dateArr = array();

					foreach($map_data['data'] as $row){
						if(!in_array($row['id'], $myZoneArr)){
							$modelData[$i]['zone_name'] = $row['name'];
							$modelData[$i]['comment1'] = $row['comment1'];
							$modelData[$i]['comment2'] = $row['comment2'];
							$modelData[$i]['delivery_count'] = '';
							$modelData[$i]['order_weight'] = '';
							$modelData[$i]['order_volume'] = '';
							$modelData[$i]['order_pieces'] = '';
							$modelData[$i]['order_cost_goods'] = '';
							$modelData[$i]['day_name'] = '';
							$i++;
						}
					}
				}
			}

			// No Zone Delivery Data
			$j = $i+1;
			if($zone_data['no_zone']){
				$delivery_count = 0;
				$order_pieces = 0;
				$order_cost_goods = 0;
				$order_weight = 0;
				$order_volume = 0;
				$day_name = '';
				$dateArr = array();

				foreach($zone_data['no_zone'] as $row){
					$delivery_count = count($zone_data['no_zone']);
					$order_weight += $row['order_weight'];
					$order_volume += $row['order_volume'];
					$order_pieces += $row['order_pieces'];
					$order_cost_goods += $row['order_cost_goods'];

					if($row['order_delivery_date_TR1']){
						$dateArr[] = strtolower(date('l',strtotime($row['order_delivery_date_TR1'])));
					}

					if($dateArr){
						$dateArr = array_values(array_unique($dateArr));
						$day_name = implode(', ',$dateArr);
					}

					$zoneName = 'No Zone';
					$comment1 = '';
					$comment2 = '';

					$modelData[$j]['zone_name'] = $zoneName;
					$modelData[$j]['comment1'] = $comment1;
					$modelData[$j]['comment2'] = $comment2;
					$modelData[$j]['delivery_count'] = $delivery_count;
					$modelData[$j]['order_weight'] = $order_weight;
					$modelData[$j]['order_volume'] = $order_volume;
					$modelData[$j]['order_pieces'] = $order_pieces;
					$modelData[$j]['order_cost_goods'] = $order_cost_goods;
					$modelData[$j]['day_name'] = $day_name;
				}
			}
		}
	}

	public function getDataForPredict(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$data['user_email'] = $user_email = $user['email'];
		$data['user_id'] = $user_id = $user['user_id'];
		$data['company'] = $req['company'];
		$data['warehouse'] = $req['warehouse'];
		$data['folder'] = $folder = 'ZONES';
		$jsonName = $req['fileName'];
		$gcsBucket_url = 'https://storage.googleapis.com/arms4-zonificacion-dev';
		$cw_name = $data['company'].'-'.$data['warehouse'];
		$jsonURL = $gcsBucket_url.'/'.$cw_name.'/'.$folder.'/'.rawurlencode($jsonName);
	//	echo $jsonURL;die;
		$response = $api->getDatafromJson($jsonURL);
		//echo '<pre>'; print_R($response);die;
		if($response){
			$delResData['status'] = true;
			if(Session::has('predict_data')){
				$predict_data = Session::get('predict_data');
			}else{
				$predict_data = array();
			}

			$predict_data = $response;
			session()->forget('predict_data');
			Session::put('predict_data', $predict_data);
			$delResData['data'] = $predict_data;
		}else{
			$delResData['status'] = false;
			$delResData['data'] = array();
		}
		echo json_encode($delResData);
	}

	public function setDefaultOptions(Request $request){
		$req = $request->all();
		$data['my_company'] = $req['company'];
		$data['my_warehouse'] = $req['warehouse'];
		$data['my_model'] = $req['fileName'];

		if(Session::has('default_options')){
			session()->forget('default_options');
		}
		Session::put('default_options', $data);
		$delResData['status'] = true;
		echo json_encode($delResData);
	}

	public function export(Request $request) {
		$zoneId = $request->route('id');
		$prefix = Input::get('name');
		$suffix = '';
		$data = array();

 		if(Session::has('exportZoneData')){
			$exportZoneData = Session::get('exportZoneData');
		}else{
			$exportZoneData = array();
		}

		if($exportZoneData){
			foreach($exportZoneData as $zone){
				if($zone['id'] == $zoneId){
					$data = $zone['data'];
					$suffix = $zone['name'].'.csv';
					continue;
				}
			}
		}
		//echo '<pre>';print_r($exportZoneData);die;
		/*
            'Order ID',
            'Delivery Cost',
            'KM',
            'Pieces',
            '$ Goods',
            'Order Weight',
            'Order Volume',
            'Volume Occupation %',
            'Weight Occupation %',
		*/
			if($data && $suffix!= ''){
				$filename = $prefix.' - '.$suffix;
			//echo '<pre>';print_r($data);die;

			return '';// Excel::download(new ZonesExport($data), $filename);
			}


    }

	public function seDataForExportFiles($api){
		$user = Session::get('user');
		$data['user_id'] = $user_id = $user['user_id'];

		$ZoneData = $api->getZoneData($user_id);

		if($ZoneData){
			$zone_data = $ZoneData;
		}else{
			$zone_data = array();
		}

		$MapData = $api->getMapData($user_id);

		if($MapData){
			$map_data = $MapData;
		}else{
			$map_data = array();
		}

		$exportZoneData = array();

 		if($map_data){
			$i=0;
			foreach($map_data['data'] as $row){

				$exportZoneData[$i]['id'] = $row['id'];
				$exportZoneData[$i]['name'] = $row['name'];
				$exportZoneData[$i]['data'] = array();

				if($zone_data){
					foreach($zone_data['zones'] as $key => $value){

						if($key!=0){
							if($key == $row['id']){
								$data = array();

								if($value){
									$j=0;

									foreach($value as $zone){

									//echo '<pre>';print_r($zone);die;
										$data[$j] = $zone;
/* 										$data[$j] = array(
											'id' => $zone['order_id'],
											'delivery_cost' => $zone['trip_tot_cost_trip'],
											'km' => $zone['trip_total_km'],
											'pieces' => $zone['order_pieces'],
											'goods' => $zone['order_cost_goods'],
											'weight' => $zone['order_weight'],
											'volume' => $zone['order_volume'],
											'volume_occupation' => $zone['trip_volume_occupation_vehicle'],
											'weight_occupation' => $zone['trip_weight_occupation_vehicle'],
										); */

									$j++;
									}
								}

								$exportZoneData[$i]['data'] = $data;
								continue;
							}
						}
					}
				}
				$i++;
			}
		}

	//echo '<Pre>';print_R($exportZoneData);die;
		if($exportZoneData){
			session()->forget('exportZoneData');
			Session::put('exportZoneData', $exportZoneData);
		}
		return true;
	}

/* 	public function getDeliveriesByWarehouse(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$user_id = $user['user_id'];
		$warehouse = $req['warehouse'];//all,selected,none
		$deliveryArr = array();

		$delivery_data = $api->getDeliveryDataByWarehouse($user_id,$warehouse);
		
		$unique_delivery_data = array();

		if($delivery_data){
			
			foreach($delivery_data as $row){
				$unique_delivery_data[$row['visit_destination_name']] = $row;
			}
				
			foreach($unique_delivery_data as $row){
				$deliveryArr[] = $row->visit_destination_name;
			}
		}

		$resData['status'] = true;
		$resData['data'] = $deliveryArr;
		echo json_encode($resData);
	} */


	public function getWarehouseMarkers(Request $request, ApiService $api){
		$req = $request->all();
		$user = Session::get('user');
		$user_id = $user['user_id'];
		$warehouse = $req['warehouse'];//all,selected,none
		$warehouse_rows = $user['warehouse_data'];
		$WarehouseData = array();
		
		if($warehouse_rows != ''){
			$warehouse_rows = json_decode($warehouse_rows,true);
		}
		
		if(is_array($warehouse_rows) && count($warehouse_rows) > 0){

			if($warehouse == 'all'){
				if(Session::get('lastCompanySelected')){
					$company = Session::get('lastCompanySelected');
					$i=0;
					foreach($warehouse_rows as $row){
						if($row['company_name'] == $company){
							//$warehouse_name = $row['warehouse_name'];
							//$WarehouseData[$i][$warehouse_name]['latitude'] = $row['warehouse_latitud'];
							//$WarehouseData[$i][$warehouse_name]['longitude'] = $row['warehouse_longitud'];
							
							$WarehouseData[$i]['warehouse_name'] = $row['warehouse_name'];
							$WarehouseData[$i]['latitude'] = $row['warehouse_latitud'];
							$WarehouseData[$i]['longitude'] = $row['warehouse_longitud'];
							$i++;
						}
					}
				}
			}elseif($warehouse == 'none'){
				$WarehouseData = array();
			}elseif($warehouse == 'selected'){
				
				if(Session::get('lastCompanySelected') && Session::get('lastWarehouseSelected')){
					$company = Session::get('lastCompanySelected');
					$warehouse = Session::get('lastWarehouseSelected');
					$i=0;
					foreach($warehouse_rows as $row){
						if($row['company_name'] == $company && $row['warehouse_name'] == $warehouse){
							//$warehouse_name = $row['warehouse_name'];
							//$WarehouseData[$i][$warehouse_name]['latitude'] = $row['warehouse_latitud'];
							//$WarehouseData[$i][$warehouse_name]['longitude'] = $row['warehouse_longitud'];
							$WarehouseData[$i]['warehouse_name'] = $row['warehouse_name'];
							$WarehouseData[$i]['latitude'] = $row['warehouse_latitud'];
							$WarehouseData[$i]['longitude'] = $row['warehouse_longitud'];							
							$i++;
						}
					}
				}
			}
		}
		
		$resData['status'] = true;
		$resData['data'] = $WarehouseData;
		echo json_encode($resData);
	}
	
	public function getDeliveriesByDay(Request $request, ApiService $api){
		$req = $request->all();
		$days = array();
		
		if(isset($req['days'])){
			$days = $req['days'];			
		}

		$user = Session::get('user');
		$user_id = $user['user_id'];

		$DeliveryData = $api->getDeliveryData($user_id);
		$dayArr = array();

		if($DeliveryData){
			$dData = $DeliveryData;
			$delivery_data = $dData['data'];
			$unique_delivery_data = array();

			if($delivery_data){
				
				foreach($delivery_data as $row){
					
					$destination = $row['visit_destination_name'];
					$date = date('Y-m-d',strtotime($row['order_delivery_date_TR1']));			
					$unique_delivery_data[$destination][$date]['visit_destination_name'] = $destination;					
					$unique_delivery_data[$destination][$date]['order_delivery_date_TR1'] = $date;					
				}
				
				$i=0;
				foreach($unique_delivery_data as $destination){		
				
					if($destination){
						
						foreach($destination as $row){
							$day = strtolower(date('l',strtotime($row['order_delivery_date_TR1'])));
							$dayArr[$day][] = $row['visit_destination_name'];
						}	
					}
				}
			}
		}

		if($dayArr){
			//echo 'before <pre>'; print_r($dayArr);echo '</pre>';
			foreach($dayArr as $key => $value){
				$dayArr[$key] = array_values(array_unique($dayArr[$key]));
			}
			//echo 'after <pre>'; print_r($dayArr);die;

			$deliveryArr = array();

			if($days){
				foreach($days as $value){

					if(isset($dayArr[$value])){
						$myArr = $dayArr[$value];
						foreach($myArr as $myVal){
							$deliveryArr[] = $myVal;
						}
					}
				}
			}
			
			if($deliveryArr){
				$resData['status'] = true;
			}else{
				$resData['status'] = false;
			}
			
			$resData['data'] = $deliveryArr;
			echo json_encode($resData);
		}
	}

	public function getHeatmapDeliveriesByUnits(Request $request, ApiService $api){
		$req = $request->all();
		$unit = $req['unit'];
		$selected_days = array();
		
		if(isset($req['selectedDays'])){
			$selected_days = $req['selectedDays'];
		}
		
		$selected_all_days = $req['selectedAllDays'];
		$user = Session::get('user');
		$user_id = $user['user_id'];
		
		$deliveryData = $api->getDeliveryData($user_id);
		$dataForResponse = [];
		$DeliveryData = array();
		$NewDeliveryData = array();
		
		if($deliveryData){		
			foreach($deliveryData['data'] as $row){
				$destination = $row['visit_destination_name'];
				$date = date('Y-m-d',strtotime($row['order_delivery_date_TR1']));
				
				$DeliveryData[$destination][$date]['visit_destination_name'] = $row['visit_destination_name'];
				$DeliveryData[$destination][$date]['order_delivery_date_TR1'] = $date;
				$DeliveryData[$destination][$date]['order_latitude_TR1'] = $row['order_latitude_TR1'];
				$DeliveryData[$destination][$date]['order_longitud_TR1'] = $row['order_longitud_TR1'];
				$DeliveryData[$destination][$date]['order_frequency'][] = $row['order_id'];
				$DeliveryData[$destination][$date]['order_weight'][] = $row['order_weight'];
				$DeliveryData[$destination][$date]['order_volume'][] = $row['order_volume'];
				$DeliveryData[$destination][$date]['order_pieces'][] = $row['order_pieces'];
				$DeliveryData[$destination][$date]['order_cost_goods'][] = $row['order_cost_goods'];
			}
		}

		if($DeliveryData){
			//echo '<pre>';print_R($DeliveryData);die;
			$i=0;
			foreach($DeliveryData as $destination){		
			
				if($destination){
					
					foreach($destination as $row){
						$NewDeliveryData[$i]['visit_destination_name'] = $row['visit_destination_name'];
						$NewDeliveryData[$i]['order_frequency'] = count($row['order_frequency']);				
						$NewDeliveryData[$i]['order_delivery_date_TR1'] = $row['order_delivery_date_TR1'];						
						$NewDeliveryData[$i]['order_latitude_TR1'] = $row['order_latitude_TR1'];
						$NewDeliveryData[$i]['order_longitud_TR1'] = $row['order_longitud_TR1'];
						
						$order_weight_total = array_sum($row['order_weight']);
						$NewDeliveryData[$i]['order_weight'] = round($order_weight_total,2);
						
						$order_volume_total = array_sum($row['order_volume']);
						$NewDeliveryData[$i]['order_volume'] = round($order_volume_total,2);
						
						$order_pieces_total = array_sum($row['order_pieces']);
						$NewDeliveryData[$i]['order_pieces'] = $order_pieces_total;
						
						$order_cost_goods_total = array_sum($row['order_cost_goods']);
						$NewDeliveryData[$i]['order_cost_goods'] = round($order_cost_goods_total,2);						
						$i++;						
					}
				}
			}
		}		
		
		//echo '<pre>';print_R($NewDeliveryData);die;		
		if($NewDeliveryData){
			$delivery_data = $NewDeliveryData;
			
			if($delivery_data){
				
				foreach($delivery_data as $i => $row){
					
					if ($selected_all_days === 'true') {
						$day = strtolower(date('l',strtotime($row['order_delivery_date_TR1'])));
						$date = explode(' ', $row['order_delivery_date_TR1'])[0];
						
						if (!array_key_exists($day, $dataForResponse)) {
							$dataForResponse[$day]['data'] = array();
						}
										
						if($unit == 'frequency') {
							
							if (!empty($selected_days)) {
								
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {
									
										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_frequency'],
										);
										array_push($dataForResponse[$day]['data'], $data);
									}
								}
							}

						} elseif($unit == 'kg') {

							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_weight'],
										);
										array_push($dataForResponse[$day]['data'], $data);
									}
								}
							}
							
						} elseif($unit == 'm3') {
							
							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_volume'],
										);
										array_push($dataForResponse[$day]['data'], $data);
									}
								}
							}
							
						} elseif($unit == 'pieces') {
							
							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_pieces'],
										);
										array_push($dataForResponse[$day]['data'], $data);
									}
								}
							}
							
						} elseif($unit == 'merchandise') {
							
							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_cost_goods'],
										);
										array_push($dataForResponse[$day]['data'], $data);
									}
								}
							}
						}						
					}
					else
					{
						$day = strtolower(date('l',strtotime($row['order_delivery_date_TR1'])));
						$date = explode(' ', $row['order_delivery_date_TR1'])[0];
						$dest = $row['visit_destination_name'];
						
						if (!array_key_exists($dest, $dataForResponse)) {
							$dataForResponse[$dest]['data'] = array();
						}
										
						if($unit == 'frequency') {
							
							if (!empty($selected_days)) {
								
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {
									
										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_frequency'],
										);
										array_push($dataForResponse[$dest]['data'], $data);
									}
								}
							}

						} elseif($unit == 'kg') {

							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_weight'],
										);
										array_push($dataForResponse[$dest]['data'], $data);
									}
								}
							}
							
						} elseif($unit == 'm3') {
							
							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_volume'],
										);
										array_push($dataForResponse[$dest]['data'], $data);
									}
								}
							}
							
						} elseif($unit == 'pieces') {
							
							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_pieces'],
										);
										array_push($dataForResponse[$dest]['data'], $data);
									}
								}
							}
							
						} elseif($unit == 'merchandise') {
							
							if (!empty($selected_days)) {
								foreach ($selected_days as $key => $val) {
									
									if ($day == $val) {

										$data = array(
											'latitude' => $row['order_latitude_TR1'],
											'longitude' => $row['order_longitud_TR1'],
											'visit_destination_name' => $row['visit_destination_name'],
											'Total' => $row['order_cost_goods'],
										);
										array_push($dataForResponse[$dest]['data'], $data);
									}
								}
							}
						}						
					}

				}
			}
			
			//Day/Destination wise Colors distribution
						
			$keyWiseData = array();
			$colors = array('#59b98b', '#76bd85', '#c2cb7a', '#ffd56f', '#fbc86f', '#efa171', '#e37c74');
			
		
			if($dataForResponse){
				
				if ($selected_all_days === 'true') {
					foreach ($dataForResponse as $key => $value) {
						$data = $value['data'];
						$grand_total = 0;
						foreach($data as $val){
							$grand_total += $val['Total'];
						}
						$keyWiseData[$key] = $grand_total;
					}					
				}
				else{
					//Remove empty data
					foreach($dataForResponse as $key => $value){
						if(count($value['data']) == 0){
							unset($dataForResponse[$key]);
						}
					}
					//echo '<pre>';print_r($dataForResponse);die;
					//Get only one index data
					foreach ($dataForResponse as $key => $value) {

						$data = $value['data'];
						
						if(count($data) > 1){
							$Total = $this->getMax($data);
							
							foreach($data as $val){
								if($val['Total'] == $Total){
								$newData[0]['latitude'] = $val['latitude'];
								$newData[0]['longitude'] = $val['longitude'];
								$newData[0]['visit_destination_name'] = $val['visit_destination_name'];
								$newData[0]['Total'] = $val['Total'];
								$dataForResponse[$key]['data'] = $newData;
								}
							}
							
						}else{
							$Total = $data[0]['Total'];
						}
						$keyWiseData[$key] = $Total;	
					}					
				}
			}

			if($keyWiseData){
											
				//Ascending Order
				asort($keyWiseData);
					
				$totalCounts = count($keyWiseData);
				//echo '<pre>';print_R($keyWiseData);die;						
				
				if($totalCounts == 7){
					$i = 0;
					foreach ($keyWiseData as $key => $value) {
						$dataForResponse[$key]['color'] = $colors[$i];
						$i++;
					}
				}
				elseif($totalCounts < 7){
					$i=0;
					foreach ($keyWiseData as $key => $value) {
						
						if($totalCounts == 1){
							$dataForResponse[$key]['color'] = $colors[6];//red
						}
						
						if($totalCounts == 2){
							
							if($i == 0){ $dataForResponse[$key]['color'] = $colors[0]; }//green
							if($i == 1){ $dataForResponse[$key]['color'] = $colors[6]; }//red
						}
						
						if($totalCounts == 3){
							
							if($i == 0){ $dataForResponse[$key]['color'] = $colors[0]; }//green
							if($i == 1){ $dataForResponse[$key]['color'] = $colors[4]; }//yellow
							if($i == 2){ $dataForResponse[$key]['color'] = $colors[6]; }//red
						}
						
						if($totalCounts == 4){
							
							if($i == 0){ $dataForResponse[$key]['color'] = $colors[0]; }//green
							if($i == 1){ $dataForResponse[$key]['color'] = $colors[2]; }//greenyellow
							if($i == 2){ $dataForResponse[$key]['color'] = $colors[5]; }//orange
							if($i == 3){ $dataForResponse[$key]['color'] = $colors[6]; }//red
						}
						
						if($totalCounts == 5){
							
							if($i == 0){ $dataForResponse[$key]['color'] = $colors[0]; }//green
							if($i == 1){ $dataForResponse[$key]['color'] = $colors[2]; }//yellowgreen
							if($i == 2){ $dataForResponse[$key]['color'] = $colors[4]; }//yellow
							if($i == 3){ $dataForResponse[$key]['color'] = $colors[5]; }//orange
							if($i == 4){ $dataForResponse[$key]['color'] = $colors[6]; }//red
						}
						
						if($totalCounts == 6){
							
							if($i == 0){ $dataForResponse[$key]['color'] = $colors[0]; }//green
							if($i == 1){ $dataForResponse[$key]['color'] = $colors[2]; }//greenyellow
							if($i == 2){ $dataForResponse[$key]['color'] = $colors[3]; }//yellowgreen
							if($i == 3){ $dataForResponse[$key]['color'] = $colors[4]; }//yellow
							if($i == 4){ $dataForResponse[$key]['color'] = $colors[5]; }//orange
							if($i == 5){ $dataForResponse[$key]['color'] = $colors[6]; }//red
						}
						
						$i++;
					}
				}
				else{
					$w=0;
					$indexWiseData = array();
					foreach ($keyWiseData as $key => $value) {
						$indexWiseData[$w] = $value;
						$w++;
					}
					$i=0;
					$last = count($keyWiseData) - 1;
					$mid = floor(count($keyWiseData) / 2);
					$max = max($keyWiseData);
					$min = min($keyWiseData);
					$midVal = $indexWiseData[$mid];		
				
					$iColor = array();
					$assignDestArr = array();
					foreach ($keyWiseData as $key => $value) {
						
						if($i == 0){ 
							$iColor[$i] = $dataForResponse[$key]['color'] = $colors[0]; 
							$assignDestArr[] = $key;
						}
						
						elseif($i == $last){ 
							$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[6];
							$assignDestArr[] = $key;
						}
						
						else{
							if($value == $indexWiseData[0]){ 
									$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[0];
									$assignDestArr[] = $key;
								} //if same value of index 
							
							elseif($value == $indexWiseData[count($indexWiseData)-1]){ 
									$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[6];
									$assignDestArr[] = $key;
								} //if same value of last index
							
							else{
								
								if($value == $indexWiseData[$i-1]){ 
									$iColor[$i] =  $dataForResponse[$key]['color'] = $iColor[$i-1];
									$assignDestArr[] = $key;
								}else{
									if(!in_array($key,$assignDestArr)){
										if($value <= $midVal){
											$add = $min + $value;
											$add2 = $value + $indexWiseData[$i-1];
											if(isset($indexWiseData[$i-2])){
												$add3 = $value + $indexWiseData[$i-2];
											}else{
												$add3 = $value + $indexWiseData[$i-1];
											}
											
										
											if($add >= $add2){
												$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[1];
												$assignDestArr[] = $key;
											}elseif($add2 >= $add3){
												$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[2];
												$assignDestArr[] = $key;
											}else{
												$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[3];
												$assignDestArr[] = $key;
											}												
										}elseif($value > $midVal){
											
											$diff = $max - $value;
											$diff2 = $value - $indexWiseData[$i-1];
											$diff3 = $value - $indexWiseData[$i-2];
							
											if($diff < $diff2){
												$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[6];
												$assignDestArr[] = $key;
											}elseif($diff2 < $diff3){
												$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[5];
												$assignDestArr[] = $key;
											}else{
												$iColor[$i] =  $dataForResponse[$key]['color'] = $colors[4];
												$assignDestArr[] = $key;
											}	
											
										}
									}
								}

							}								
						}
						$i++;
					}						
				}				
			}
		}
		//echo '<pre>';print_r($dataForResponse);die;
		$resData['status'] = true;
		$resData['data'] = $dataForResponse;
		echo json_encode($resData);
	}
	
	
	public function getMax( $array )
	{
		$max = 0;
		foreach( $array as $k => $v )
		{
			$max = max( array( $max, $v['Total'] ) );
		}
		return $max;
	}	
}


