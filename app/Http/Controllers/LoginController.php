<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\User;
use Hash;
use Session;
use App;
use App\Library\Services\ApiService;


class LoginController extends Controller
{
	public function redirect($provider) {
		return Socialite::driver($provider)->with(["prompt" => "select_account"])->redirect();
	}

	public function callback($provider, ApiService $api) {
		//var_dump('testing');
		// var_dump(Socialite::driver($provider));
		$getInfo = Socialite::driver($provider)->stateless()->user();
		var_dump($getInfo);
		Session::put('getInfo', $getInfo);
		if(env('GOOGLE_LOGIN_DEFAULT') == 1){
			$request['user_email'] = 'alcyonedeveloper@gmail.com';
			$request['user_name'] = 'ALCYONE';
			$pass = 'arms2019';
		}else{
			$request['user_email'] = $getInfo->email;
			$request['user_name'] = $getInfo->name;
			$pass = 'arms2019';
		}

		$result = $api->loginApi($request);

		if($result->status == 1){

			$user_data = array(
				'email' => $request['user_email'],
				'password' => Hash::make($pass),
				'name' => $request['user_name'],
				'user_warehouse_accesss_list' => json_encode($result->user_warehouse_accesss_list),
				'warehouse_data' => json_encode($result->warehouse_data),
				'provider' => $provider,
				'provider_id' => $getInfo->id,
			);

			$user = $this->createUser($user_data);
			auth()->login($user);

			$user_info['user_id'] = $user['id'];
			$user_info['email'] = $request['user_email'];
			$user_info['user_warehouse_accesss_list'] = json_encode($result->user_warehouse_accesss_list);
			$user_info['warehouse_data'] = json_encode($result->warehouse_data);
			session()->put('user', $user_info);
			
			$api->flushOldData($user['id']);
			
			return redirect()->to('/login');

		}elseif($result->status != 1 && $result->message == 'Usuario no registrado'){
		    $msg = __('messages.login.loginErrorNotReg');
			Session::put('getInfo', array());
			return redirect('/login')->withErrors([$msg]);
		}else{
		    $msg = __('messages.login.loginError');
			Session::put('getInfo', array());
			return redirect('/login')->withErrors([$msg]);
		}

		//return redirect()->to('/login');

	}

	function createUser($user_data){
		//echo '<Pre>'; print_r($user_data);die;

		$user = User::where('email', $user_data['email'])->first();

		if (!$user) {
		$user = User::create($user_data); //create new user
		}else{
		User::where('email',$user_data['email'])->update($user_data);//update existing user
		}
		return $user;
	}

    // For language change
    public function changeLanguage(Request $request) {
		$locale = request()->segment(2);
        $page = request()->segment(3);
        Session::put('locale', $locale);
		App::setLocale($locale);
        return Redirect($page);
    }

    public function logout(Request $request) {
		Session::flush();
		return Redirect('/');
    }

}

?>