// <?php

// namespace App\Exports;

// use App\User;
// use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\Exportable;
// use Maatwebsite\Excel\Concerns\WithHeadings;

// class ZonesExport implements FromCollection, WithHeadings {
//     /**
//     * @return \Illuminate\Support\Collection
//     */
//     use Exportable;
//     protected $data;
//     function __construct($data) {
//         $this->data = $data;
//     }

//     public function collection() {
//         return $this->data;
//     }

//     public function headings(): array {
//          return [
// 			'order_alias','order_cost_goods','order_delivery_date_TR1','order_id','order_invoice','order_latitude_TR1','order_longitud_TR1','order_pieces','order_trip_id','order_visit_TR1','order_visit_id','order_volume','order_weight','trip_tot_cost_trip','trip_total_km','trip_volume_occupation_vehicle','trip_weight_occupation_vehicle','visit_destination_name',	 
//         ]; 
//     }
// }