<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZoneDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zone_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();					
            $table->integer('zone_id')->nullable();					
            $table->integer('is_zone')->nullable();							
            $table->string('del_row_json')->nullable();					
            $table->timestamps();
        });
    }

    /**
	
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zone_data');
    }
}
